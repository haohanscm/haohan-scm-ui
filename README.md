# haohan-scm-ui

#### 介绍
haohan-scm-ui 是与<https://gitee.com/haohanscm/haohan-scm> 搭配的 前端管理。<br/>

产品适用于供应链管理, 采购配送，仓储管理、进销存、订单系统等。
saas系统支持多租户。
<br/>
演示地址： http://pds.haohanwork.com  <br/>
租户：北京分公司  <br/>
账号密码：pds/123456  scm/123456

底层采用spring-cloud 微服务
前端基于 element、avue 混开完成，同时兼容 element、avue 的功能.
#### 代码目录结构
```
haohan-scm-ui  -- UI工程
 ├── public -- 静态资源
 ├    └── cdn -- 伪CDN
 ├    └── img -- 图片资源
 ├    └── svg -- SVG资源
 ├    └── util -- 网上下载的工具
 ├    └── favicon.ico -- 网站图标
 ├    └── index.html -- 网站首页，也是唯一的一张页面
 ├── src -- 源码目录
 ├    └── api -- 和后端交互的API相关
 ├    ├── components -- 自己封装的组件
 ├    ├── config -- 工程配置
 ├    ├── const -- 常量
 ├    ├── docker -- docker部署相关
 ├    ├── filters -- 全局过滤器
 ├    └── mixins -- VUE组件混入
 ├    └── page -- 页面组件
 ├    └── route -- VUE-Router相关
 ├         └── page -- 页面路由
 ├         └── views -- 业务路由
 ├         └── avue-router.js --自定义路由处理，包括路由拦截，动态路由等
 ├         └── axios.js --axios增强
 ├         └── route.js --路由配置入口
 ├    └── store -- VUEX相关
 ├    └── styles -- 样式管理
 ├    └── util -- 工具包
 ├    └── views -- 业务代码
 ├    └── App.vue -- 根组件
 ├    └── error.js -- 自定义错误日志处理
 ├    └── main.js -- 入口js
 ├    └── permission.js -- 权限判断，导航守卫
 ├── .browserslistrc -- barbel兼容配置
 ├── .editorconfig -- 开发组统一环境配置
 ├── .editorconfig -- ESLint配置
 ├── .gitignore -- git忽略列表
 ├── .postcssrc.js -- CSS预处理配置
 ├── babel.config.js -- barbel配置入口
 ├── package.json -- 依赖管理
 ├── vue.config.js -- vue cli3的webpack配置
```

#### 代码说明
根目录下:
- api 请求接口
- components 通用组件
- const
  - crud 表单、表格组件的选项option
  - setting 页面顶部菜单栏设置
- page 页面布局、登录页、首页
  - index 页面布局
  - login 登录员
  - wel 首页
- router 页面路由 VUE-Router相关
  - page -- 页面路由
  - views -- 业务路由 
  - avue-router.js --自定义路由处理，包括路由拦截，动态路由等
  - axios.js --axios增强
  - route.js --路由配置入口
- store VUEX相关
- styles 样式
- util 工具方法
- views 系统页面(框架功能页面、代码生成页面)

### 通用模块
根目录下:
- bill 账单管理
  - payable 应付
  - receivable 应付
  - settlement 结算
- goods 商品管理
- message 消息管理
  - in_mail 站内信消息
- opc 平台管理模块
  - photo 图片管理
  - ship_record 发货记录
  - shipper 发货人

### 系统模块
根目录下:
- pds 采购配送系统
- scm 供应链系统

包含的功能列表如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/165520_5052581a_7837037.png "屏幕截图.png")


#### 系统相关截图:
![系统截图](images/sys-01.png)
![系统截图](images/sys-02.png)
![系统截图](images/sys-03.png)

#### 安装教程
1.  git clone https://gitee.com/haohanscm/haohan-scm-ui.git
2.  npm install -g cnpm --registry=https://registry.npm.taobao.org
3.  cnpm install ##安装依赖
4.  cnpm run dev ##启动

#### 使用说明

1. 已开源的代码，授权协议采用 AGPL v3 + Apache Licence v2 进行发行。
2. 您可以免费使用、修改和衍生代码，但不允许修改后和衍生的代码做为闭源软件发布。
3. 修改后和衍生的代码必须也按照AGPL协议进行流通，对修改后和衍生的代码必须向社会公开。
4. 如果您修改了代码，需要在被修改的文件中进行说明，并遵守代码格式规范，帮助他人更好的理解您的用意。
5. 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议、版权声明和其他原作者规定。
6. 您可以应用于商业软件，但必须遵循以上条款原则（请协助改进本作品）。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 商业版本

开源不易，商业版需单独授权。
更多高阶功能请移步 http://www.haohanscm.com/demo

商业版PDS采购配送系统 <br/>

产品功能：http://haohanscm.com/demo/72-cn.html <br/>
视频介绍：https://www.bilibili.com/video/BV1vB4y1P7k7  <br/>

体验账号登录地址：http://cloud.haohanscm.com <br/>
企业code: cs01 <br/> 
账号: cscp <br/>
密码: cs123456 <br/>


需解锁更多高级功能，请关注客服微信：<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/143525_be83aa45_7837037.png "Wechat-logo.png")




