import request from '@/router/axios'

/**
 * 分页列表
 */
export function fetchList(params) {
    return request({
        url: '/bill/api/bill/query/payable/page',
        method: 'get',
        params: params
    })
}

/**
 * 账单详情
 */
export function fetchInfo(params) {
    return request({
        url: '/bill/api/bill/query/payable/billInfo',
        method: 'get',
        params: params
    })
}

/**
 * 审核通过
 */
export function reviewSuccess(params) {
    return request({
        url: '/bill/api/bill/manage/payable/reviewSuccess',
        method: 'post',
        params: params
    })
}

/**
 * 审核不通过
 */
export function reviewFailed(params) {
    return request({
        url: '/bill/api/bill/manage/payable/reviewFailed',
        method: 'post',
        params: params
    })
}

/**
 * 账单金额修改
 */
export function amountModify(params) {
    return request({
        url: '/bill/api/bill/manage/payable/modify',
        method: 'post',
        params: params
    })
}