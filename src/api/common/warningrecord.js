/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/common/warningrecord/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/common/warningrecord',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/common/warningrecord/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/common/warningrecord/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/common/warningrecord',
    method: 'put',
    data: obj
  })
}
