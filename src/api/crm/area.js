/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/area/page',
    method: 'get',
    params: query
  })
}

export function editArea(query) {
  return request({
    url: '/crm/api/crm/customer/editArea',
    method: 'post',
    params: query
  })
}

export function areaTree() {
  return request({
    url: '/crm/api/crm/customer/areaTree',
    method: 'get',
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/area',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/area/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/area/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/area',
    method: 'put',
    data: obj
  })
}
