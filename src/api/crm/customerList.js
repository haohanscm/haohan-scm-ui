/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/api/crm/customer/customerList',
    method: 'get',
    params: query
  })
}
// 查询客户详情
export function getDetail(query) {
  return request({
    url: '/crm/api/crm/customer/info',
    method: 'get',
    params: query
  })
}
// //新增打印机
// export function addFeiePrinter(params) {
//   return request({
//     url: '/iot/api/iot/feiePrinter/addFeiePrinter',
//     method: 'post',
//     data: params,
//     headers: {"Content-Type":"application/json"}
//   })
// }
// //编辑打印机
// export function editFeiePrinter(params) {
//   return request({
//     url: '/iot/api/iot/feiePrinter/editFeiePrinter',
//     method: 'post',
//     data: params,
//     headers: {"Content-Type":"application/json"}
//   })
// }
// //删除打印机
// export function deleteById(params) {
//   return request({
//     url: '/iot/api/iot/feiePrinter/deleteById',
//     method: 'post',
//     data: params,
//     headers: {"Content-Type":"application/json"}
//   })
// }
