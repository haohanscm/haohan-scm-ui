/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/customeranniversary/page',
    method: 'get',
    params: query
  })
}

export function getDetail(query) {
  return request({
    url: '/crm/customeranniversary/' + query,
    method: 'get',
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/customeranniversary',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/customeranniversary/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/customeranniversary/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/customeranniversary',
    method: 'put',
    data: obj
  })
}
