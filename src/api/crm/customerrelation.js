/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/customerrelation/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/customerrelation',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/customerrelation/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/customerrelation/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/customerrelation',
    method: 'put',
    data: obj
  })
}
