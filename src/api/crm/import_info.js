import request from '@/router/axios'

// 导入信息

// 员工
export function employeeImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/employee',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}
export function employeeExcelImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/employeeExcel',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "multipart/form-data"}
    })
}

// 销售区域导入
export function areaImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/area',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

// 市场导入
export function marketImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/market',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}
export function marketExcelImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/marketExcel',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "multipart/form-data"}
    })
}

// 客户信息导入
export function customerImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/customer',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}
export function customerExcelImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/customerExcel',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "multipart/form-data"}
    })
}

// 客户经纬度导入
export function customerPositionImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/customerPosition',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

// 客户联系人导入
export function linkmanImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/linkman',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}
export function linkmanExcelImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/linkmanExcel',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "multipart/form-data"}
    })
}

// 客户送货地址导入
export function addressImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/address',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

// 销售订单导入
export function salesOrderImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/salesOrder',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

// 销量上报导入
export function salesReportImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/salesReport',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}
export function salesReportExcelImport(obj) {
    return request({
        url: '/crm/api/crm/importInfo/salesReportExcel',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "multipart/form-data"}
    })
}
