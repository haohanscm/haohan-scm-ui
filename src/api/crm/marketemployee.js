/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/marketemployee/page',
    method: 'get',
    params: query
  })
}


export function getDetail(query) {
  return request({
    url: '/crm/marketemployee/' + query,
    method: 'get',
  })
}


export function addObj(obj) {
  return request({
    url: '/crm/marketemployee',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/marketemployee/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/marketemployee/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/marketemployee',
    method: 'put',
    data: obj
  })
}
