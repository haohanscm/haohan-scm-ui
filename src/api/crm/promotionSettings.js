/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/api/crm/wares/promotionSettings',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/shiporder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/shiporder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/shiporder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/shiporder',
    method: 'put',
    data: obj
  })
}
