/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/api/crm/stockReport/queryList',
    method: 'get',
    params: query
  })
}
//库存上报详情
export function getDetail(params) {
  return request({
    url: '/crm/api/crm/stockReport/queryInfo',
    method: 'get',
    params: params,
  })
}

//新增库存上报
export function addReport(params) {
  return request({
    url: '/crm/api/crm/stockReport/addReport',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}
//删除库存上报
export function deleteReport(query) {
  return request({
    url: '/crm/api/crm/stockReport/deleteReport',
    method: 'get',
    params: query
  })
}
//编辑库存上报
export function modifyReport(params) {
  return request({
    url: '/crm/api/crm/stockReport/modifyReport',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}

// 确认库存上报
export function sureReport(params) {
  return request({
    url: '/crm/api/crm/stockReport/review',
    method: 'post',
    params: params,
    headers:{"Content-Type":"application/x-www-form-urlencoded"},
  })
}


export function addObj(obj) {
  return request({
    url: '/crm/salescontract',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/salescontract/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/salescontract/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/salescontract',
    method: 'put',
    data: obj
  })
}
