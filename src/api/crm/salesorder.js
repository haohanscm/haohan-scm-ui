/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(params) {
  return request({
    url: '/crm/salesorder/page',
    method: 'get',
    params: params
  })
}

//审核订单
export function orderReview(query) {
  return request({
    url: '/crm/api/crm/salesOrder/orderReview',
    method: 'post',
    params: query,
  })
}

//新增订单
export function addOrder(params) {
  return request({
    url: '/crm/api/crm/salesOrder/addOrder',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}

//销售单详情
export function getDetail(params) {
  return request({
    url: '/crm/api/crm/salesOrder/queryInfo',
    method: 'get',
    params: params,
  })
}

export function editDetail(params) {
  return request({
    url: '/crm/api/crm/salesOrder/modifyOrder',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/salesorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/salesorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/salesorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/salesorder',
    method: 'put',
    data: obj
  })
}
