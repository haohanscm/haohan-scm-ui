/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/shiporder/page',
    method: 'get',
    params: query
  })
}

//发货单详情
export function getDetail(params) {
  return request({
    url: '/crm/api/crm/shipOrder/queryInfo',
    method: 'get',
    params: params,
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/shiporder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/shiporder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/shiporder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/shiporder',
    method: 'put',
    data: obj
  })
}
