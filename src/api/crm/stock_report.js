'use strict';

import request from '@/router/axios';

export function queryStock(params) {
    return request({
        url: '/crm/api/crm/stockReport/queryStock',
        method: 'get',
        params: params,
    });
}