
import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/api/crm/team/teamList',
    method: 'get',
    params: query
  })
}
