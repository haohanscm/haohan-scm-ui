/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/workreport/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/workreport',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/workreport/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/workreport/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/workreport',
    method: 'put',
    data: obj
  })
}

export function getReportInfo(params) {
  return request({
    url: '/crm/api/crm/reporting/info',
    method: 'get',
    params: params,
  })
}


export function addReport(data) {
    return request({
        url: '/crm/api/crm/reporting/add',
        method: 'post',
        data: data,
    })
}

export function updateReport(data) {
    return request({
        url: '/crm/api/crm/reporting/update',
        method: 'post',
        data: data,
    })
}