/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/goods/goodscategory/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/goods/goodscategory',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/goods/goodscategory/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/goods/goodscategory/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/goods/goodscategory',
    method: 'put',
    data: obj
  })
}
//分类栏
export  function getCategoryTree(params) {
  return request({
    url: '/goods/api/goods/manage/categoryTree',
    method: 'get',
    params: params
  })
}
//商品分类新增
export function addCategory(params) {
  return request({
    url: '/goods/api/goods/category',
    method: "post",
    params: params
  })
}
//商品分类修改
export function editCategory(params) {
  return request({
    url: '/goods/api/goods/category',
    method: 'put',
    params: params
  })
}
//商品分类删除
export function deleteCategory(id) {
  return request({
    url: '/goods/api/goods/category/' + id,
    method: "delete"
  })
}
//查询商品分类详情
export function getCategoryDetail(id) {
  return request({
    url: '/goods/api/goods/category/' + id,
    method: 'get'
  })
}
//特殊商品分类获取对应平台分类
export function getSpecialCategory(id) {
  return request({
    url:'/goods/api/goods/special/category/' + id,
    method: 'get'
  })
}
