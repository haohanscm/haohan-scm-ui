/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/goods/standardproductunit/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/goods/standardproductunit',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/goods/standardproductunit/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/goods/standardproductunit/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/goods/standardproductunit',
    method: 'put',
    data: obj
  })
}
