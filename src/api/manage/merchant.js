/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/manage/api/manage/merchant/page',
        method: 'get',
        params: query
    })
}

// 详情
export function getObj(id) {
    return request({
        url: '/manage/api/manage/merchant/info',
        method: 'get',
        params: {merchantId: id}
    })
}

// 平台商家详情
export function getPlatform() {
    return request({
        url: '/manage/api/manage/merchant/platform',
        method: 'get',
        params: {}
    })
}
// 初始化平台商家
export function initPlatform(obj) {
    return request({
        url: '/manage/api/manage/merchant/platform',
        method: 'post',
        params: obj
    })
}

// 新增
export function addObj(obj) {
    return request({
        url: '/manage/api/manage/merchant',
        method: 'post',
        params: obj
    })
}

export function delObj(id) {
    return request({
        url: '/manage/api/manage/merchant/' + id,
        method: 'delete'
    })
}

// 修改
export function putObj(obj) {
    return request({
        url: '/manage/api/manage/merchant',
        method: 'put',
        params: obj
    })
}
