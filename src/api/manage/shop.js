/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/manage/api/manage/shop/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/manage/api/manage/shop',
        method: 'post',
        data: obj
    })
}

export function getObj(shopId) {
    return request({
        url: '/manage/api/manage/shop/fetchInfo',
        method: 'get',
        params: {shopId: shopId}
    })
}

export function delObj(id) {
    return request({
        url: '/manage/api/manage/shop/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/manage/api/manage/shop',
        method: 'put',
        data: obj
    })
}
//查询平台商品规格对应的供应商品信息
export function supplyGoods(params) {
  return request({
    url: '/supply/api/supply/goods/platformModel',
    method: 'get',
    params: params
  })
}
