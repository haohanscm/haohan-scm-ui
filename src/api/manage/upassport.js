/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/manage/upassport/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/manage/upassport',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/manage/upassport/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/manage/upassport/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/manage/upassport',
    method: 'put',
    data: obj
  })
}

export function fetchUid(obj) {
    return request({
        url: '/manage/upassport/fetchUid',
        method: 'post',
        params: obj
    })
}

export function fetchUserId(obj) {
    return request({
        url: '/manage/upassport/fetchUserId',
        method: 'post',
        params: obj
    })
}
