import request from '@/router/axios'

/**
 * 分页列表
 */
export function fetchList(params) {
    return request({
        url: '/msg/api/msg/message/record/page',
        method: 'get',
        params: params
    })
}

/**
 * 站内信详情
 */
export function fetchInfo(params) {
    return request({
        url: '/msg/api/msg/message/inMail/fetchInfo',
        method: 'get',
        params: params
    })
}

/**
 * 平台消息发送
 */
export function sendPlatform(params) {
    return request({
        url: '/msg/api/msg/message/inMail/sendPlatform',
        method: 'post',
        params: params
    })
}

/**
 * 平台消息发送
 */
export function messageModify(params) {
    return request({
        url: '/msg/api/msg/message/inMail/modify',
        method: 'post',
        params: params
    })
}