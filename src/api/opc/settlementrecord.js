/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/opc/settlementrecord/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/opc/settlementrecord',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/opc/settlementrecord/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/opc/settlementrecord/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/opc/settlementrecord',
    method: 'put',
    data: obj
  })
}

// 准备结算
export function readySettlement(obj) {
    return request({
        url: '/opc/api/opc/settlement/readySettlement',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}
