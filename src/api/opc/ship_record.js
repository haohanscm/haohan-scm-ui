import request from '@/router/axios'

/**
 * 分页列表
 */
export function fetchList(params) {
    return request({
        url: '/opc/api/opc/shipManage/record/page',
        method: 'get',
        params: params
    })
}

/**
 * 发货记录详情
 */
export function fetchInfo(params) {
    return request({
        url: '/opc/api/opc/shipManage/record/fetchInfo',
        method: 'get',
        params: params
    })
}

/**
 * 通过订单sn查询发货记录详情
 */
export function fetchInfoByOrderSn(params) {
    return request({
        url: '/opc/api/opc/shipManage/record/fetchInfoByOrderSn',
        method: 'get',
        params: params
    })
}

/**
 * 完成发货操作, 选择发货人，物流公司
 */
export function completeShip(params) {
    return request({
        url: '/opc/api/opc/shipManage/record/complete',
        method: 'post',
        params: params
    })
}

/**
 * 修改发货记录
 */
export function putObj(params) {
    return request({
        url: '/opc/shiprecord',
        method: 'put',
        data: params
    })
}
