import request from '@/router/axios'


export function fetchList(query) {
    return request({
        url: '/opc/shipper/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/opc/shipper',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/opc/shipper/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/opc/shipper/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/opc/shipper',
        method: 'put',
        data: obj
    })
}

/**
 * 分页列表 发货人
 */
export function findList(params) {
    return request({
        url: '/opc/api/opc/shipManage/shipper/page',
        method: 'get',
        params: params
    })
}
