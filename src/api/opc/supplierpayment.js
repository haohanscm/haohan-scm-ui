/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/opc/supplierpayment/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/opc/supplierpayment',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/opc/supplierpayment/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/opc/supplierpayment/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/opc/supplierpayment',
    method: 'put',
    data: obj
  })
}
//查询待结算应付帐单
export function countPayAccount(query) {
    return request({
        url: '/opc/supplierpayment/countBySupplierPaymentReq',
        method: 'post',
        data: query,
    })
}