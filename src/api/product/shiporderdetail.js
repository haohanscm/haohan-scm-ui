/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/product/shiporderdetail/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/product/shiporderdetail',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/product/shiporderdetail/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/product/shiporderdetail/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/product/shiporderdetail',
    method: 'put',
    data: obj
  })
}
