/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/purchase/purchaseorder/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/purchase/purchaseorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/purchase/purchaseorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/purchase/purchaseorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/purchase/purchaseorder',
    method: 'put',
    data: obj
  })
}

//查询采购单总记录数
export function queryPurchaseOrder(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/countPurchaseOrder',
        method: 'post',
        data: query,
        headers: {"Content-Type":"application/json"}
    })
}
//查询采购单总记录数
export function countPurchaseOrder(query) {
    return request({
        url: '/purchase/purchaseorder/countByPurchaseOrderReq',
        method: 'post',
        data: query,
    })
}