/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/saleb/buyorderdetail/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/saleb/buyorderdetail',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/saleb/buyorderdetail/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/saleb/buyorderdetail/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/saleb/buyorderdetail',
    method: 'put',
    data: obj
  })
}

export function lockStorage(obj) {
    return request({
        url: '/saleb/api/saleb/buyOrder/detailConfirmStockList',
        method: 'post',
        data: obj,
        headers: {"Content-Type":"application/json"}
    })
}



