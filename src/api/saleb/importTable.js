/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
//客户订单table导入
import request from '@/router/axios'

export function importTable(params) {
  return request({
    url: '/saleb/api/saleb/excel/importTable',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}
//客户采购订单table导入商品明细
export function importDetail(params) {
  return request({
    url: '/saleb/api/saleb/excel/importDetail',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}


