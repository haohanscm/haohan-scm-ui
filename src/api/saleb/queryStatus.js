/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
//pds检测采购商能否下单
import request from '@/router/axios'

export function queryStatus(query) {
  return request({
    url: '/saleb/api/saleb/buyer/queryStatus',
    method: 'get',
    params: query
  })
}


