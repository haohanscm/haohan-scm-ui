/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function toSaleC(params) {
  return request({
    url: '/salec/api/salec/product/syncShopGoods',
    method: 'post',
    params: params
  })
}

// export function onSale(req) {
//     return request({
//         url: '/salec/api/storeProduct/addSingleOrMultipleProduct',
//         method: 'post',
//         data:req,
//         headers: {"Content-Type":"application/json"}
//     })
// }

// export function offSale(req) {
//     return request({
//         url: '/salec/api/storeProduct/offTheShelf',
//         method: 'post',
//         data:req,
//         headers: {"Content-Type":"application/json"}
//     })
// }
// 单个同步商城
export function onSale(req) {
  return request({
      url: '/salec/api/salec/product/syncGoods',
      method: 'post',
      params:req,
      headers: {"Content-Type":"application/x-www-form-urlencoded"}
  })
}
// 取消单个同步商城
export function offSale(req) {
  return request({
      url: '/salec/api/salec/product/syncGoods',
      method: 'delete',
      params:req,
      headers: {"Content-Type":"application/json;charset=utf-8"}
  })
}
export function categoryToSaleC(params) {
  return request({
    url: '/salec/api/salec/category/syncShopCategory',
    method: 'post',
    params: params
  })
}
