/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function toSalecCategory() {
    return request({
        url: '/salec/api/storeCategory/createStoreCategory',
        method:'post'
    })
}

export function fetchCategoryList(params) {
  return request({
    url: '/goods/goodscategory/page',
    method:'get',
    params:params
  })
}
