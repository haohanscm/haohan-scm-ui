/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

// 应付账单

export function fetchList(query) {
    return request({
        url: '/opc/supplierpayment/fetchPage',
        method: 'get',
        params: query
    })
}
//应付账单详情
export function getDetail(params) {
  return request({
    url: '/opc/api/opc/payable/queryPaymentDetail',
    method: 'get',
    params: params,
  })
}
// 创建应付账单
export function createPayable(obj) {
    return request({
        url: '/opc/api/opc/payable/createPayable',
        method: 'post',
        data: obj,
        headers: {"Content-Type":"application/json"}
    })
}

// 批量创建 采购应付账单
export function createPayableBatch(obj) {
    return request({
        url: '/opc/api/opc/payable/createPayable/batch',
        method: 'post',
        data: obj,
        headers: {"Content-Type":"application/json"}
    })
}


// 准备结算
export function readySettlement(obj) {
    return request({
        url: '/opc/api/opc/settlement/readySettlement',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}



// 查询是否可审核
export function checkPayable(obj) {
    return request({
        url: '/opc/api/opc/payable/checkPayable',
        method: 'post',
        params: obj
    })
}

// 查询是否可审核
export function reviewPayment(obj) {
    return request({
        url: '/opc/api/opc/payable/reviewPayment',
        method: 'post',
        params: obj
    })
}
