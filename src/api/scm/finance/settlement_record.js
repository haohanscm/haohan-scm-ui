/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/opc/settlementrecord/page',
        method: 'get',
        params: query
    })
}
export function getDetail(query) {
  return request({
    url: '/opc/api/opc/settlement/info',
    method: 'get',
    params: query
  })
}

// // 准备结算
// export function readySettlement(obj) {
//     return request({
//         url: '/opc/api/opc/settlement/readySettlement',
//         method: 'post',
//         data: obj,
//         headers: {"Content-Type": "application/json"}
//     })
// }

// 完成结算
export function completeSettlement(obj) {
    return request({
        url: '/opc/api/opc/settlement/completeSettlement',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

