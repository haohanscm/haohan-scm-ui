/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(params) {
    return request({
        url: '/wms/api/wms/scmExitWarehouse/queryWaitList',
        method: 'post',
        params: params
    })
}

export function confirmRealExitNum(params) {
    return request({
        url: '/wms/api/wms/scmExitWarehouse/confirmRealExitNum',
        method: 'post',
        data: params,
        headers: {"Content-Type":"application/json"}
    })
}

// export function addObj(obj) {
//     return request({
//         url: '/opc/api/opc/operation/createSummaryOrderForOrder',
//         method: 'post',
//         data: obj
//     })
// }
//
// export function getObj(id) {
//     return request({
//         url: '/purchase/purchasetask/' + id,
//         method: 'get'
//     })
// }
//
// export function delObj(id) {
//     return request({
//         url: '/purchase/purchasetask/' + id,
//         method: 'delete'
//     })
// }
//
// export function putObj(obj) {
//     return request({
//         url: '/purchase/purchasetask',
//         method: 'put',
//         data: obj
//     })
// }
//
