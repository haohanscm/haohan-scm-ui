/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/aftersales/returnorder/page',
        method: 'get',
        params: query
    })
}
//新增退货单
export function addSend(params) {
  return request({
    url: '/aftersales/api/returnOrder/addBuyReturnOrder',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}

//是否通过退货单
export function returnOrder(params) {
  return request({
    url: '/aftersales/api/returnOrder/auditPassOrder',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}

//查询订单
export function getOrder(params) {
  return request({
    url: '/saleb/api/saleb/buyOrder/queryBuyDetailBySn',
    method: 'get',
    params: params,
  })
}
//入库
export function auditPassReturnOrder(obj) {
  return request({
    url: '/aftersales/api/returnOrder/passReturnOrderAddStock',
    method: 'post',
    data: obj,
    headers: {"Content-Type":"application/json"}
  })
}
//确认收货
export function receiptGoods(obj) {
  return request({
    url: '/aftersales/api/returnOrder/receiptGoods',
    method: 'post',
    data: obj,
    headers: {"Content-Type":"application/json"}
  })
}
//退款
export function refund(obj) {
  return request({
    url: '/supply/api/supply/payment/createPayable',
    method: 'post',
    data: obj
  })
}
//编辑退货单
export function editSend(params) {
  return request({
    url: '/aftersales/api/returnOrder/editReturnOrder',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}
//退货单详情
export function getDetail(params) {
  return request({
    url: '/aftersales/api/returnOrder/queryReturnDetail',
    method: 'get',
    params: params,
  })
}

export function addObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'put',
        data: obj
    })
}
