/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(params) {
    return request({
        url: '/opc/api/opc/operation/summaryOrderList',
        method: 'post',
        params: params
    })
}

//新增汇总采购单
export function createPurchaseOrder(params) {
  return request({
    url: '/opc/api/opc/operation/createPurchaseOrder',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}

export function addObj(obj) {
    return request({
        url: '/opc/api/opc/operation/createSummaryOrderForOrder',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'put',
        data: obj
    })
}

export function changeStatus(obj) {
    return request({
        url: '/saleb/api/saleb/buyOrder/changeStatus',
        method: 'post',
        params: obj
    })
}

// 订单汇总需采购里的订单详情
export function queryWaitSummaryOrderDetail(obj){
  return request({
    url:'/saleb/api/saleb/buyOrder/queryWaitSummaryOrderDetail',
    method:'post',
    data:obj,
    headers: {"Content-Type":"application/json"}
  })
}

//订单汇总可备货里的订单详情
export function summaryOrderDetail(obj) {
  return request({
    url: '/saleb/api/wms/scmExitWarehouse/queryWaitList',
    method: 'post',
    params: obj
  })
}
