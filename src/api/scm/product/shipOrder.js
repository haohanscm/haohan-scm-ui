/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/product/shiporder/page',
        method: 'get',
        params: query
    })
}

// 配送单批量送达
export function arriveBatch(obj){
    return request({
        url:'/product/api/product/shipOrder/arriveBatch',
        method:'post',
        params:obj
    })
}


// 配送单开始配送
export function deliveryBegin(obj){
    return request({
        url:'/product/api/product/shipOrder/deliveryBegin',
        method:'post',
        params:obj
    })
}


