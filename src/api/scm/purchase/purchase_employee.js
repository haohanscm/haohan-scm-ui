/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

/**
 * 新增采购员工
 * @param query
 */
export function addEmployee(params) {
    return request({
        url: '/purchase/api/purchase/employee/addEmployee',
        method: 'post',
        params: params,
    })

}

/**
 * 修改采购员工
 * @param query
 */
export function modifyEmployee(params) {
    return request({
        url: '/purchase/api/purchase/employee/modifyEmployee',
        method: 'post',
        params: params,
    })
}


