/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

/**
 * 查询采购单列表
 * @param query
 */
export function fetchList(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/queryOrderList',
        method: 'get',
        params: query
    })
}


/**
 *查询采购单详情
 * @param {*} purchaseSn
 */
export function getDetail(query) {
  return request({
      url: '/purchase/api/purchase/purchaseOrder/queryPurchaseOrderAndDetail',
      method: 'get',
      params: query
  })
}

/**
 *采购单编辑
 * @param {*} purchaseSn
 */
export function editOrder(query) {
  return request({
      url: '/purchase/api/purchase/purchaseOrder/editPurchaseOrder',
      method: 'POST',
      data: query,
      headers: {"Content-Type":"application/json"}
  })
}


/**
 *发起采购单
 * @param {*} purchaseSn
 */
export function beginPurchase(query) {
  return request({
    url: '/purchase/api/purchase/purchaseOrder/beginPurchase',
    method: 'POST',
    params: query,
  })
}




/**
 * 根据采购单号查询采购单明细
 * @param purchaseSn
 */
export function queryList(purchaseSn) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/queryPurchaseOrderDetailList?purchaseSn=' + purchaseSn,
        method: 'get'
    })
}

/**
 * 查询采购员列表
 * @param purchaseSn
 */
export function queryEmployeeList(query) {
    return request({
        url: '/purchase/api/purchase/employee/queryEmployeeList',
        method: 'post',
        data: query,
        headers: {"Content-Type":"application/json"}
    })
}

/**
 * 新增采购单(采购单明细)/初始化采购任务/创建货品信息/初始化入库单
 * @param query
 */
export function addPurchaseOrderAndDetail(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/addPurchaseOrderAndDetail',
        method: 'post',
        data: query,
        headers: {"Content-Type":"application/json"}
    })
}
/**
 * 审核采购单
 * @param query
 */
export function checkTaskBatch(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/checkTaskBatch',
        method: 'post',
        data: query,
        headers: {"Content-Type":"application/json"}
    })
}
