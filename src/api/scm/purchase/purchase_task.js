/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

/**
 * 查询任务列表(带采购商品信息)
 * @param query
 */
export function fetchList(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/fetchTaskList',
        method: 'post',
        data: query,
        headers: {"Content-Type": "application/json"}
    })
}

/**
 * 查询采购员列表
 * @param query
 */
export function queryEmployeeList(query) {
    return request({
        url: '/purchase/api/purchase/employee/queryEmployeeList',
        method: 'post',
        data: query,
        headers: {"Content-Type": "application/json"}
    })
}

/**
 * 经理 执行采购任务(分配) 指定采购员
 * @param query
 */
export function appointTask(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/appointTask',
        method: 'post',
        data: query,
        headers: {"Content-Type": "application/json"}
    })
}

/**
 * 采购明细货品 实际入库
 * @param query
 */
export function purchaseProductEntry(query) {
    return request({
        url: '/wms/api/wms/enterWarehouse/purchaseProductEntry',
        method: 'post',
        data: query,
        headers: {"Content-Type": "application/json"}
    })
}

/**
 *揽货确认
 * @param {*} purchaseSn
 */
export function confirmGood(params) {
  return request({
    url: '/purchase/api/purchase/purchaseOrderDetail/confirmGoods',
    method: 'POST',
    params: params,
  })
}


/**
 * 采购单明细 供应商确定(单品采购)
 * @param query
 */
export function singlePurchase(query) {
    return request({
        url: '/purchase/api/purchase/purchaseOrder/singlePurchase',
        method: 'post',
        data: query,
        headers: {"Content-Type": "application/json"}
    })
}
