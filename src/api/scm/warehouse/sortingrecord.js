/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchDetailList(query) {
    return request({
        url: '/product/api/product/sortingOrderDetail/queryList',
        method: 'get',
        params: query
    })
}


export function sortedDetail(obj){
    return request({
        url:'/product/api/product/sortingOrderDetail/sortingDetailConfirm',
        method:'post',
        params:obj,
    })
}

export function allSortOut(obj){
    return request({
        url:'/product/api/product/sortingOrder/sortingBatch',
        method:'post',
        params:obj
    })
}

// 分拣单批量确认完成
export function finishBatch(obj){
    return request({
        url:'/product/api/product/sortingOrder/finishBatch',
        method:'post',
        params:obj
    })
}

// 分拣单 单个确认完成
export function finish(obj){
    return request({
        url:'/product/api/product/sortingOrder/finish',
        method:'post',
        params:obj
    })
}


// 配送单批量送达
export function arriveBatch(obj){
    return request({
        url:'/product/api/product/shipOrder/arriveBatch',
        method:'post',
        params:obj
    })
}


