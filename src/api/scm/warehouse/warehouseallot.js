/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/wms/warehouseallot/page',
    method: 'get',
    params: query
  })
}
//新增库存调拨
export function addWarehouseAllot(obj){
  return request({
    url:'/wms/api/wms/scmWarehouseAllot/addWarehouseAllot',
    method:'post',
    data:obj,
    headers: {"Content-Type":"application/json"}
  })
}

/**
 * 库存调拨详情
 * @param query
 */
export function queryEnterWarehouseDetail(query) {
  return request({
    url: '/api/wms/scmWarehouseAllot/queryWarehouseAllotDetails',
    method: 'post',
    data: query,
    headers: {"Content-Type":"application/json"}
  })
}
//编辑库存调拨
export function editWarehouseAllot(obj){
  return request({
    url:'/api/wms/scmWarehouseAllot/editWarehouseAllot',
    method:'post',
    data:obj,
    headers: {"Content-Type":"application/json"}
  })
}


export function addObj(obj) {
  return request({
    url: '/purchase/purchasetask',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/purchase/purchasetask/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/purchase/purchasetask/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/purchase/purchasetask',
    method: 'put',
    data: obj
  })
}
