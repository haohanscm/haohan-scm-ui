/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/wms/warehouseinventory/page',
    method: 'get',
    params: query
  })
}
//获取库存商品列表
export function goodsList(query) {
  return request({
    url: '/product/productinfo/page',
    method: 'get',
    params: query
  })
}

/**
 * 查询库存商品详情
 * @param query
 */
export function queryInventoryDetail(query) {
  return request({
    url: '/wms/api/wms/scmWarehouseInventory/queryInventoryDetail',
    method: 'get',
    params: query,
  })
}
//新增库存商品
export function addWarehouseInventory(obj){
  return request({
    url:'/wms/api/wms/scmWarehouseInventory/addWarehouseInventory',
    method:'post',
    data:obj,
    headers: {"Content-Type":"application/json"}
  })
}
//编辑入库单
export function editWarehouseInventory(data){
  return request({
    url:'/wms/api/wms/scmWarehouseInventory/editWarehouseInventory',
    method:'post',
    data:data,
    headers: {"Content-Type":"application/json"}
  })
}




export function addObj(obj) {
  return request({
    url: '/purchase/purchasetask',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/purchase/purchasetask/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/purchase/purchasetask/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/purchase/purchasetask',
    method: 'put',
    data: obj
  })
}
