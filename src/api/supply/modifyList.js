/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
//pds客户采购订单明细修改
import request from '@/router/axios'

export function modifyList(query) {
  return request({
    url: '/saleb/api/saleb/buyOrderDetail/modifyDetail',
    method: 'post',
    params: query
  })
}


