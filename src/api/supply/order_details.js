import request from '@/router/axios'

export function orderDetails(supplySn) {
    return request({
        url:'/supply/api/supply/order/info',
        method: 'get',
        params: {supplySn:supplySn}
    })
}
