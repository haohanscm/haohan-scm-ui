import request from '@/router/axios'

export function orderList(params) {
    return request({
        url:'/supply/supplyorder/page',
        method: 'get',
        params: params
    })
}
export function detailsList(params) {
    return request({
        url:'/supply/api/supply/order/info',
        method: 'get',
        params: params
    })
}
export function supplyConfirm(data) {
    return request({
        url:'/supply/api/supply/order/confirm',
        method: 'post',
        params: data
    })
}
export function supplyInfo(params) {
    return request({
        url:'/supply/api/supply/order/buyInfo',
        method: 'get',
        params: params
    })
}
/**
 * 通过订单sn查询发货记录详情
 */
export function fetchInfoByOrderSn(params) {
    return request({
        url: '/opc/api/opc/shipManage/record/fetchInfoByOrderSn',
        method: 'get',
        params: params
    })
}