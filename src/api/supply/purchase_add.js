import request from '@/router/axios'
export function purchaseaddOrder(query, isBatch = false) {
    if (isBatch) {
        return request({
            url: '/supply/api/supply/order/addByBuyOrderBatch',
            method: 'post',
            data: query
        })
    } else {
        return request({
            url: '/supply/api/supply/order/addByBuyOrder',
            method: 'post',
            params: query
        })
    }
}
export function purchaseRelation(buyOrderSn) {
    return request({
        url: '/supply/api/supply/order/buyOrder/relation',
        method: 'get',
        params: {buyOrderSn:buyOrderSn}
    })
}
