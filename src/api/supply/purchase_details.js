/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'


export function purchaseDetails(params) {
    return request({
        url: '/saleb/api/saleb/buyOrder/info',
        method: 'get',
        params: params
    })
}
// 获取商品
export function platformModel(params) {
    return request({
        url: '/supply/api/supply/goods/platformModel',
        method: 'get',
        params: params
    })
}
// 获取关联供应订单信息
export function supplyOrderRelation(params) {
    return request({
        url: '/supply/api/supply/order/buyOrder/relation',
        method: 'get',
        params: params
    })
}
