/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'


export function purchaseOrder(query) {
    return request({
        url: '/saleb/buyorder/page',
        method: 'get',
        params: query
    })
}





//以下用不上
export function addObj(obj) {
    return request({
        url: '/saleb/buyorder',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/saleb/buyorder/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/saleb/buyorder/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/saleb/buyorder',
        method: 'put',
        data: obj
    })
}

export function updDetailList(list) {
    return request({
        url: '/saleb/api/saleb/buyOrder/updBuyOrderDetailList',
        method: 'post',
        data: list,
        headers: {"Content-Type":"application/json"}
    })
}

export function cancelOrder(req) {
    return request({
        url: '/saleb/api/saleb/buyOrder/cancelOrder',
        method: 'post',
        data: req,
        headers: {"Content-Type":"application/json"}
    })
}

export function confirmBuyOrder(obj) {
    return request({
        url: '/saleb/api/saleb/buyOrder/confirmBuyOrder',
        method: 'post',
        data: obj,
    })
}
//根据状态查询订单数量
export function countBuyOrder(query) {
    return request({
        url: '/saleb/buyorder/countByBuyOrderReq',
        method: 'post',
        data: query,
    })
}

// 修改B客户订单
export function modifyBuyOrder(obj) {
    return request({
        url: '/saleb/api/saleb/buyOrder/modify',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

// 批量创建应收账单
export function createBillBatch(params) {
    return request({
        url: '/saleb/api/saleb/buyOrder/createBillBatch',
        method: 'post',
        params: params,
    })
}

// 应收账单重建
export function resetBill(params) {
    return request({
        url: '/saleb/api/saleb/buyOrder/resetBill',
        method: 'post',
        params: params,
    })
}
//客户订单table导入
export function importTable(params) {
    return request({
      url: '/saleb/api/saleb/excel/importTable',
      method: 'post',
      data: params,
      headers: {"Content-Type":"application/json"}
    })
  }
  //客户采购订单table导入商品明细
  export function importDetail(params) {
    return request({
      url: '/saleb/api/saleb/excel/importDetail',
      method: 'post',
      data: params,
      headers: {"Content-Type":"application/json"}
    })
  }