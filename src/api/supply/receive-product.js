// 获取商品
export function receiveProduct(goodsModelId) {
    return request({
        url: '/supply/api/supply/goods/platformModel',
        method: 'get',
        params: {goodsModelId:goodsModelId}
    })
}
