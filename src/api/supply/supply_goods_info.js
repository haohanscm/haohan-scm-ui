import request from '@/router/axios'
//获取详情供应商品数据
export function getgoodsinfo(params) {
  return request({
    url: "/supply/api/supply/goods/info",
    method: 'get',
    params: params
  })
}
//查询商品规格列表(带类型名称)
export function getgoodslist(params) {
  return request({
    url: "/goods/api/goods/manage/fetchModelInfoPage",
    method: 'get',
    params: params
  })
}
//关联供应商品规格到平台
export function bindgoods(params) {
  return request({
    url: "/supply/api/supply/goods/relation",
    method: 'post',
    params: params
  })
}
//取消关联的供应商品规格
export function cancelrelategoods(params) {
  return request({
    url: "/supply/api/supply/goods/relation",
    method: 'delete',
    params: params
  })
}
