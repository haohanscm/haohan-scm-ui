import Layout from '@/page/index/'

const settlementRouter = {
  path: '/settlement',
  redirect: '/settlement/settlementList',
  component: Layout,
  meta: {title: '结算单'},
  children: [
      {
        path: 'settlementList',
        component: () => import('./index.vue'),
        name: '结算单列表',
        meta: {title: '结算单列表'}
      },
      {
        path: 'settlementDetail',
        component: () => import('./detail.vue'),
        name: '结算单详情',
        meta: {title: '结算单详情'}
      },
      {
        path: 'settlementFinish',
        component: () => import('./finish.vue'),
        name: '完成结算',
        meta: {title: '完成结算'}
      },
  ]
}

export default settlementRouter;
