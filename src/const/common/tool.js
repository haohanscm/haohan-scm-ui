/**
 * @param {*} cascader 需要转换的数值
 * @param {*} label 需要转换为label的属性
 * @param {*} value 需要转换为value的属性
 */
export function formatCascader(cascader = [], label, value) {
    let newCascader = new Array();
    if(!Array.isArray(cascader)) return newCascader;
    return initCascaderData(cascader, label, value);
}

function initCascaderData(list = [], label, value) {
    list.map(item => {
        item['label'] = item[label];
        item['value'] = item[value];
        // item['label'] = item.areaName;
        // item['value'] = item.id;
        if(Array.isArray(item.children) && item.children.length > 0) initCascaderData(item.children, label, value);
    })
    return list;
}

/**
 * @param {*} data 字典状态数组
 * @param {*} value 状态值
 */
export function getStatusName(data, value) {
    let label = '-';
    if(!Array.isArray(data)) return label;
    data.map(item => {
        if(item.value === value) label = item.label;
    });
    return label;
}

// 需要处理的行政区列表
export const regionList = ['重庆市', '北京市', '上海市', '天津市', '香港特别行政区', '澳门特别行政区'];

/**
 * @param {*} location 坐标
 */
export function getLocation(data) {
    // 默认坐标重庆市
    let location = [106.55046400000003, 29.563761];
    if(data && typeof data === 'string') {
        location = [Number(data.split(',')[0]),  Number(data.split(',')[1])];
    }
    return location;
}