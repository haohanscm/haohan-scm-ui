/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '售后单编号',
      prop: 'afterSalesSn'
    },
	  {
      label: '售后金额',
      prop: 'afterSalesAmount'
    },
	  {
      label: '联系人名称',
      prop: 'linkManName'
    },
	  {
      label: '联系人电话',
      prop: 'linkManTelephone'
    },
	  {
      label: '售后订单类型',
      prop: 'afterOrderType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_after_order_type',
      search: true
    },
	  {
      label: '供应商报价单编号',
      prop: 'offerOrderId'
    },
	  {
      label: 'B客户采购单编号',
      prop: 'buyId'
    },
	  {
      label: 'C客户零售单编号',
      prop: 'goodsOrderId'
    },
	  {
      label: '处理结果',
      prop: 'resultDesc'
    },
	  {
      label: '售后状态',
      prop: 'afterSalesStatus',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_after_sales_status',
      search: true
    },
	  {
      label: '申请时间',
      prop: 'applyTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '处理时间',
      prop: 'actionTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '完成时间',
      prop: 'finishTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
