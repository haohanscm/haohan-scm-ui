/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '售后单编号',
      prop: 'afterSalesSn'
    },
	  {
      label: '售后状态',
      prop: 'afterSalesStatus',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_after_sales_status',
      search: true
    },
	  {
      label: '售后类型',
      prop: 'afterType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_after_type',
      search: true
    },
	  {
      label: '申请人名称',
      prop: 'applicantName'
    },
	  {
      label: '售后申请描述',
      prop: 'applyDesc'
    },
	  {
      label: '售后申请图片组编号',
      prop: 'photoGroupNum'
    },
	  {
      label: '复核人名称',
      prop: 'reviewerName'
    },
	  {
      label: '反馈描述',
      prop: 'reviewDesc'
    },
	  {
      label: '申请时间',
      prop: 'applyTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '复核时间',
      prop: 'reviewTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
