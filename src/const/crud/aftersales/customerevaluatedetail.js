/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '评分名称',
      prop: 'evaluateName'
    },
	  {
      label: '分值',
      prop: 'score'
    },
	  {
      label: '评分类型',
      prop: 'evaluateType',
      type: 'select',
      dicUrl: '/admin/dict/type/evaluate_type',
      search: true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
