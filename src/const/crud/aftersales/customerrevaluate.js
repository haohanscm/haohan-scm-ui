/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '应用名称',
      prop: 'appName'
    },
	  {
      label: '商家名称',
      prop: 'merchantName'
    },
	  {
      label: '服务类型',
      prop: 'serviceType',
      type: 'select',
      dicUrl: '/admin/dict/type/use_status',
      search: true
    },
	  {
      label: '评分人姓名',
      prop: 'evaluateName'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
