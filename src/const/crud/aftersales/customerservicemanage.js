/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商家ID',
      prop: 'merchant'
    },
	  {
      label: '服务类型',
      prop: 'serviceType',
      type: 'select',
      dicUrl: '/admin/dict/type/use_status',
      search: true
    },
	  {
      label: '服务内容',
      prop: 'serviceContent'
    },
	  {
      label: '服务描述',
      prop: 'serviceDesc'
    },
	  {
      label: '服务时间',
      prop: 'serviceTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '支付方式',
      prop: 'payType',
      type: 'select',
      dicUrl: '/admin/dict/type/pay_type',
      search: true
    },
	  {
      label: '收费',
      prop: 'payAmount'
    },
	  {
      label: '支付时间',
      prop: 'payTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '业务专管员',
      prop: 'bizUser'
    },
	  {
      label: '运营专管员',
      prop: 'opUser'
    },
	  {
      label: '技术负责人',
      prop: 'techUser'
    },
	  {
      label: '财务核对人',
      prop: 'financeUser'
    },
	  {
      label: '服务情况',
      prop: 'serviceInfo'
    },
	  {
      label: '服务状态',
      prop: 'serviceStatus'
    },
	  {
      label: '客户评分',
      prop: 'custormEvaluate'
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
