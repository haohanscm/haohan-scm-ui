/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    title: '订单商品明细',
    page: false,
    border: true,
    index: true,
    indexWidth: 50,
    indexLabel: "序号",
    stripe: true,
    menu: false,
    header: false,
    align: "center",
    column: [
        {
            type: 'input',
            label: '商品',
            prop: 'goodsName',
        },
        {
            type: 'input',
            label: '规格',
            prop: 'modelName',

        },
        {
            type: 'input',
            label: '单位',
            prop: 'unit',

        },
        {
            label: '商品图片',
            prop: 'goodsImg',
            type:'img',
            dataType: 'string',
        },
        {
            type: 'input',
            label: '市场价格',
            prop: 'marketPrice',

        },
        {
            type: 'input',
            label: '成交价格',
            prop: 'dealPrice',

        },
        {
            type: 'input',
            label: '成交数量',
            prop: 'goodsNum',

        },
        {
            type: "number",
            label: '商品金额',
            prop: 'amount',
            precision: 2,
            valueDefault: 0,
        },

    ]
}
