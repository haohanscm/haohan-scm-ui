/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    type: 'input',
                    label: '账单编号',
                    prop: 'billSn',
                    span: 4
                },
                {
                    label: '审核状态',
                    prop: 'reviewStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_review_status',
                    // valueDefault: '1',
                    span: 4
                },
                {
                    label: '账单类型',
                    prop: 'billType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_payable_bill_type',
                    span: 4
                },
                {
                    type: 'input',
                    label: '结算单编号',
                    prop: 'settlementSn',
                    span: 4
                },
                {
                    label: '结算状态',
                    prop: 'settlementStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_yes_no',
                    span: 4
                }
            ]
        }, {
            icon: 'el-icon-info',
            label: '金额相关',
            prop: 'group2',
            column: [
                {
                    type: "number",
                    label: '账单金额',
                    prop: 'billAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 6
                },
                {
                    type: "number",
                    label: '订单金额',
                    prop: 'orderAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 6
                },
                {
                    label: '是否预付账单',
                    prop: 'advanceFlag',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_yes_no',
                    span: 6
                },
                {
                    type: "number",
                    label: '预付金额',
                    prop: 'advanceAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 6
                },
                {
                    type: "textarea",
                    label: "备注信息",
                    prop: "remarks",
                    maxlength: 255,
                    span: 12,
                    showWordLimit: true
                }
            ]
        },
        {
            icon: 'el-icon-info',
            label: '订单相关',
            prop: 'group3',
            column: [
                {
                    type: 'input',
                    label: '来源订单编号',
                    prop: 'orderSn',
                    span: 8
                },
                {
                    label: '订单类型',
                    prop: 'orderType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_order_type',
                    span: 8
                },
                {
                    type: 'date',
                    label: '订单成交日期',
                    prop: 'dealDate',
                    format: "yyyy-MM-dd",
                    span: 8
                },
                {
                    type: 'input',
                    label: '平台商家名称',
                    prop: 'pmName',
                    span: 8
                },
                {
                    type: 'input',
                    label: '下单客户名称',
                    prop: 'customerName',
                    span: 8
                },
                {
                    type: 'input',
                    label: '客户商家名称',
                    prop: 'merchantName',
                    span: 8
                },

                {
                    type: "number",
                    label: '总金额',
                    prop: 'totalAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 8
                },
                {
                    type: "number",
                    label: '商品合计',
                    prop: 'sumAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 8
                },
                {
                    type: "number",
                    label: '其他金额',
                    prop: 'otherAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 8
                },
            ]
        }
    ]
}
