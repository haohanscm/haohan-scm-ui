/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    type: 'input',
                    label: '结算单编号',
                    prop: 'settlementSn',
                    span: 4
                },
                {
                    type: 'input',
                    label: '平台商家',
                    prop: 'pmName',
                    span: 4
                },
                {
                    type: 'input',
                    label: '结算公司',
                    prop: 'companyName',
                    span: 4
                },
                {
                    type: 'input',
                    label: '订单编号',
                    prop: 'orderSn',
                    span: 4
                },
                {
                    type: 'input',
                    label: '账单编号',
                    prop: 'billSn',
                    span: 4
                },
            ]
        },
        {
            icon: 'el-icon-info',
            label: '结算相关',
            prop: 'group2',
            column: [
                {
                    label: '结算类型',
                    prop: 'settlementType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_settlement_type',
                    span: 8
                },
                {
                    type: "number",
                    label: '结算金额',
                    prop: 'settlementAmount',
                    precision: 2,
                    valueDefault: 0,
                    span: 8
                },
                {
                    label: '是否结算',
                    prop: 'settlementStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_yes_no',
                    span: 8
                },
                {
                    label: '结算单类型',
                    prop: 'settlementStyle',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_settlement_style',
                    span: 8
                },
                {
                    type: 'input',
                    label: '汇总结算单编号',
                    prop: 'summarySettlementSn',
                    span: 8
                }
            ]
        },
    ]
}
