/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        // {
        //     label: '主键',
        //     prop: 'id'
        // },
        {
            label: '账单编号',
            prop: 'billSn',
            minWidth:100,
            search: true
        },
        {
            label: '审核状态',
            prop: 'reviewStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_review_status',
            search: true,
        },
        {
            label: '账单类型',
            prop: 'billType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_payable_bill_type',
            search: true,
        },
        {
            label: '结算单编号',
            prop: 'settlementSn',
            minWidth:100,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '结算状态',
            prop: 'settlementStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true,
        },
        {
            label: '平台商家ID',
            prop: 'pmId',
            hide: true,
        },
        {
            label: '平台商家名称',
            prop: 'pmName',
            minWidth:100,
            overHidden: true,
        },
        {
            label: '来源订单编号',
            prop: 'orderSn',
            search: true,
            minWidth:100,
            searchLabelWidth: 100
        },
        {
            label: '下单客户id',
            prop: 'customerId',
            hide: true,
        },
        {
            label: '下单客户名称',
            prop: 'customerName',
            minWidth:100,
            overHidden: true,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '客户商家id',
            prop: 'merchantId',
            hide: true,
        },
        {
            label: '客户商家名称',
            prop: 'merchantName',
            minWidth:100,
            overHidden: true,
            search: true,
            searchLabelWidth: 100
        },
        {
            "label": "订单成交日期",
            "prop": "dealDate",
            minWidth:120,
            search: true,
            sortable: true,
            searchLabelWidth: 100,
            searchSpan: 12,
            searchslot:true
        },
        {
            label: '账单金额',
            prop: 'billAmount',
        },
        {
            label: '订单金额',
            prop: 'orderAmount',
        },
        {
            label: '是否预付账单',
            prop: 'advanceFlag',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '预付金额',
            prop: 'advanceAmount',
        },
        {
            label: '备注信息',
            prop: 'remarks',
            width: 200,
            overHidden: true,
        }
    ]
}
