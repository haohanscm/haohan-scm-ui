/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    menuWidth: 200,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    selection: true,
    column: [
        {
            label: '结算单编号',
            prop: 'settlementSn',
            minWidth: 100,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '平台商家ID',
            prop: 'pmId',
            hide: true,
        },
        {
            label: '平台商家名称',
            prop: 'pmName',
            minWidth:100,
            overHidden: true,
        },
        {
            label: '结算公司id',
            prop: 'companyId',
            hide: true,
        },
        {
            label: '结算公司名称',
            prop: 'companyName',
            minWidth:100,
            overHidden: true,
        },
        {
            label: '账单编号',
            prop: 'billSn',
            minWidth: 100,
            search: true
        },
        {
            label: '来源订单编号',
            prop: 'orderSn',
            search: true,
            minWidth: 100,
            searchLabelWidth: 100
        },
        {
            label: '结算类型',
            prop: 'settlementType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_settlement_type',
            search: true,
        },
        {
            label: '结算金额',
            prop: 'settlementAmount',
        },
        {
            "label": "结算时间",
            "prop": "settlementTime",
            "type": "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            minWidth: 120,
            search: true,
            sortable: true,
            searchLabelWidth: 100,
            searchSpan: 12,
            searchslot:true
        },
        {
            label: '付款方式',
            prop: 'payType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_pay_type',
            search: true,
        },
        {
            label: '是否结算',
            prop: 'settlementStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true,
        },
        {
            label: '结算单类型',
            prop: 'settlementStyle',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_settlement_style',
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '汇总结算单编号',
            prop: 'summarySettlementSn',
        },
        {
            label: '结款人名称',
            prop: 'companyOperator',
        },
        {
            label: '经办人名称',
            prop: 'operatorName',
        },
        {
            label: '结算说明',
            prop: 'settlementDesc',
            width: 200,
            overHidden: true,
        }
    ]
}
