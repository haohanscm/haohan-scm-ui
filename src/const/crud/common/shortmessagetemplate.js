/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '模板id',
      prop: 'templateId'
    },
	  {
      label: '应用id',
      prop: 'appId'
    },
	  {
      label: '短信模板类型:1验证码2通知3推广',
      prop: 'shortTemplateType'
    },
	  {
      label: '消息类型:不同使用类型',
      prop: 'shortMsgType'
    },
	  {
      label: '模板名称',
      prop: 'templateName'
    },
	  {
      label: '模板内容',
      prop: 'templateContent'
    },
	  {
      label: '短信签名',
      prop: 'signName'
    },
	  {
      label: '模板说明',
      prop: 'templateDesc'
    },
	  {
      label: '启用状态:0.未启用1.启用',
      prop: 'useStatus'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
