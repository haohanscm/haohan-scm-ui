/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '预警记录编号',
      prop: 'warningSn'
    },
	  {
      label: '查看时间',
      prop: 'lookTime'
    },
	  {
      label: '消息状态:1待发送2已发送3已查看',
      prop: 'messageStatus'
    },
	  {
      label: '接收人uid',
      prop: 'receiverUid'
    },
	  {
      label: '接收人名称',
      prop: 'receiverName'
    },
	  {
      label: '发送时间',
      prop: 'sendTime'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
