/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '微信消息编号',
      prop: 'wechatMsgSn'
    },
	  {
      label: '业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户',
      prop: 'departmentType'
    },
	  {
      label: '消息状态:1待发送2已发送3已查看',
      prop: 'messageStatus'
    },
	  {
      label: '业务类型:各部门的消息',
      prop: 'msgActionType'
    },
	  {
      label: '接收人uid',
      prop: 'receiverUid'
    },
	  {
      label: '接收人名称',
      prop: 'receiverName'
    },
	  {
      label: '发送时间',
      prop: 'sendTime'
    },
	  {
      label: '消息内容',
      prop: 'content'
    },
	  {
      label: '应用id',
      prop: 'appId'
    },
	  {
      label: '使用模板id',
      prop: 'templateId'
    },
	  {
      label: '请求参数',
      prop: 'reqParams'
    },
	  {
      label: '返回参数',
      prop: 'respParams'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
