/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '消息ID',
      prop: 'msgId'
    },
	  {
      label: '授权微信名称',
      prop: 'openWxName'
    },
	  {
      label: '授权微信ID',
      prop: 'openWxId'
    },
	  {
      label: '微信ID',
      prop: 'wxId'
    },
	  {
      label: '微信名称',
      prop: 'wxName'
    },
	  {
      label: '微信类型',
      prop: 'wxType'
    },
	  {
      label: '用户通行证ID',
      prop: 'passportUid'
    },
	  {
      label: '用户ID',
      prop: 'openUid'
    },
	  {
      label: '用户昵称',
      prop: 'nickName'
    },
	  {
      label: '用户头像',
      prop: 'albumUrl'
    },
	  {
      label: '用户openid',
      prop: 'openId'
    },
	  {
      label: '用户unionid',
      prop: 'unionId'
    },
	  {
      label: '消息类型',
      prop: 'msgType'
    },
	  {
      label: '消息名称',
      prop: 'msgName'
    },
	  {
      label: '消息内容',
      prop: 'msgContent'
    },
	  {
      label: '发送时间',
      prop: 'sendTime'
    },
	  {
      label: '完整消息包',
      prop: 'fullMsgPkg'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
