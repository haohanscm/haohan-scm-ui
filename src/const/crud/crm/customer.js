/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '客户编号',
      prop: 'customerSn',
      width: 110
    },
	  {
      label: '客户名称',
      prop: 'customerName',
      width: 110
    },
	  {
      label: '销售区域',
      prop: 'areaSn',
      width:110
    },
	  {
      label: '客户联系人',
      prop: 'contact',
      width: 110
    },
	  {
      label: '联系手机',
      prop: 'telephone',
      width:120
    },
	  {
      label: '座机电话',
      prop: 'phoneNumber',
      width:110
    },
	  {
      label: '详细地址',
      prop: 'address',
      overHidden:true,
      width:110
    },
	  // {
    //   label: '客户地址定位',
    //   prop: 'position',
    //   width: 110
    // },
	  {
      label: '客户标签',
      prop: 'tags',
      overHidden:true,
      tableOption
    },
	  {
      label: '邮编',
      prop: 'postcode'
    },
	  {
      label: '营业时间',
      prop: 'serviceTime'
    },
	  {
      label: '门头照',
      prop: 'pictureUrl'
    },
	  {
      label: '经营面积',
      prop: 'operateArea'
    },
	  {
      label: '店铺名称',
      prop: 'shopName'
    },
	  {
      label: '年经营流水',
      prop: 'shopSale',
      width:110
    },
	  {
      label: '业务介绍',
      prop: 'bizDesc'
    },
	  {
      label: '营业执照',
      prop: 'bizLicense'
    },
	  {
      label: '营业执照名称',
      prop: 'licenseName',
      width: 110
    },
	  {
      label: '工商注册号',
      prop: 'registrationNum',
      width: 110
    },
	  {
      label: '注册日期',
      prop: 'registrationDate'
    },
	  {
      label: '客户注册全称',
      prop: 'regName',
      width: 110
    },
	  {
      label: '经营法人名称',
      prop: 'legalName',
      width: 110
    },
	  {
      label: '经营地址',
      prop: 'regAddress'
    },
	  {
      label: '收录时间',
      prop: 'initTime'
    },
	  {
      label: '是否审核',
      prop: 'status'
    },
	  {
      label: '客户类型:1经销商2门店',
      prop: 'customerType'
    },
	  {
      label: '客户负责人id',
      prop: 'directorId'
    },
	  {
      label: '公司性质:1.个体2.民营3.国有4.其他',
      prop: 'companyNature'
    },
	  {
      label: '客户级别:1星-5星',
      prop: 'customerLevel'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
