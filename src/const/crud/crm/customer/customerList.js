
export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  indexWidth: 50,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn: false,
  editBtn: false,
  detailBtn: false,
  column: [
    {
      label: '主键',
      prop: 'id',
      hide: true
    },
    {
      label: '客户编号',
      prop: 'customerSn',
      width: 100,
      search: true
    },
    {
      label: '客户名称',
      prop: 'customerName',
      width: 100,
      search: true
    },
    {
      label: '客户状态',
      prop: 'status',
      dicUrl: '/admin/dict/type/customer_status',
      type: 'select',
      search: true
    },
    {
      label: '客户类型',
      prop: 'customerType',
      dicUrl: '/admin/dict/type/scm_customer_type',
      type: 'select',
      search: true
    },
    {
      label: '客户级别',
      prop: 'customerLevel',
      dicUrl: '/admin/dict/type/scm_customer_level',
      type: 'select',
      search: true
    },
    {
      label: '公司性质',
      prop: 'companyNature',
      dicUrl: '/admin/dict/type/scm_company_nature',
      type: 'select',
      search: true
    },
    {
      label: '客户经理',
      prop: 'directorName'
    },
    {
      label: '客户联系人',
      prop: 'contact',
      width: 100
    },
    {
      label: '联系手机',
      prop: 'telephone',
      width: 120,
      search: true
    },
    {
      label: '座机电话',
      prop: 'phoneNumber',
      width: 120
    },
    {
      label: '客户标签',
      prop: 'tags',
      search: true
    },
    {
      label: '销售区域',
      prop: 'areaName'
    },
    {
      label: '销售区域',
      prop: 'areaSn',
      hide: true,
      search: true,
      searchslot: true
    },
    {
      label: '市场',
      prop: 'marketName'
    },
    {
      label: '市场',
      prop: 'marketSn',
      hide: true,
      search: true,
      searchslot: true
    },
    {
      label: '详细地址',
      prop: 'address',
      width: 180,
      overHidden: true,
      search: true

    },
    {
      label: '是否标注定位',
      prop: 'positionFlag',
      type: 'select',
      search: true,
      dicData: [
        {
          label: '是',
          value: true
        },
        {
          label: '否',
          value: false
        }],
      searchLabelWidth: 100
    },
    {
      label: '客户地址定位',
      prop: 'position',
      hide: true
    },

    {
      label: '邮编',
      prop: 'postcode',
      hide: true
    },
    {
      label: '营业时间',
      prop: 'serviceTime',
      hide: true
    },
    {
      label: '门头照',
      prop: 'pictureUrl',
      hide: true
    },
    {
      label: '经营面积',
      prop: 'operateArea',
      hide: true
    },
    // {
    //   label: '店铺名称',
    //   prop: 'shopName'
    // },
    {
      label: '年经营流水',
      prop: 'shopSale',
      hide: true
    },
    {
      label: '业务介绍',
      prop: 'bizDesc',
      hide: true
    },
    {
      label: '营业执照',
      prop: 'bizLicense',
      hide: true
    },
    {
      label: '营业执照名称',
      prop: 'licenseName',
      hide: true
    },
    {
      label: '工商注册号',
      prop: 'registrationNum',
      hide: true
    },
    {
      label: '注册日期',
      prop: 'registrationDate',
      hide: true
    },
    {
      label: '客户注册全称',
      prop: 'regName',
      hide: true
    },
    {
      label: '经营法人名称',
      prop: 'legalName',
      hide: true
    },
    {
      label: '经营地址',
      prop: 'regAddress',
      hide: true
    },
    {
      label: '收录时间',
      prop: 'initTime'
    },
    {
      label: '客户经理',
      prop: 'directorId',
      hide: true,
      search: true,
      searchslot: true
    },
    {
      label: '创建者',
      prop: 'createBy',
      hide: true
    },
    {
      label: '创建时间',
      prop: 'createDate',
      format: 'yyyy-MM-dd HH:mm:ss',
      width: 180,
      searchSpan: 12,
      search: true,
      searchslot: true
    },
    {
      label: '更新者',
      prop: 'updateBy',
      hide: true
    },
    {
      label: '更新时间',
      prop: 'updateDate',
      hide: true
    },
    {
      label: '备注信息',
      prop: 'remarks'
    }
    // {
    //   label: '删除标记',
    //   prop: 'delFlag',
    //   hide:true
    // },
    // {
    //   label: '租户id',
    //   prop: 'tenantId',
    //   hide:true
    // },
  ]
}
