/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    viewBtn:true,
    menu:false,
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '提交时间',
            prop: 'picName',
            search: true
        },
        {
            label: '提交人姓名',
            prop: 'picUrl',
            search: true
        },
        {
            label: '提交人账号',
            prop: 'picType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/pic_type'
        },
        {
            label: '提交人部门',
            prop: 'picSize'
        },
        {
            label: '客户名称',
            prop: 'picFrom'
        },
        {
            label: '客户编码',
            prop: 'status'
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '操作',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '操作内容',
            prop: 'remarks'
        },
        {
            label: '操作内容',
            prop: 'delFlag'
        },
        {
          label: '审批状态',
          prop: 'delFlag'
        },
        /*
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
