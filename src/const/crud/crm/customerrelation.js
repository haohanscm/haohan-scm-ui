/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '客户编号',
      prop: 'customerSn'
    },
	  {
      label: '员工id',
      prop: 'employeeId'
    },
	  {
      label: '关系建立时间',
      prop: 'startTime'
    },
	  {
      label: '关系结束时间',
      prop: 'endTime'
    },
	  {
      label: '关系状态:1.维持中2.已结束',
      prop: 'relationStatus'
    },
	  {
      label: '采购商id',
      prop: 'buyerId'
    },
	  {
      label: '销售分润比例',
      prop: 'salesRate'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
