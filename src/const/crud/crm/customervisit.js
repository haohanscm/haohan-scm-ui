/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '主键',
            prop: 'id',
            hide: true
        },
        {
            label: '客户编号',
            prop: 'customerSn'
        },
        {
            label: '客户名称',
            prop: 'customerName',
            search: true
        },
        {
            label: '拜访地址',
            prop: 'visitAddress'
        },
        {
            label: '拜访联系人id',
            prop: 'linkmanId',
            hide: true
        },
        {
            label: '拜访日期',
            prop: 'visitDate',
            minWidth: 100,
            sortable: true,
            type: 'date',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            search: true,
            searchRange: true,
            searchSpan: 12,
        },
        {
            label: '拜访内容',
            prop: 'visitContent'
        },
        {
            label: '进展阶段',//进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来
            prop: 'visitStep',
            dicUrl: '/admin/dict/type/scm_visit_step'
        },
        {
            label: '拜访图片组编号',
            prop: 'photoNum',
            hide: true
        },
        {
            label: '拜访员工id',
            prop: 'employeeId',
            hide: true
        },
        {
            label: '下次回访时间',
            prop: 'nextVisitTime',
            overHidden: true
        },
        {
            label: '客户状态:0.无1潜在2.有意向3成交4失败',
            prop: 'customerStatus',
            hide: true
        },
        {
            label: '执行状态:1开始2进行中3完成',
            prop: 'visitStatus',
            hide: true
        },
        {
            label: '拜访总结',
            prop: 'summaryContent'
        },
        {
            label: '抵达时间',
            prop: 'arrivalTime',
            overHidden: true
        },
        {
            label: '离开时间',
            prop: 'departureTime',
            overHidden: true
        },
        {
            label: '创建者',
            prop: 'createBy',
            hide: true
        },
        {
            label: '创建时间',
            prop: 'createDate',
            hide: true
        },
        {
            label: '更新者',
            prop: 'updateBy',
            hide: true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            hide: true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        {
            label: '删除标记',
            prop: 'delFlag',
            hide: true
        },
        {
            label: '租户id',
            prop: 'tenantId',
            hide: true
        },
    ]
}
