/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '动态编号',
      prop: 'reportSn'
    },
	  {
      label: '客户名称',
      prop: 'goodsModelId'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '规格属性',
      prop: 'modelAttr'
    },
	  {
      label: '单位',
      prop: 'goodsUnit'
    },
	  {
      label: '商品编码',
      prop: 'goodsSn'
    },
	  {
      label: '条形码',
      prop: 'goodsModelCode'
    },
	  {
      label: '批发价',
      prop: 'tradePrice'
    },
	  {
      label: '保质期',
      prop: 'expirationDate'
    },
	  {
      label: '生产日期',
      prop: 'productDate'
    },
	  {
      label: '到期日期',
      prop: 'maturityDate'
    },
	  {
      label: '数量',
      prop: 'goodsNum'
    },
	  {
      label: '操作人',
      prop: 'operatorName'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
