/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    "type": "input",
                    label: '员工姓名',
                    prop: 'name',
                    span: 8
                },
                {
                    "type": "number",
                    label: '手机',
                    prop: 'telephone',
                    precision: 0,
                    span: 8
                },
                {
                    label: '性别',
                    prop: 'sex',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_sex',
                    valueDefault: '1',
                    span: 8,
                    value: '1',
                },
                {
                    label: '员工类型',
                    prop: 'employeeType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_market_employee_type',
                    valueDefault: '3',
                    span: 8,
                    value: '3',
                },
                {
                    label: '启用状态',
                    prop: 'useStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_use_status',
                    valueDefault: '1',
                    span: 8,
                    value: '1',
                },
                {
                    label: '头像',
                    prop: 'avatar',
                    type:'input',
                    formslot: true,
                    span: 12
                },

            ]
        }, {
            icon: 'el-icon-info',
            label: '扩展信息',
            prop: 'group2',
            "column": [
                {
                    "type": "input",
                    "label": "部门",
                    "prop": "deptId",
                    formslot: true,
                    span: 8
                }, {
                    "type": "input",
                    "label": "权限角色",
                    "prop": "roleIds",
                    formslot: true,
                    span: 8
                }, {
                    "type": "input",
                    "label": "关联区域",
                    "prop": "areaSns",
                    formslot: true,
                    span: 8
                },
                {
                    "type": "input",
                    label: '职位',
                    prop: 'post',
                    span: 12
                },
                {
                    "type": "number",
                    label: '默认分润比例',
                    prop: 'defaultRate',
                    precision: 2,
                    valueDefault: 0,
                    span: 8,
                    display:false
                },
                {
                    "type": "textarea",
                    "label": "备注信息",
                    "prop": "remarks",
                    maxlength: 255,
                    span: 12,
                    showWordLimit: true
                }

            ]
        }]
}
