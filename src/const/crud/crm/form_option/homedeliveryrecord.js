/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    card: true,
    group: [
        {
            icon: 'el-icon-info',
            label: '客户相关',
            prop: 'group1',
            "column": [
                {
                    "type": "input",
                    "label": "客户",
                    "prop": "customerSn",
                    formslot: true,
                    span: 8
                },
                {
                    "type": "input",
                    "label": "客户联系人",
                    "prop": "linkmanId",
                    formslot: true,
                    span: 8
                }, {
                    "type": "textarea",
                    "label": "补充说明",
                    "prop": "description",
                    maxlength: 255,
                    span: 8,
                    showWordLimit: true
                }]
        }, {
            icon: 'el-icon-info',
            label: '支付相关',
            prop: 'group2',
            "column": [
                {
                    "type": "radio",
                    "label": "会员类型",
                    "prop": "memberType",
                    dicUrl: '/admin/dict/type/scm_member_type',
                    valueDefault: '1',
                    span: 8
                },
                {
                    "type": "radio",
                    "label": "付款方式",
                    "prop": "paymentType",
                    dicUrl: '/admin/dict/type/scm_payment_type',
                    valueDefault: '1',
                    span: 8
                },
                {
                    "type": "number",
                    "label": "付款金额(元)",
                    "prop": "amount",
                    precision: 2,
                    valueDefault: 0,
                    span: 8
                }
            ]
        }, {
            icon: 'el-icon-info',
            label: '货物相关',
            prop: 'group3',
            "column": [

                {
                    "type": "number",
                    "label": "件数",
                    "prop": "goodsNum",
                    valueDefault: 1,
                    span: 6
                },
                {
                    "type": "number",
                    "label": "重量(kg)",
                    "prop": "weight",
                    precision: 2,
                    valueDefault: 1,
                    span: 6
                },
                {
                    "type": "radio",
                    "label": "配件类型",
                    "prop": "goodsType",
                    dicUrl: '/admin/dict/type/scm_delivery_goods_type',
                    valueDefault: '1',
                    span: 6
                },
                {
                    "type": "radio",
                    "label": "配送状态",
                    "prop": "deliveryStatus",
                    dicUrl: '/admin/dict/type/scm_delivery_goods_status',
                    valueDefault: '1',
                    span: 6
                },
                {
                    "type": "input",
                    "label": "配送物品",
                    "prop": "goodsTags",
                    formslot: true,
                    maxlength: 50,
                    showWordLimit: true,
                    span: 12
                },
                {
                    "type": "input",
                    "label": "其他服务",
                    "prop": "otherServices",
                    formslot: true,
                    maxlength: 50,
                    showWordLimit: true,
                    span: 12
                },
                {
                    "type": "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    valueFormat: "yyyy-MM-dd HH:mm:ss",
                    "label": "预约配送时间",
                    "prop": "appointmentTime",
                    span: 6
                }, {
                    "type": "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    valueFormat: "yyyy-MM-dd HH:mm:ss",
                    "label": "配送时间",
                    "prop": "deliveryTime",
                    span: 6
                }
            ]
        }, {
            icon: 'el-icon-info',
            label: '补充信息',
            prop: 'group4',
            "column": [

                {
                    "type": "input",
                    "label": "订单号",
                    "prop": "orderSn",
                    maxlength: 32,
                    showWordLimit: true,
                    span: 8
                },
                // {
                //     "type": "input",
                //     "label": "订单类型",
                //     "prop": "orderTye",
                // },
                {
                    "type": "input",
                    "label": "第三方订单号",
                    "prop": "thirdOrderSn",
                    maxlength: 32,
                    showWordLimit: true,
                    span: 8
                },

                {
                    "type": "textarea",
                    "label": "备注信息",
                    "prop": "remarks",
                    maxlength: 255,
                    span: 8,
                    showWordLimit: true
                }
            ]
        }
    ]


}
