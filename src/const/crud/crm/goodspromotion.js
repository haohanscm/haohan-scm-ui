/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '商品ID',
      prop: 'goodsModelId'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '规格属性',
      prop: 'modelAttr'
    },
	  {
      label: '市场价格',
      prop: 'marketPrice'
    },
	  {
      label: '促销价格',
      prop: 'promotionPrice'
    },
	  {
      label: '开始时间',
      prop: 'beginTime'
    },
	  {
      label: '结束时间',
      prop: 'endTime'
    },
	  {
      label: '是否赠品0否1是',
      prop: 'giftFlag'
    },
	  {
      label: '状态 0未启用1启用',
      prop: 'status'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
