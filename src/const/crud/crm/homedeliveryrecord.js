/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    editBtn: false,
    detailBtn: false,
    addBtn: false,
    delBtn: false,
    "column": [
        {
            "type": "input",
            "label": "主键",
            "prop": "id",
            hide: true
        }, {
            "type": "input",
            "label": "客户编码",
            "prop": "customerSn",
            minWidth: 120,
            searchSpan: 4,
            search: true
        }, {
            "type": "input",
            "label": "客户名称",
            "prop": "customerName",
            minWidth: 120,
            searchSpan: 4,
            search: true
        }, {
            "type": "input",
            "label": "联系人",
            "prop": "linkmanId",
            hide: true,
            searchSpan: 4,
            search: true
        }, {
            "type": "input",
            "label": "联系人名称",
            "prop": "linkmanName",
            minWidth: 120,
            search: true,
            searchSpan: 4,
            searchLabelWidth: 100
        },
        {
            "type": "radio",
            "label": "配件类型",
            "prop": "goodsType",
            dicUrl: '/admin/dict/type/scm_delivery_goods_type',
            searchSpan: 4,
            search: true
        },
        {
            "type": "input",
            "label": "件数",
            "prop": "goodsNum"
        },
        {
            "type": "input",
            "label": "重量(kg)",
            "prop": "weight",
            minWidth: 100
        },
        {
            "type": "radio",
            "label": "配送状态",
            "prop": "deliveryStatus",
            dicUrl: '/admin/dict/type/scm_delivery_goods_status',
            searchSpan: 4,
            search: true
        },
        {
            "type": "radio",
            "label": "会员类型",
            "prop": "memberType",
            dicUrl: '/admin/dict/type/scm_member_type',
            searchSpan: 4,
            search: true
        },
        {
            "type": "radio",
            "label": "付款方式",
            "prop": "paymentType",
            dicUrl: '/admin/dict/type/scm_payment_type',
            searchSpan: 4,
            search: true
        },
        {
            "type": "input",
            "label": "付款金额(元)",
            "prop": "amount"
        },
        {
            "type": "input",
            "label": "配送物品",
            "prop": "goodsTags",
            overHidden: true,
            minWidth: 100,
            searchSpan: 4,
            search: true
        },
        {
            "type": "input",
            "label": "其他服务",
            "prop": "otherServices",
            overHidden: true,
            minWidth: 100,
            searchSpan: 4,
            search: true
        },
        {
            "type": "input",
            "label": "订单号",
            "prop": "orderSn",
            searchSpan: 4,
            search: true
        },
        {
            "type": "input",
            "label": "第三方订单号",
            "prop": "thirdOrderSn",
            minWidth: 120,
            searchSpan: 4,
            search: true,
            searchLabelWidth: 100
        },
        {
            "type": "daterange",
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            "label": "预约时间",
            "prop": "appointmentTime",
            minWidth: 200,
            searchSpan: 8,
            search: true
        }, {
            "type": "daterange",
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            "label": "配送时间",
            "prop": "deliveryTime",
            minWidth: 200,
            searchSpan: 8,
            search: true
        },

        {
            "type": "input",
            "label": "订单类型",
            "prop": "orderTye",
            hide: true
        },
        {
            "type": "input",
            "label": "补充说明",
            "prop": "description",
            hide: true
        },
        {
            "type": "input",
            "label": "创建者",
            "prop": "createBy",
            hide: true
        }, {
            "type": "input",
            "label": "创建时间",
            "prop": "createDate",
            hide: true
        }, {
            "type": "input",
            "label": "更新者",
            "prop": "updateBy",
            hide: true
        }, {
            "type": "input",
            "label": "更新时间",
            "prop": "updateDate",
            hide: true
        }, {
            "type": "input",
            "label": "备注信息",
            "prop": "remarks",
            hide: true
        }, {
            "type": "input",
            "label": "删除标记",
            "prop": "delFlag",
            hide: true
        }, {
            "type": "input",
            "label": "租户id",
            "prop": "tenantId",
            hide: true
        }]
}
