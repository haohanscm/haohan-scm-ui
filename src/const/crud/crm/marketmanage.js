/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  maxHeight: 500,
  menuWidth:180,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn:false,
  editBtn:false,
  column: [
	  {
      label: '区域编码',
      prop: 'areaSn',
      width: '100',
    },
    {
      label: '区域名称',
      prop: 'areaName'
    },
	  {
      label: '市场编码',
      prop: 'marketSn',
      width: '100',
    },
	  {
      label: '市场名称',
      prop: 'marketName',
      search: true,
      overHidden: true,
      width: '200'
    },
	  {
      label: '市场类型',
      prop: 'marketType',
      type: 'select',
      dicData: [{
        label: '一级市场',
        value: '1'
      }, {
        label: '二级市场',
        value: '2'
      }],
      search: true,
    },
	  {
      label: '标签',
      prop: 'tags'
    },
	  {
      label: '地址',
      prop: 'address',
      overHidden: true,
      width: '200',
    },
	  {
      label: '电话',
      prop: 'phone',
      width: '120'
    },
	  {
      label: '相册编号',
      prop: 'photos',
      hide:true
    },
	  {
      label: '创建者',
      prop: 'createBy',
      hide:true
    },
	  {
      label: '创建时间',
      prop: 'createDate',
      hide:true
    },
	  {
      label: '更新者',
      prop: 'updateBy',
      hide:true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      hide:true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      width: '150',
      overHidden: true,
    },
	  {
      label: '删除标记',
      prop: 'delFlag',
      hide:true
    },
	  {
      label: '租户id',
      prop: 'tenantId',
      hide:true
    },
  ]
}
