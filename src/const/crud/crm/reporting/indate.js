export const tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    addBtn:false,
    menu: false,
    column: [{
        label: '客户名称',
        prop: 'customerName',
    }, {
        label: '客户地址',
        prop: 'address',
    }, {
        label: '库存数量',
        prop: 'num',
    }, {
        label: '库存金额',
        prop: 'amount',
    }, {
        label: '最近上报人',
        prop: 'reportPeople',
    }, {
        label: '最近上报时间',
        prop: 'date',
        type: 'datetime',
        format: "yyyy-MM-dd",
        valueFormat: "yyyy-MM-dd",
        sortable:true,
    }, {
        label: '管辖区域',
        prop: 'direct',
    }, {
        label: '客户经理',
        prop: 'customerManager',
    }]
}
