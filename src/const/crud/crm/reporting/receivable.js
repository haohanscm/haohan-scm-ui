/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    type: 'input',
                    label: '客户编号',
                    prop: 'customerSn',
                    // span: 4
                },
                {
                    type: 'input',
                    label: '客户名称',
                    prop: 'customerName',
                    // span: 4
                }, {
                    type: 'date',
                    label: '销售日期',
                    prop: 'reportDate',
                    format: "yyyy-MM-dd",
                    // span: 4
                },
                {
                    type: 'input',
                    label: '商品总种数',
                    prop: 'goodsTotalNum',
                    // span: 4
                }, {
                    type: 'input',
                    label: '其他金额',
                    prop: 'otherAmount',
                    // span: 4
                },
                {
                    type: 'input',
                    label: '商品合计金额',
                    prop: 'sumAmount',
                    // span: 4
                }, {
                    type: 'input',
                    label: '商品总金额',
                    prop: 'totalAmount',
                    // span: 4
                },
                {
                    type: 'input',
                    label: '上报人名称	',
                    prop: 'reportMan',
                    // span: 4
                }, {
                    type: 'input',
                    label: '上报人手机号',
                    prop: 'reportTelephone',
                    // span: 4
                },
                {
                    type: 'input',
                    label: '上报位置定位',
                    prop: 'reportLocation',
                    // span: 4
                }, {
                    type: 'input',
                    label: '上报位置地址',
                    prop: 'reportAddress',
                    // span: 4
                },
                {
                    type: 'input',
                    label: '上报编号',
                    prop: 'reportSn',
                    // span: 4
                },
                {
                    label: '上报状态',
                    prop: 'reportStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_data_report_status',
                    // valueDefault: '1',
                    // span: 4
                },
                {
                    label: '上报类型',
                    prop: 'reportType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_data_report',
                    // span: 4
                },
                
            ]
        },
        
    ]
}
