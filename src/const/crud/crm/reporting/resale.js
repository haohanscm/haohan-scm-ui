export const tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    addBtn:false,
    menu: false,
    column: [{
        label: '日期',
        prop: 'date',
        search:true,
        type: 'datetime',
        format: "yyyy-MM-dd",
        valueFormat: "yyyy-MM-dd",
        sortable:true,
    }, {
        label: '所属区域',
        prop: 'direct',
        search:true,
    }, {
        label: '客户类型',
        prop: 'customerType',
        search:true,
    }, {
        label: '客户名称',
        prop: 'customerName',
        search: true,
    }, {
        label: '客户经理',
        prop: 'customerManager',
    }]
}
