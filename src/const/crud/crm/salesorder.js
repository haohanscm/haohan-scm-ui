/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  delBtn: false,
  addBtn:false,
  editBtn:false,
    // height: 600,
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide:true
    },
	  {
      label: '订单编号',
      prop: 'salesOrderSn',
      search:true,
      width: 100,
    },
	  {
      label: '客户编号',
      prop: 'customerSn',
      search:true,
      width: 100,
    },
	  {
      label: '客户名称',
      prop: 'customerName',
      search:true
    },
	  
      
 
	  {
      label: '交货日期',
      prop: 'deliveryDate',
      overHidden: true,
      width: 120,
    },
	  {
      label: '合计金额',
      prop: 'sumAmount'
    },
    {
      label: '其它金额',
      prop: 'otherAmount',
    },
	  {
      label: '总计金额',
      prop: 'totalAmount'
    },
	  {
      label: '订单类型',//:1代客下单,2自主下单
      prop: 'salesType',
      dicUrl:'/admin/dict/type/scm_sales_type',
      type: 'select',
      search:true
    },
	  {
      label: '业务员',
      prop: 'employeeId',
      hide:true
    },
	  {
      label: '业务员名称',
      minWidth:120,
      prop: 'employeeName'
    },
	  {
      label: '收货人id',
      prop: 'linkmanId',
      hide:true
    },
	  {
      label: '收货人名称',
      minWidth:120,
      prop: 'linkmanName'
    },
	  {
      label: '收货地址',
      prop: 'address',
      overHidden: true,
      width: 180,
    },
	  {
      label: '手机号',
      prop: 'telephone',
      search:true,
      width: 180,
    },
	  {
      label: '审核状态',//: 1.待审核2.审核不通过3.审核通过
      prop: 'reviewStatus',
      dicUrl:'/admin/dict/type/scm_review_status',
      type: 'select',
      search:true
    },
	  {
      label: '货品种数',
      prop: 'goodsNum'
    },
	  {
      label: '支付状态',//:0未付,1支付中,2已付
      prop: 'payStatus',
      dicUrl:'/admin/dict/type/scm_pay_status',
      type: 'select',
      search:true
    },
    {
      "label": '下单时间',
      "prop": 'orderTime',
      minWidth:110,
          search: true,
          sortable: true,
          searchLabelWidth: 100,
          searchSpan: 12,
          searchslot:true
    },
	  {
      label: '创建者',
      prop: 'createBy',
      hide:true
    },
	  {
      label: '创建时间',
      prop: 'createDate',
      hide:true
    },
	  {
      label: '更新者',
      prop: 'updateBy',
      hide:true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      hide:true
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag',
      hide:true
    },
	  {
      label: '租户id',
      prop: 'tenantId',
      hide:true
    },
  ]
}
