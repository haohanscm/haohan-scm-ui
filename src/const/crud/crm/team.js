/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        {
            label: '头像',
            prop: 'avatar',
            minWidth: 100,
            type:'img',
            slot:true
        },
        {
            label: '员工姓名',
            prop: 'name',
            search: true
        },
        {
            label: '通行证ID',
            prop: 'passportId',
            hide: true
        },
        {
            label: '手机',
            prop: 'telephone',
            minWidth:120,
            search: true
        },
        {
            label: '性别',
            prop: 'sex',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_sex',
            search: true,
        },
        {
            label: '员工类型',
            prop: 'employeeType',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_market_employee_type'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_use_status'
        },
        {
            "label": "部门",
            "prop": "deptName",
        },
        // {
        //   label: '默认分润比例',
        //   prop: 'defaultRate'
        // },
        {
            "type": "input",
            label: '职位',
            prop: 'post',
        },
        {
            label: '备注信息',
            prop: 'remarks',
            width: 200,
            overHidden: true,
        }
    ]
}
