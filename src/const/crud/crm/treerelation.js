/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '祖先节点',
      prop: 'ancestor'
    },
	  {
      label: '后代节点',
      prop: 'descendant'
    },
	  {
      label: '类名',
      prop: 'className'
    },
	  {
      label: '租户ID',
      prop: 'tenantId'
    },
  ]
}
