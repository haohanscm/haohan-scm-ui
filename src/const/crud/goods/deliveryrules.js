/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商家id',
      prop: 'merchantId'
    },
	  {
      label: '商品id',
      prop: 'goodsId'
    },
	  {
      label: '配送方式',
      prop: 'deliveryType'
    },
	  {
      label: '配送计划类型',
      prop: 'deliveryPlanType'
    },
	  {
      label: '配送周期',
      prop: 'deliverySchedule'
    },
	  {
      label: '配送时效',
      prop: 'arriveType'
    },
	  {
      label: '每次配送数量',
      prop: 'deliveryNum'
    },
	  {
      label: '起送数量',
      prop: 'minNum'
    },
	  {
      label: '规则描述',
      prop: 'rulesDesc'
    },
	  {
      label: '指定时间',
      prop: 'specificDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '配送初始时间间隔',
      prop: 'startDayNum'
    },
	  {
      label: '配送总数量',
      prop: 'deliveryTotalNum'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
