/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '店铺ID',
      prop: 'shopId'
    },
	  {
      label: '商家ID',
      prop: 'merchantId'
    },
	  {
      label: '商品分类',
      prop: 'goodsCategoryId'
    },
	  {
      label: '商品唯一编号',
      prop: 'goodsSn'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '商品描述',
      prop: 'detailDesc'
    },
	  {
      label: '概要描述',
      prop: 'simpleDesc'
    },
	  {
      label: '商品品牌ID',
      prop: 'brandId'
    },
	  {
      label: '售卖规则标记',
      prop: 'saleRule'
    },
	  {
      label: '图片组编号',
      prop: 'photoGroupNum'
    },
	  {
      label: '缩略图地址',
      prop: 'thumbUrl'
    },
	  {
      label: '0:YES 1:NO',
      prop: 'isMarketable'
    },
	  {
      label: '库存数量',
      prop: 'storage'
    },
	  {
      label: '服务选项标记',
      prop: 'serviceSelection'
    },
	  {
      label: '配送规则标记',
      prop: 'deliveryRule'
    },
	  {
      label: '赠品标记',
      prop: 'goodsGift'
    },
	  {
      label: '商品规格标记',
      prop: 'goodsModel'
    },
	  {
      label: '商品状态(出售中/仓库中/已售罄)',
      prop: 'goodsStatus'
    },
	  {
      label: '商品来源',
      prop: 'goodsFrom'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '商品类型',
      prop: 'goodsType'
    },
	  {
      label: '扫码购编码',
      prop: 'scanCode'
    },
	  {
      label: '第三方编号/即速商品id',
      prop: 'thirdGoodsSn'
    },
	  {
      label: '公共商品库通用编号',
      prop: 'generalSn'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
