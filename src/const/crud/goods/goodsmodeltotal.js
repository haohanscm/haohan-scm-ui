/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商品id',
      prop: 'goodsId'
    },
	  {
      label: '规格名称',
      prop: 'modelName'
    },
	  {
      label: '规格ID',
      prop: 'modelId'
    },
	  {
      label: '子规格名称',
      prop: 'subModelName'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
