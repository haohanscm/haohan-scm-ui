/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '规则名称',
      prop: 'ruleName'
    },
	  {
      label: '店铺ID',
      prop: 'shopId'
    },
	  {
      label: '商家ID',
      prop: 'merchantId'
    },
	  {
      label: '商品ID',
      prop: 'goodsId'
    },
	  {
      label: '市场价/销售价',
      prop: 'ruleDesc'
    },
	  {
      label: '批发定价,单位分',
      prop: 'wholesalePrice'
    },
	  {
      label: 'vip定价,单位分',
      prop: 'vipPrice'
    },
	  {
      label: '零售定价,单位分',
      prop: 'marketPrice'
    },
	  {
      label: '计量单位',
      prop: 'unit'
    },
	  {
      label: '0启用1暂停',
      prop: 'status'
    },
	  {
      label: '虚拟价格',
      prop: 'virtualPrice'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
