/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '属性值',
      prop: 'attrValue'
    },
	  {
      label: '属性名id',
      prop: 'attrNameId'
    },
	  {
      label: '标准商品id',
      prop: 'spuId'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
