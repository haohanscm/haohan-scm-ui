/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '父级编号',
      prop: 'parentId'
    },
	  {
      label: '所有父级编号',
      prop: 'parentIds'
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '分类描述',
      prop: 'categoryDesc'
    },
	  {
      label: '聚合平台类型',
      prop: 'aggregationType'
    },
	  {
      label: '商品分类通用编号',
      prop: 'generalCategorySn'
    },
	  {
      label: 'logo地址',
      prop: 'logo',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
