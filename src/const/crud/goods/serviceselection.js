/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商家id',
      prop: 'merchantId'
    },
	  {
      label: '商品id',
      prop: 'goodsId'
    },
	  {
      label: '服务名称',
      prop: 'serviceName'
    },
	  {
      label: '服务内容',
      prop: 'serviceDetail'
    },
	  {
      label: '服务价格',
      prop: 'servicePrice'
    },
	  {
      label: '服务次数',
      prop: 'serviceNum'
    },
	  {
      label: '服务周期',
      prop: 'serviceSchedule'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
