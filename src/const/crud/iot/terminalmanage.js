/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '设备编号',
      prop: 'terminalNo'
    },
	  {
      label: '设备类型',
      prop: 'terminalType'
    },
	  {
      label: '设备名称',
      prop: 'name'
    },
	  {
      label: '设备别名',
      prop: 'alias'
    },
	  {
      label: 'SN码',
      prop: 'snCode'
    },
	  {
      label: '制造厂商',
      prop: 'producer'
    },
	  {
      label: 'IMEI',
      prop: 'imeiCode'
    },
	  {
      label: '购货时间',
      prop: 'purchaseTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '出库时间',
      prop: 'sellTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '商家id',
      prop: 'merchantId'
    },
	  {
      label: '店铺id',
      prop: 'shopId'
    },
	  {
      label: '店铺名称',
      prop: 'shopName'
    },
	  {
      label: '设备状态',
      prop: 'status'
    },
	  {
      label: '设备备注',
      prop: 'remark'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
