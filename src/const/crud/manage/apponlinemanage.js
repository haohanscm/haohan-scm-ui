/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '应用名称',
            prop: 'appName',
            search:true
        },
        {
            label: '商家名称',
            prop: 'merchantName'
        },
        {
            label: '步骤名称',
            prop: 'stepName'
        },
        {
            label: '步骤序号',
            prop: 'stepNo'
        },
        // {
        //     label: '请求值',
        //     prop: 'reqParams'
        // },
        // {
        //     label: '返回值',
        //     prop: 'respParams'
        // },
        {
            label: '是否上线',
            prop: 'status',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_yes_no  '
        },
        {
            label: '渠道',
            prop: 'channel'
        },
        // {
        //     label: '操作类型',
        //     prop: 'opType'
        // },
        // {
        //     label: '请求方法',
        //     prop: 'reqMethod'
        // },
        // {
        //     label: '请求地址',
        //     prop: 'reqUrl'
        // },
        // {
        //     label: '操作时间',
        //     prop: 'reqTime'
        // },
        // {
        //     label: '返回时间',
        //     prop: 'respTime'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
