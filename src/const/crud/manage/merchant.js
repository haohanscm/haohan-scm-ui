/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    indexWidth: 50,
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        {
            label: '商家id',
            prop: 'merchantId',
            hide: true
        },
        // {
        //     label: '归属用户',
        //     prop: 'manageUser'
        // },
        {
            label: '商家名称',
            prop: 'merchantName',
            search: true
        },
        // {
        //     label: '区域ID',
        //     prop: 'areaId'
        // },
        {
            label: '商家地址',
            prop: 'address'
        },
        {
            label: '联系人',
            prop: 'contact',
            search: true
        },
        {
            label: '电话',
            prop: 'telephone',
            search: true
        },
        {
            label: '所属行业',
            prop: 'industry',
            search: true
        },
        {
            label: '规模',
            prop: 'scale'
        },
        {
            label: '业务介绍',
            prop: 'bizDesc'
        },
        // {
        //     label: '业务专管员',
        //     prop: 'bizUser'
        // },
        {
            label: '启用状态',
            prop: 'status',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_merchant_status'
        },
        // {
        //     label: '渠道编号',
        //     prop: 'partnerNum'
        // },
        // {
        //     label: '商家类型',
        //     prop: 'merchantType',
        //     type:'select',
        //     search:true,
        //     dicUrl:'/admin/dict/type/scm_merchant_type '
        // },
        // {
        //     label: '产品类型',
        //     prop: 'prodType'
        // },
        // {
        //     label: '是否自动分单',
        //     prop: 'isAutomaticOrder',
        //     type:'select',
        //     search:true,
        //     dicUrl:'/admin/dict/type/scm_yes_no '
        // },
        {
            label: '商家类型',
            prop: 'pdsType',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_merchant_pds_type '
        },
        // {
        //     label: '产品线类型',
        //     prop: 'productLine',
        //     type:'select',
        //     search:true,
        //     dicUrl:'/admin/dict/type/scm_product_line_type '
        // },
        // {
        //     label: '归属商家id',
        //     prop: 'parentId'
        // },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
