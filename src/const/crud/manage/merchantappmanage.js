/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '应用appid',
      prop: 'appId'
    },
	  {
      label: '应用名称',
      prop: 'appName'
    },
	  {
      label: '商家ID',
      prop: 'merchantId'
    },
	  {
      label: '商家名称',
      prop: 'merchantName'
    },
	  {
      label: '管理员ID',
      prop: 'adminId'
    },
	  {
      label: '管理员名称',
      prop: 'adminName'
    },
	  {
      label: '模板ID',
      prop: 'templateId'
    },
	  {
      label: '模板名称',
      prop: 'templateName'
    },
	  {
      label: '即速AppId',
      prop: 'jisuappId'
    },
	  {
      label: '扩展信息',
      prop: 'ext'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '上线时间',
      prop: 'onlineTime'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
