/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '平台商家ID',
            prop: 'pmId'
        },
        {
            label: '商家ID',
            prop: 'merchantId'
        },
        {
            label: '用户ID',
            prop: 'passportId'
        },
        {
            label: '角色',
            prop: 'role'
        },
        {
            label: '手机号',
            prop: 'telephone'
        },
        {
            label: '名称',
            prop: 'name',
            search:true
        },
        {
            label: '注册日期',
            prop: 'regDate',
            type: "date",
            format: "yyyy-MM-dd ",
            valueFormat: "yyyy-MM-dd "
        },
        {
            label: '来源',
            prop: 'origin'
        },
        {
            label: '状态',
            prop: 'status'
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
