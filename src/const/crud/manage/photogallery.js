/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexWidth: 50,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    menuWidth: 120,
    menu: false,
    align: 'center',
    addBtn: false,
    editBtn: false,
    delBtn: false,
    viewBtn: true,
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '图片',
            prop: 'picUrl',
            slot: true
        },
        {
            label: '图片名称',
            prop: 'picName',
            search: true
        },
        // {
        //     label: '图片地址',
        //     prop: 'picUrl'
        // },
        {
            label: '图片类型',
            prop: 'picType',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_photo_type'
        },
        {
            label: '图片大小',
            prop: 'picSize'
        },
        {
            label: '图片来源',
            prop: 'picFrom'
        },
        {
            label: '状态',
            prop: 'status',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_merchant_status'
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            display: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            width: 200,
            overHidden: true,
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
