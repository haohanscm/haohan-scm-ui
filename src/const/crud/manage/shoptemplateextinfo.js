/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '编号',
      prop: 'id'
    },
	  {
      label: '父级编号',
      prop: 'parentId'
    },
	  {
      label: '所有父级编号',
      prop: 'parentIds'
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '模板ID',
      prop: 'templateId'
    },
	  {
      label: '字段名称',
      prop: 'fieldName'
    },
	  {
      label: '字段Code',
      prop: 'fieldCode'
    },
	  {
      label: '类型',
      prop: 'fieldType'
    },
	  {
      label: '是否必填',
      prop: 'isNeed'
    },
	  {
      label: '默认值',
      prop: 'defaultValue'
    },
	  {
      label: '功能说明',
      prop: 'fieldDesc'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
