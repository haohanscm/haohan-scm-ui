/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '用户ID',
            prop: 'id'
        },
        {
            label: '头像',
            prop: 'avatar'
        },
        {
            label: '商家ID',
            prop: 'merchantId'
        },
        {
            label: '登录名',
            prop: 'loginName'
        },
        {
            label: '邮箱',
            prop: 'email'
        },
        {
            label: '手机',
            prop: 'telephone'
        },
        /*{
            label: '密码',
            prop: 'password'
        },
        {
            label: '登录盐值',
            prop: 'salt'
        },*/
        {
            label: '注册时间',
            prop: 'regTime'
        },
        {
            label: '注册IP',
            prop: 'regIp'
        },
        {
            label: '注册方式 0：login_name；1：telephone；2：email 3：QQ；4：sinaweibo； 5：weixin',
            prop: 'regType'
        },
        {
            label: '注册类型 0 ：Web端；1： 移动端； 2： Wap端',
            prop: 'regFrom'
        },
        {
            label: '唯一ID',
            prop: 'unionId'
        },
        {
            label: '服务ID',
            prop: 'serviceId'
        },
        {
            label: '状态 0 ：冻结  1：正常  2 待审核 3 小白用户',
            prop: 'status'
        },
        {
            label: '冻结原因',
            prop: 'reason'
        },
        {
            label: '备注',
            prop: 'memo'
        },
        {
            label: '是否测试',
            prop: 'isTest'
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
       /* {
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
