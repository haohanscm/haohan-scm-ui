/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '用户ID',
            prop: 'uid'
        },
        {
            label: '第三方唯一标识',
            prop: 'openId'
        },
        {
            label: '联合唯一id目前仅用于微信',
            prop: 'unionId'
        },
        {
            label: '应用key',
            prop: 'appId'
        },
        {
            label: '应用类型：0：qq； 1： sinaweibo； 2： weixin',
            prop: 'appType'
        },
        {
            label: '登录账号',
            prop: 'nickName'
        },
        {
            label: '头像地址',
            prop: 'albumUrl'
        },
        {
            label: '性别 0：未知 1：男   2：女',
            prop: 'sex'
        },
        {
            label: '个人信息',
            prop: 'personalInfo'
        },
        {
            label: '刷新登录标识的标识',
            prop: 'flushToken'
        },
        {
            label: '登录标识',
            prop: 'accessToken'
        },
        {
            label: '刷新时间',
            prop: 'updateTime'
        },
        {
            label: '创建时间',
            prop: 'createTime'
        },
        {
            label: '备注',
            prop: 'memo'
        },
        {
            label: '省',
            prop: 'province'
        },
        {
            label: '市',
            prop: 'city'
        },
        {
            label: '区',
            prop: 'district'
        },
        {
            label: '状态',
            prop: 'status'
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
