/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '通行证ID',
      prop: 'passportId'
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '联系电话',
      prop: 'telephone'
    },
	  {
      label: '市场部员工类型',
      prop: 'marketEmployeeType',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_market_employee_type'
    },
	  {
      label: '启用状态',
      prop: 'useStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_use_status'
    },
	  {
      label: '默认分润比例',
      prop: 'defaultRate'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
