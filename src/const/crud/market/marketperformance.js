/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '考核时间',
      prop: 'evaluateTime'
    },
	  {
      label: '考核区间',
      prop: 'evaluateWeek'
    },
	  {
      label: '员工id',
      prop: 'employeeId'
    },
	  {
      label: '部门',
      prop: 'department'
    },
	  {
      label: '岗位',
      prop: 'position'
    },
	  {
      label: '姓名',
      prop: 'employeeName'
    },
	  {
      label: '新客户目标数',
      prop: 'targetCustomerNumber'
    },
	  {
      label: '新客户数',
      prop: 'customerNumber'
    },
	  {
      label: '销售目标额',
      prop: 'targetSalesAmount'
    },
	  {
      label: '销售额',
      prop: 'salesAmount'
    },
	  {
      label: '上级点评',
      prop: 'superiorEvaluate'
    },
	  {
      label: '考核评级',
      prop: 'evaluateLevel',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_evaluate_level'
    },
	  {
      label: '考核状态',
      prop: 'evaluateStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_evaluate_status'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
