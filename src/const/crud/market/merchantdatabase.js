/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商户注册全称',
      prop: 'regName'
    },
	  {
      label: '经营法人',
      prop: 'regUser'
    },
	  {
      label: '所属区域',
      prop: 'areaId'
    },
	  {
      label: '经营地址',
      prop: 'opAddress'
    },
	  {
      label: '商家联系人',
      prop: 'contact'
    },
	  {
      label: '联系手机',
      prop: 'telephone'
    },
	  {
      label: '座机电话',
      prop: 'phoneNumber'
    },
	  {
      label: '商户类别',
      prop: 'merchantType'
    },
	  {
      label: '行业类别',
      prop: 'industry'
    },
	  {
      label: '网站名称',
      prop: 'website'
    },
	  {
      label: '淘宝店名称',
      prop: 'taobaoShop'
    },
	  {
      label: '现有推广平台',
      prop: 'marketPlatform'
    },
	  {
      label: '现有支付工具',
      prop: 'payTools'
    },
	  {
      label: '店铺名称',
      prop: 'shopName'
    },
	  {
      label: '经营面积',
      prop: 'operateArea'
    },
	  {
      label: '员工人数',
      prop: 'workerNum'
    },
	  {
      label: '店铺介绍',
      prop: 'shopDesc'
    },
	  {
      label: '营业时间',
      prop: 'serviceTime'
    },
	  {
      label: '业务介绍',
      prop: 'bizDesc'
    },
	  {
      label: '店铺地址',
      prop: 'shopAddress'
    },
	  {
      label: '营业执照',
      prop: 'bizLicense',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '周围环境',
      prop: 'environment'
    },
	  {
      label: '店铺服务',
      prop: 'shopService'
    },
	  {
      label: '年经营流水',
      prop: 'shopSale'
    },
	  {
      label: '照片资料',
      prop: 'pictureFile',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '收录时间',
      prop: 'initTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '商机来源',
      prop: 'bizfromType',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_bizfrom_type'
    },
	  {
      label: '是否审核',
      prop: 'status'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '客户状态',
      prop: 'customerStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_customer_status'
    },
	  {
      label: '客户级别',
      prop: 'customerLevel',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_customer_level'
    },
	  {
      label: '公司性质',
      prop: 'companyNature',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_company_nature'
    },
	  {
      label: '客户类型',
      prop: 'customerType',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_customer_type'
    },
	  {
      label: '市场负责人id',
      prop: 'directorId'
    },
  ]
}
