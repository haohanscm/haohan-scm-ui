/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商家资料ID',
      prop: 'merchantResId'
    },
	  {
      label: '拜访地址',
      prop: 'visitAddress'
    },
	  {
      label: '联系人',
      prop: 'contact'
    },
	  {
      label: '联系电话',
      prop: 'phoneNumber'
    },
	  {
      label: '拜访时间',
      prop: 'visitTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '拜访内容',
      prop: 'visitContent'
    },
	  {
      label: '进展阶段',
      prop: 'visitStep',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_visit_step'
    },
	  {
      label: '拜访照片',
      prop: 'visitPic',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '拜访人员',
      prop: 'bizUser'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '客户状态',
      prop: 'customerStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_customer_status'
    },
	  {
      label: '下次回访时间',
      prop: 'nextVisitTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    }
  ]
}
