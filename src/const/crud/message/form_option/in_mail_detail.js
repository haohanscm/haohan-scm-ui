export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    label: '消息编号',
                    prop: 'messageSn',
                    readonly: true
                },
                {
                    label: '发送人名称',
                    prop: 'senderName',
                    search: true,
                    span: 8,
                },
                {
                    label: '消息类型',
                    prop: 'messageType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_message_type',
                    clearable: false,
                },
                {
                    label: "发送时间",
                    prop: "sendTime",
                    type: "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    valueFormat: "yyyy-MM-dd HH:mm:ss",
                },
            ]
        },
        {
            icon: 'el-icon-info',
            label: '消息',
            prop: 'group2',
            column: [
                {
                    type: 'input',
                    label: '标题',
                    prop: 'title',
                    span: 24,
                    maxlength: 32,
                    showWordLimit: true,
                    rules: [
                        {required: true, message: '需要一个标题', trigger: 'blur'}
                    ]
                },
                {
                    label: '内容',
                    prop: 'content',
                    type: "textarea",
                    maxlength: 5000,
                    showWordLimit: true,
                    span: 24,
                    rules: [
                        {required: true, message: '请填写消息内容', trigger: 'blur'}
                    ]
                },
                {
                    label: '业务部门',
                    prop: 'departmentType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_department_type',
                    clearable: false,
                },
                {
                    label: '业务类型',
                    prop: 'msgActionType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_msg_action_type',
                    clearable: false,
                },
                {
                    type: "textarea",
                    label: "请求参数",
                    prop: "reqParams",
                    maxlength: 1000,
                    span: 12,
                    showWordLimit: true,
                    display: false
                }
            ]
        }
    ]
}
