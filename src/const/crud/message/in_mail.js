/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        {
            label: '主键',
            prop: 'id',
            hide: true
        },
        {
            label: '站内信编号',
            prop: 'inMailSn',
            minWidth: 100,
            search: true
        },
        {
            label: '标题',
            prop: 'title',
            minWidth: 100,
            search: true
        },
        {
            label: '内容',
            prop: 'content',
        },
        {
            label: '站内信类型',
            prop: 'inMailType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_in_mail_type',
            search: true,
        },
        {
            label: '发送人名称',
            prop: 'senderName',
            search: true
        },
        {
            label: '业务部门类型',
            prop: 'departmentType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_department_type',
            search: true,
        },
        {
            label: '业务类型',
            prop: 'msgActionType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_msg_action_type',
            search: true,
        },
        {
            "label": "发送时间",
            "prop": "sendTime",
            "type": "date",
            format: "yyyy-MM-dd HH:mm:ss",
            minWidth: 180,
        },
        {
            label: '请求参数',
            prop: 'reqParams',
            hide: true,
        },
    ]
}
