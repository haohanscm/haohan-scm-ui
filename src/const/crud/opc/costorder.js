/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家ID',
      prop: 'pmId',
      search: true
    },
	  {
      label: '交易日',
      prop: 'dealDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '总订单数',
      prop: 'totalOrderNum'
    },
	  {
      label: '商品SPU数',
      prop: 'spuNum'
    },
	  {
      label: '商品SKU数',
      prop: 'skuNum'
    },
	  {
      label: '供应商货款',
      prop: 'supplierPayment'
    },
	  {
      label: '采购商货款',
      prop: 'buyerPayment'
    },
	  {
      label: '售后货款',
      prop: 'afterSalePayment'
    },
	  {
      label: '成本合计',
      prop: 'costTotal'
    },
	  {
      label: '毛利润',
      prop: 'grossProfit'
    },
	  {
      label: '利润率',
      prop: 'grossRate'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
