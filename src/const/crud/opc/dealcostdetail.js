/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家ID',
      prop: 'pmId'
    },
	  {
      label: '交易日',
      prop: 'dealDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '成本编号',
      prop: 'costNo',
      search: true
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '数量',
      prop: 'number'
    },
	  {
      label: '金额',
      prop: 'amount'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
