/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '商家信息',
            prop: 'group1',
            column: [
                {
                    label: '商家名称',
                    prop: 'merchantName',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入商家名称",
                        trigger: "blur"
                    }]
                },
                {
                    label: '商家地址',
                    prop: 'address',
                    span: 8,
                    maxlength: 64,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入商家地址",
                        trigger: "blur"
                    }]
                },
                {
                    label: '商家id',
                    prop: 'merchantId',
                    readonly: true,
                    span: 8,
                    placeholder: '无需填写'
                },
                {
                    label: '联系人名称',
                    prop: 'contact',
                    span: 8,
                    maxlength: 10,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入联系人名称",
                        trigger: "blur"
                    }]
                },
                {
                    label: '电话',
                    prop: 'telephone',
                    span: 8,
                    maxlength: 20,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入电话",
                        trigger: "blur"
                    }]
                },
                {
                    label: '所属行业',
                    prop: 'industry',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '规模',
                    prop: 'scale',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '启用状态',
                    prop: 'status',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_merchant_status',
                    span: 8,
                },
                {
                    label: '商家类型',
                    prop: 'pdsType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_merchant_pds_type',
                    span: 8,
                    display: true
                },
                {
                    label: '业务介绍',
                    prop: 'bizDesc',
                    type: "textarea",
                    span: 12,
                    maxlength: 64,
                    showWordLimit: true
                },
                {
                    label: '备注信息',
                    prop: 'remarks',
                    type: "textarea",
                    span: 12,
                    maxlength: 255,
                    showWordLimit: true
                },
            ]
        }
    ]
}
