/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

// 收货相关和订单相关信息
export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '收货相关',
            prop: 'group1',
            column: [
                {
                    label: '平台商家',
                    prop: 'pmName',
                    span: 6
                },
                {
                    label: '发货单编号',
                    prop: 'shipRecordSn',
                    span: 6
                },
                {
                    label: '客户名称',
                    prop: 'customerName',
                    span: 6
                },
                {
                    label: '收货人名称',
                    prop: 'receiverName',
                    span: 6
                },
                {
                    label: '收货人电话',
                    prop: 'receiverTelephone',
                    span: 6
                },
                {
                    label: '收货人地址',
                    prop: 'receiverAddress',
                    span: 6
                },
                {
                    "label": "交货日期",
                    "prop": "deliveryDate",
                    "type": "date",
                    format: "yyyy-MM-dd",
                    span: 6
                },
            ]
        },
        // {
        //     icon: 'el-icon-info',
        //     label: '发货相关',
        //     prop: 'group2',
        //     column: [
        //         {
        //             label: '平台商家',
        //             prop: 'pmName',
        //             span: 4
        //         },
        //         {
        //             label: '发货单编号',
        //             prop: 'shipRecordSn',
        //             span: 4
        //         },
        //         {
        //             label: '发货人名称',
        //             prop: 'shipperName',
        //             span: 4
        //         },
        //         {
        //             label: '发货人电话',
        //             prop: 'shipperTelephone',
        //             span: 4
        //         },
        //         {
        //             label: '发货地址',
        //             prop: 'shipAddress',
        //             span: 4
        //         },
        //         {
        //             "label": "发货时间",
        //             "prop": "shipTime",
        //             "type": "datetime",
        //             format: "yyyy-MM-dd HH:mm:ss",
        //             span: 4
        //         },
        //         {
        //             label: '发货状态',
        //             prop: 'shipStatus',
        //             type: 'select',
        //             dicUrl: '/admin/dict/type/scm_ship_record_status',
        //             span: 4
        //         },
        //         {
        //             label: '发货方式',
        //             prop: 'shipType',
        //             type: 'select',
        //             dicUrl: '/admin/dict/type/scm_ship_type',
        //             span: 4
        //         },
        //         {
        //             label: '物流单号',
        //             prop: 'logisticsSn',
        //             span: 4
        //         },
        //         {
        //             label: '物流公司',
        //             prop: 'logisticsName',
        //             span: 4
        //         },
        //     ]
        // },
        {
            icon: 'el-icon-info',
            label: '订单相关',
            prop: 'group3',
            column: [
                {
                    label: '订单编号',
                    prop: 'orderSn',
                    span: 4
                },
                {
                    label: '订单类型',
                    prop: 'orderType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_order_type',
                    span: 4
                },
                {
                    "label": "下单时间",
                    "prop": "orderTime",
                    "type": "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    span: 4
                },

                {
                    label: '订单总金额',
                    prop: 'totalAmount',
                    span: 4
                },
                {
                    label: '订单其他金额',
                    prop: 'otherAmount',
                    span: 4
                },
                {
                    label: '订单商品合计金额',
                    prop: 'sumAmount',
                    span: 4
                },
                {
                    label: '订单商品种类数',
                    prop: 'goodsNum',
                    span: 4
                },
                {
                    label: '备注信息',
                    prop: 'remarks',
                    maxlength: 255,
                    span: 12,
                    showWordLimit: true
                }
            ]
        }
    ]
}
