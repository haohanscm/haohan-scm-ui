/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

// 发货人 可修改属性
export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '发货人属性',
            prop: 'group1',
            column: [
                // {
                //     "type": "input",
                //     "label": "主键",
                //     "prop": "id",
                //     hide: true
                // },
                {
                    label: '发货人姓名',
                    prop: 'shipperName',
                    span: 8,
                    maxlength: 10,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入发货人姓名",
                        trigger: "blur"
                    }]
                },
                {
                    label: '发货人电话',
                    prop: 'telephone',
                    span: 8,
                    maxlength: 20,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入发货人电话",
                        trigger: "blur"
                    }]
                },
                {
                    label: '发货地址',
                    prop: 'shipAddress',
                    span: 8,
                    maxlength: 64,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入发货地址",
                        trigger: "blur"
                    }]
                },
                {
                    "type": "select",
                    "label": "性别",
                    "prop": "sex",
                    dicUrl: '/admin/dict/type/scm_sex',
                    span: 8,
                },
                {
                    "type": "number",
                    "label": "排序值",
                    "prop": "sort",
                    span: 8,
                },
                {
                    "label": "备注信息",
                    "prop": "remarks",
                    type: "textarea",
                    span: 8,
                    maxlength: 255,
                    showWordLimit: true
                }
            ]
        },
    ]
}
