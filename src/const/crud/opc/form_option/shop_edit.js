/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '店铺信息',
            prop: 'group1',
            column: [
                {
                    label: '店铺名称',
                    prop: 'name',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入店铺名称",
                        trigger: "blur"
                    }]
                },
                {
                    label: '店铺id',
                    prop: 'shopId',
                    span: 8,
                    readonly: true,
                    placeholder: '无需填写'
                },
                {
                    label: '负责人名称',
                    prop: 'manager',
                    span: 8,
                    maxlength: 10,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入负责人名称",
                        trigger: "blur"
                    }]
                },
                {
                    label: '店铺电话',
                    prop: 'telephone',
                    span: 8,
                    maxlength: 20,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入店铺电话",
                        trigger: "blur"
                    }]
                },
                {
                    label: '商家',
                    prop: 'merchantId',
                    span: 8,
                    formslot: true,
                    rules: [{
                        required: true,
                        message: "请选择商家",
                        trigger: "blur"
                    }]
                },
                {
                    label: '启用状态',
                    prop: 'status',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_merchant_status',
                    span: 8,
                },
                {
                    label: '店铺等级',
                    prop: 'shopLevel',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_shop_level',
                    span: 8,
                },
                {
                    label: '定位',
                    prop: 'position',
                    span: 8,
                    formslot: true,
                },
                {
                    label: '店铺地址',
                    prop: 'address',
                    type:"textarea",
                    span: 8,
                    maxlength: 64,
                    showWordLimit: true,
                    rules: [{
                        required: true,
                        message: "请输入店铺地址",
                        trigger: "blur"
                    }]
                }
            ]
        }
        ,
        {
            icon: 'el-icon-info',
            label: '描述信息',
            prop: 'group2',
            column: [
                {
                    label: '营业时间',
                    prop: 'onlineTime',
                    span: 8,
                    type:"textarea",
                    maxlength: 100,
                    showWordLimit: true
                },
                {
                    label: '店铺服务',
                    prop: 'shopService',
                    span: 8,
                    type:"textarea",
                    maxlength: 100,
                    showWordLimit: true
                },
                {
                    label: '店铺介绍',
                    prop: 'shopDesc',
                    span: 8,
                    type:"textarea",
                    maxlength: 250,
                    showWordLimit: true
                },
                {
                    label: '行业名称',
                    prop: 'industry',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '更新时间',
                    prop: 'updateDate',
                    span: 8,
                    readonly: true,
                    placeholder: '无需填写'
                },
                {
                    label: '备注信息',
                    prop: 'remarks',
                    type: "textarea",
                    span: 8,
                    maxlength: 255,
                    showWordLimit: true
                },
            ]
        },
        {
            icon: 'el-icon-info',
            label: '图片信息',
            prop: 'group3',
            column: [
                {
                    label: '轮播图',
                    prop: 'photoList',
                    span: 12,
                    formslot: true
                },
                {
                    label: '店铺收款码',
                    prop: 'payCodeList',
                    span: 12,
                    formslot: true
                },
                {
                    label: '店铺二维码',
                    prop: 'qrcodeList',
                    span: 12,
                    formslot: true
                },
                {
                    label: '店铺Logo',
                    prop: 'shopLogoList',
                    span: 12,
                    formslot: true
                }
            ]
        }
    ]
}
