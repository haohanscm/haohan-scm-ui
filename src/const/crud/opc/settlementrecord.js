/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '结算记录编号',
      prop: 'settlementId'
    },
	  {
      label: '结算类型',
      prop: 'settlementType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_settlement_type',
      search: true

    },
	  {
      label: '结算金额',
      prop: 'settlementAmount'
    },
	  {
      label: '结算开始日期',
      prop: 'settlementBeginDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '结算结束日期',
      prop: 'settlementEndDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '付款日期',
      prop: 'payDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '结算公司类型:采购商/供应商',
      prop: 'companyType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_company_type',
      search: true
    },
	  {
      label: '结算公司名称',
      prop: 'companyName'
    },
	  {
      label: '结款人名称',
      prop: 'companyOperator'
    },
	  {
      label: '结算凭证图片',
      prop: 'settlementImg',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'

    },
	  {
      label: '结算说明',
      prop: 'settlementDesc'
    },
	  {
      label: '货款单号',
      prop: 'paymentSn'
    },
	  {
      label: '经办人名称',
      prop: 'operator'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
