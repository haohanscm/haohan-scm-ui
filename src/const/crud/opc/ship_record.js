/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        // {
        //     label: '主键',
        //     prop: 'id'
        // },
        {
            label: '平台商家',
            prop: 'pmName',
            minWidth: 100,
            overHidden: true,
        },
        {
            label: '发货单编号',
            prop: 'shipRecordSn',
            minWidth: 100,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '客户名称',
            prop: 'customerName',
            minWidth: 100,
            overHidden: true,
            search: true
        },
        {
            label: '收货人名称',
            prop: 'receiverName',
            minWidth: 100,
            overHidden: true,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '收货人电话',
            prop: 'receiverTelephone',
            minWidth: 120,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '收货人地址',
            prop: 'receiverAddress',
            width: 150,
            overHidden: true
        },
        {
            "label": "交货日期",
            "prop": "deliveryDate",
            "type": "date",
            format: "yyyy-MM-dd",
            valueFormat: 'yyyy-MM-dd',
            minWidth: 120,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '发货人名称',
            prop: 'shipperName',
            minWidth: 100,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '发货人电话',
            prop: 'shipperTelephone',
            minWidth: 120,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '发货地址',
            prop: 'shipAddress',
            width: 150,
            overHidden: true,
        },
        {
            label: '发货状态',
            prop: 'shipStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_ship_record_status',
            search: true,
        },
        {
            label: '发货方式',
            prop: 'shipType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_ship_type',
            search: true,
            minWidth: 100,
        },
        {
            "label": "发货时间",
            "prop": "shipTime",
            "type": "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            minWidth: 180,
            search: true,
            sortable: true,
            searchLabelWidth: 100,
            searchSpan: 12,
            searchslot:true
        },
        {
            label: '物流单号',
            prop: 'logisticsSn',
            minWidth: 100,
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '物流公司',
            prop: 'logisticsName',
            minWidth: 100,
            search: true,
        },
        {
            label: '订单编号',
            prop: 'orderSn',
            minWidth: 100,
            search: true,
        },
        {
            label: '订单类型',
            prop: 'orderType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_order_type',
            search: true,
        },
        {
            "label": "下单时间",
            "prop": "orderTime",
            "type": "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            minWidth: 180,
        },

        {
            label: '订单总金额',
            prop: 'totalAmount',
        },
        {
            label: '订单其他金额',
            prop: 'otherAmount',
        },
        {
            label: '订单商品合计金额',
            prop: 'sumAmount',
        },
        {
            label: '订单商品种类数',
            prop: 'goodsNum',
        },
        {
            label: '备注信息',
            prop: 'remarks',
            width: 200,
            overHidden: true,
        }
    ]
}
