/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '汇总单号',
      prop: 'summaryOrderId'
    },
	  {
      label: '商品分类id',
      prop: 'goodsCategoryId'
    },
	  {
      label: '商品图片',
      prop: 'goodsImg',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '市场价',
      prop: 'marketPrice'
    },
	  {
      label: '平台报价',
      prop: 'platformPrice'
    },
	  {
      label: '采购均价',
      prop: 'buyAvgPrice'
    },
	  {
      label: '供应均价',
      prop: 'supplyAvgPrice'
    },
	  {
      label: '实际采购数量',
      prop: 'realBuyNum'
    },
	  {
      label: '需求采购数量',
      prop: 'needBuyNum'
    },
	  {
      label: '最小供应量',
      prop: 'limitSupplyNum'
    },
	  {
      label: '商家数量',
      prop: 'buyerNum'
    },
	  {
      label: '采购日期',
      prop: 'buyTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '送货日期',
      prop: 'deliveryTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '单位',
      prop: 'unit'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '是否生成交易单',
      prop: 'isGenTrade'
    },
	  {
      label: '采购批次',
      prop: 'buySeq'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      text: 'textarea'
    },
	  {
      label: '汇总单状态',
      prop: 'summaryStatus',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_summary_status',
      search: true
    },
	  {
      label: '调配类型',
      prop: 'allocateType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_allocate_type',
      search: true
    },
	  {
      label: '汇总时间',
      prop: 'summaryTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '完成时间',
      prop: 'finishTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '商品ID(spu)',
      prop: 'goodsId'
    },
	  {
      label: '商品规格id',
      prop: 'goodsModelId'
    },
	  {
      label: '商品规格(名称)',
      prop: 'goodsModel'
    },
	  {
      label: '采购选项',
      prop: 'purchaseOption',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_option',
      search: true,
      sortable: true
    }
  ]
}
