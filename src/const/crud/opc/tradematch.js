/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '询价单号',
      prop: 'askOrderId'
    },
	  {
      label: '报价单号',
      prop: 'offerOrderId'
    },
	  {
      label: '供应商',
      prop: 'supplierId'
    },
	  {
      label: '供应数量',
      prop: 'supplyNum'
    },
	  {
      label: '报价类型',
      prop: 'offerType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_offer_type',
      search: true
    },
	  {
      label: '报价',
      prop: 'offerPrice'
    },
	  {
      label: '成交价',
      prop: 'dealPrice'
    },
	  {
      label: '采购数量',
      prop: 'buyNum'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '状态描述',
      prop: 'respDesc'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
