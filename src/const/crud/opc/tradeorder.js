/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '交易单号',
      prop: 'tradeId'
    },
	  {
      label: '汇总单号',
      prop: 'summaryBuyId'
    },
	  {
      label: '采购编号',
      prop: 'buyId'
    },
	  {
      label: '报价类型',
      prop: 'offerType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_offer_type',
      search: true
    },
	  {
      label: '报价单号',
      prop: 'offerId'
    },
	  {
      label: '采购商',
      prop: 'buyerId'
    },
	  {
      label: '供应商',
      prop: 'supplierId'
    },
	  {
      label: '采购批次',
      prop: 'buySeq'
    },
	  {
      label: '商品ID',
      prop: 'goodsId'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '商品图片',
      prop: 'goodsImg',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '商品规格',
      prop: 'goodsModel'
    },
	  {
      label: '商品分类',
      prop: 'goodsCategory',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_use_status',
      search: true
    },
	  {
      label: '单位',
      prop: 'unit'
    },
	  {
      label: '采购数量',
      prop: 'buyNum'
    },
	  {
      label: '采购时间',
      prop: 'buyTime'
    },
	  {
      label: '采购备注',
      prop: 'buyNode'
    },
	  {
      label: '市场价',
      prop: 'marketPrice'
    },
	  {
      label: '供应单价',
      prop: 'supplyPrice'
    },
	  {
      label: '采购单价',
      prop: 'buyPrice'
    },
	  {
      label: '成交时间',
      prop: 'dealTime'
    },
	  {
      label: '联系人',
      prop: 'contact'
    },
	  {
      label: '联系电话',
      prop: 'contactPhone'
    },
	  {
      label: '送货时间',
      prop: 'deliveryTime'
    },
	  {
      label: '送货地址',
      prop: 'deliveryAddress'
    },
	  {
      label: '供应商状态',
      prop: 'supplierStatus'
    },
	  {
      label: '采购商状态',
      prop: 'buyerStatus'
    },
	  {
      label: '采购员',
      prop: 'buyOperator'
    },
	  {
      label: '运营状态',
      prop: 'opStatus'
    },
	  {
      label: '配送状态',
      prop: 'deliveryStatus'
    },
	  {
      label: '交易状态',
      prop: 'transStatus'
    },
	  {
      label: '分拣数量',
      prop: 'sortOutNum'
    },
	  {
      label: '配送方式',
      prop: 'deliveryType'
    },
	  {
      label: '货品编号',
      prop: 'productSn'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
