/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    label: '账单结算',
    submitText: '完成结算',
    labelWidth: 120,
    emptyBtn:false,
    column: [
        {
            label: '平台商家id',
            prop: 'pmId',
            span: 9,
            display: false
        },
        {
            label: '结算记录编号',
            prop: 'settlementId',
            span: 9,
            readonly: true
        },
        {
            label: '结算类型',
            prop: 'settlementType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_settlement_type',
            span: 9,
            disabled: true
        },
        {
            label: '货款单号',
            prop: 'paymentSn',
            span: 9,
            readonly: true
        },
        {
            label: '结算金额',
            prop: 'settlementAmount',
            span: 9,
            rules: [{
                required: true,
                message: "请输入结算金额",
                trigger: "blur",
            }],
        },
        {
            label: '支付方式',
            prop: 'payType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_pay_type',
            span: 9,
            rules: [{
                required: true,
                message: "请选择支付方式",
                trigger: "blur",
            }],
        },
        {
            label: '付款日期',
            prop: 'payDate',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            span: 9,
            rules: [{
                required: true,
                message: "请选择付款日期",
                trigger: "blur",
            }],
        },
        {
            label: '结款人名称',
            prop: 'companyOperator',
            span: 9,
            rules: [{
                required: true,
                message: "请输入结款人名称",
                trigger: "blur",
            }],
        },
        {
            label: '结算凭证图片',
            prop: 'settlementImg',
            span: 9,
            formslot:true
            // type: 'upload',
            // listType: 'picture-img',
            // propsHttp: {
            //     res: 'data.0'
            // },
            // tip: '只能上传jpg/png用户头像，且不超过500kb',
            // action: 'https://avueupload.91eic.com/upload/list'
        },
        {
            label: '结算说明',
            prop: 'settlementDesc',
            span: 9,
        },
        {
            label: '结算公司名称',
            prop: 'companyName',
            span: 9,
            readonly: true
        },
        {
            label: '经办人名称',
            prop: 'operator',
            span: 9,
            rules: [{
                required: true,
                message: "请输入经办人名称",
                trigger: "blur",
            }],
        }, {
            label: '是否结算',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            span: 9,
            disabled: true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            readonly: true,
            span: 9,
            hide:true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea',
            span: 12,
        }
    ]
}
