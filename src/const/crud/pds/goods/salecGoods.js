
export const tableoption = {
  emptyText: '暂无数据',
  selection: false,
  index: true,
  indexLabel: '序号',
  addBtn: false,
  editBtn: false,
  refreshBtn: false,
  columnBtn: false,
  align: 'center',
  menuAlign: 'center',
  border: true,
  header: false,
  maxHeight: 200,
  column: [
    {
      label: '商品名称',
      prop: 'goodsName',
      fixed: true,
      showColumn: false
    },
    {
      label: '权重',
      prop: 'sort',
      slot: true,
      showColumn: false
    },
    {
      label: '商品图片',
      prop: 'thumbUrl',
      slot: true,
      showColumn: false
    },
    {
      label: '商品编码',
      prop: 'goodsSn',
      showColumn: false
    },
    {
      label: '售价',
      prop: 'modelPrice',
      minWidth: 200,
      slot: true,
      showColumn: false
    },
    {
      label: '市场价',
      prop: 'marketPrice',
      showColumn: false
    },
    {
      label: '库存',
      prop: 'storage',
      slot: true,
      showColumn: false
    },
    {
      label: '单位',
      prop: 'unit',
      showColumn: false
    },
    {
      label: '是否上架',
      prop: 'isMarketable',
      slot: true,
      showColumn: false
    },
    {
      label: '同步商城',
      prop: 'salecFlag',
      slot: true,
      showColumn:true
    }
  ]
}
