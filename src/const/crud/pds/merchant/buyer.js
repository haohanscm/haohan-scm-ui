/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '#',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  // dialogWidth: '80%',
  // labelWidth: '80%',
  viewBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  emptySize:'mini',
  column: [
    {
      label: '采购商',
      prop: 'buyerName',
      minWidth: 100,
      sortable: true,
      search: true
    },
    {
      label: '简称',
      prop: 'shortName',
      minWidth: 110,
    },
    {
      label: '联系人',
      prop: 'contact',
      minWidth: 110,
      search: true
    },
    {
      label: '电话',
      prop: 'telephone',
      'type': 'number',
      minWidth: 120,
      
    },
    {
      label: '账期',
      prop: 'payPeriod',
      dicUrl: '/admin/dict/type/scm_pay_period'
    }, {
      label: '地址',
      prop: 'address',
      overHidden: true,
      minWidth: 110,
    },{
      label: '商家名称',
      prop: 'merchantName',
      minWidth: 110,
      search: true
    },{
      label: '创建时间',
      prop: 'createDate',
      minWidth: 110,
      search: true
    }, {
      label: '采购类型',
      prop: 'buyerType',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_buyer_type'
    }, {
      label: '启用状态',
      prop: 'status',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_use_status'
    },
    {
      label: '是否需下单支付',
      prop: 'needPay',
      type: 'select',
      searchLabelWidth: 120,
      minWidth: 140,
      search: true,
      dicUrl: '/admin/dict/type/scm_yes_no'
    }
    ,{
      label: '是否限制下单',
      prop: 'needConfirmation',
      searchLabelWidth: 100,
      minWidth: 140,
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_yes_no'
    }
    , {
      label: '消息推送',
      prop: 'needPush',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_yes_no'
    }, {
      label: '排序值',
      prop: 'sort'
    }
  ]
}


