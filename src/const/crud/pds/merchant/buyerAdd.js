export const addOption = {
        emptyBtn: false,
        detail: false,
        // menuBtn:false,
        group: [
          {
            label: '基本信息',
            prop: 'group1',
      
            column: [
              {
                label: '采购商名称',
                prop: 'buyerName',
                maxlength: 10,
                labelWidth: 120,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入采购商名称',
                  trigger: 'blur'
                }]
      
              }, {
                label: '采购商简称',
                prop: 'shortName',
                maxlength: 10,
                labelWidth: 120,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入采购商简称',
                  trigger: 'blur'
                }]
              }, {
                label: '商家',
                prop: 'merchantId',
                maxlength: 10,
                formslot: true,
              }, {
                label: '联系人',
                prop: 'contact',
                maxlength: 10,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入联系人',
                  trigger: 'blur'
                }]
              }, {
                label: '电话',
                prop: 'telephone',
                "type": "Number",
                // precision:Number,
                precision: 0,
                maxlength: 11,
                minlength: 11,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入电话',
                  trigger: 'blur',
                }]
              },
              {
                label: '详细地址',
                prop: 'address',
                showWordLimit: true,
                formslot: true
      
              },
              {
                type: "select",
                label: "权限角色",
                prop: "roleIds",
                showWordLimit: true,
                formslot: true,
                props: {
                  label: 'roleName',
                  value: 'roleId'
                },
                rules: [{
                  required: true,
                  message: '请选择角色',
                  trigger: 'blur',
                }]
              }, {
                label: '标注位置 ',
                prop: 'position',
                showWordLimit: true,
                formslot: true
              },
      
              {
                type: "tree",
                label: '部门',
                prop: 'deptId',
                formslot: true,
                props: {
                  label: 'name',
                  value: 'id'
                },
                rules: [{
                  required: true,
                  message: '请选择部门',
                  trigger: 'blur',
                }]
              },
              {
                label: '区域',
                prop: 'area',
                formslot: true
              }, {
                label: '备注',
                prop: 'remarks',
                type: 'textarea',
                maxlength: 200,
                showWordLimit: true
              },
              ]
          }, {
            label: '扩展信息',
            prop: 'group2',
            column: [{
              label: '采购商类型',
              prop: 'supplierType',
              type: 'radio',
              value: '1',
              dicUrl: '/admin/dict/type/scm_buyer_type'
            }, {
              label: '启用状态',
              type: 'radio',
              prop: 'status',
              value: '1',
              dicUrl: '/admin/dict/type/scm_use_status'
            }, {
              label: '账期',
              type: 'radio',
              prop: 'payPeriod',
              value: '1',
              dicUrl: '/admin/dict/type/scm_pay_period'
            }, {
              label: '消息推送',
              type: 'radio',
              prop: 'needPush',
              value: '0',
              dicUrl: '/admin/dict/type/scm_yes_no'
            }, 
            {
                label: '是否限制下单',
                type: 'radio',
                labelWidth: 120,
                prop: 'needConfirmation',
                value: '0',
                // minwidth:140,
                dicUrl: '/admin/dict/type/scm_yes_no'
              },{
              label: '排序值',
              type: 'number',
              maxlength: 5,
              prop: 'sort',
              value: '100',
              showWordLimit: true,
              // dicUrl: '/admin/dict/type/scm_supplier_level'
            }, {
              label: '账期日',
              type: 'number',
              prop: 'payDay',
              value: '1',
              maxlength: 2,
              showWordLimit: true,
              // dicUrl: '/admin/dict/type/scm_supplier_level'
            }, {
              label: '是否需下单支付',
              type: 'radio',
              prop: 'needPay',
              value: '1',
              maxlength: 2,
              labelWidth: 120,
              showWordLimit: true,
              dicUrl: '/admin/dict/type/scm_yes_no'
            }
            ]
          }]
      }
      //供应商编辑中的平台商品
//       export const modelGoodsOption = {
//         title: '供应商品的规格',
//         addBtn: false,
//         page: false,
//         border: true,
//         index: true,
//         indexWidth: 50,
//         indexLabel: "序号",
//         maxHeight: 400,
//         stripe: true,
//         header: false,
//         align: "center",
//         cellBtn: true,
//         editBtn: false,
//         delBtn: false,
//         column: [
//           {
//             label: 'typeName0',
//             prop: 'typeName0',
//             slot: true,
//             hide: true,
//             width: "120"
//           },
//           {
//             label: 'typeName1',
//             prop: 'typeName1',
//             slot: true,
//             hide: true,
//             width: "120"
//           },
//           {
//             label: 'typeName2',
//             prop: 'typeName2',
//             slot: true,
//             hide: true,
//             width: "120"
//           },
//           {
//             label: '图片',
//             prop: 'modelUrl',
//             slot: true
//           },
//           {
//             label: '价格',
//             prop: 'modelPrice',
//             type: 'number',
//             cell: true,
//             width: "120",
//             precision: 2
//           },
//           {
//             label: '虚拟价',
//             prop: 'virtualPrice',
//             type: 'number',
//             cell: true,
//             width: "120",
//             precision: 2
//           },
//           {
//             label: '参考成本价',
//             prop: 'costPrice',
//             type: 'number',
//             cell: true,
//             width: "120",
//             precision: 2
//           },
//           {
//             label:'库存',
//             prop: 'modelStorage',
//             type: 'number',
//             cell: true,
//             width: "120"
//           },
//           {
//             label: '单位',
//             prop: 'modelUnit',
//             type: 'input',
//             cell: true,
//             width: "120"
//           },
//           {
//             label: '重量',
//             prop: 'weight',
//             type: 'number',
//             cell: true,
//             width: "120",
//             precision: 3
//           },
//           {
//             label: '体积',
//             prop: 'volume',
//             type: 'number',
//             cell: true,
//             width: "120",
//             precision: 6
//           },
//           {
//             label: '库存预警值',
//             prop: 'stocksForewarn',
//             type: 'number',
//             cell: true,
//             width: "120"
//           },
//           {
//             label: '扫码购编码',
//             prop: 'modelCode',
//             type: 'input',
//             cell: true,
//             width: "250"
//           }
//         ]
//       }
      