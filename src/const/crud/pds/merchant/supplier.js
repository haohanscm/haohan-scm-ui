/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '#',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 100,
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  viewBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [
    {
      label: '供应商名称',
      prop: 'supplierName',
      sortable: true,
      search: true
    },
    {
      label: '简称',
      prop: 'shortName'
    }, {
      label: '联系人',
      prop: 'contact'
    },
    {
      label: '联系电话',
      prop: 'telephone',
    },
    {
      label: '账期',
      prop: 'payPeriod',
      dicUrl: '/admin/dict/type/scm_pay_period'
    },  {
      label: '供应商地址地址',
      prop: 'address',
    },{
      label: '商家名称',
      prop: 'merchantName',
      search: true
    },{
      label: '供应商类型',
      prop: 'supplierType',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/supplier_type'
    },{
      label: '启用状态',
      prop: 'status',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_yes_no'
    }, {
      label: '是否绑定通行证',
      prop: 'uid',
    }, {
      label: '消息推送',
      prop: 'needPush',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_yes_no'
    }
  ]
}


