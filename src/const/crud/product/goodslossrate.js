/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '商品名称',
            prop: 'goodsId',
            search:true
        },
        {
            label: '商品规格名称',
            prop: 'goodsModelId'
        },
        {
            label: '损耗率',
            prop: 'lossRate'
        },
        {
            label: '损耗类型',
            prop: 'lossType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_loss_type '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
