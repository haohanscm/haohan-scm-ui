/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '货品名称',
            prop: 'productName',
            search:true
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '货品数量',
            prop: 'productNumber'
        },
        {
            label: '货品质量',
            prop: 'productQuality',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_product_quality '
        },
        {
            label: '货品位置状态',
            prop: 'productPlaceStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_product_place_status '
        },
        {
            label: '货品编号',
            prop: 'productSn'
        },
        {
            label: '处理描述',
            prop: 'operateDesc'
        },
        {
            label: '操作人名称',
            prop: 'operatorName'
        },
        {
            label: '操作时间',
            prop: 'operateTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
