/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '送货单号',
            prop: 'shipId'
        },
        {
            label: '交易单号',
            prop: 'tradeId'
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            search:true
        },
        {
            label: '商品数量',
            prop: 'goodsNum'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        {
            label: '送货单明细编号',
            prop: 'shipDetailSn'
        },
    ]
}
