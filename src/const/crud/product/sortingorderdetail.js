/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '分拣单编号',
            prop: 'sortingOrderSn'
        },
        {
            label: '分拣明细编号',
            prop: 'sortingDetailSn'
        },
        {
            label: '客户采购单明细id',
            prop: 'buyOrderDetailId'
        },
        {
            label: '采购商品名称',
            prop: 'goodsName',
            search:true
        },
        {
            label: '采购商品规格名称',
            prop: 'goodsModelName'
        },
        {
            label: '采购商品单位',
            prop: 'goodsUnit'
        },
        {
            label: '采购数量',
            prop: 'purchaseNumber'
        },
        {
            label: '货品编号',
            prop: 'productSn'
        },
        {
            label: '货品名称',
            prop: 'productName'
        },
        {
            label: '货品单位',
            prop: 'productUnit'
        },
        {
            label: '货品分拣数量',
            prop: 'sortingNumber'
        },
        {
            label: '分拣时间',
            prop: 'sortingTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '分拣人名称',
            prop: 'operatorName'
        },
        {
            label: '分拣状态',
            prop: 'sortingStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_sorting_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
