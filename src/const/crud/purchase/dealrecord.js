/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        //  {
        //   label: '账户id',
        //   prop: 'accountId'
        // },
        {
            label: '账户拥有人名称',
            prop: 'accountOwnerName'
            ,
            search: true,
            sortable: true
        },
        {
            label: '交易金额',
            prop: 'dealAmount'
        },
        {
            label: '收支方式',
            prop: 'payType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_pay_type',
            search: true,
            sortable: true
        },
        {
            label: '交易时间',
            prop: 'dealTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        //  {
        //   label: '交易方账户id',
        //   prop: 'dealAccountId'
        // },
        {
            label: '交易人名称',
            prop: 'dealerName',
            search: true,
            sortable: true
        },
        {
            label: '采购明细编号',
            prop: 'purchaseDetailSn',
            search: true,
            sortable: true
        },
        {
            label: '交易类型',
            prop: 'dealType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_deal_type',
            search: true,
            sortable: true
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        // {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
