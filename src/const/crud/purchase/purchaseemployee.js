/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId',
            search: true
        },
        {
            label: '通行证ID',
            prop: 'passportId'

        },
        {
            label: '名称',
            prop: 'name',
            search: true,
            sortable: true
        },
        {
            label: '联系电话',
            prop: 'telephone',
            search: true,
            sortable: true
        },
        {
            label: '采购员工类型',
            prop: 'purchaseEmployeeType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_employee_type',
            search: true,
            sortable: true
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_use_status',
            search: true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        }
    ]
}
