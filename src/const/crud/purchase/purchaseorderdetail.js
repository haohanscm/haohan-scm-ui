/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家id',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        {
            label: '采购单编号',
            prop: 'purchaseSn',
            search: true,
            sortable: true
        },
        {
            label: '采购明细编号',
            prop: 'purchaseDetailSn',
            search: true,
            sortable: true
        },
        {
            label: '汇总明细编号',
            prop: 'summaryDetailSn',
            search: true,
            sortable: true
        },
        //  {
        //   label: '商品id',
        //   prop: 'goodsId',
        //       search: true,
        //       sortable: true
        // },
        //  {
        //   label: '商品规格id',
        //   prop: 'goodsModelId',
        //       search: true,
        //       sortable: true
        // },
        //  {
        //   label: '商品分类id',
        //   prop: 'goodsCategoryId',
        //       search: true,
        //       sortable: true
        // },
        {
            label: '商品图片',
            prop: 'goodsImg',
            type: 'upload',
            imgWidth: 100,
            imgHeight: 50,
            listType: 'picture-img'
        },
        {
            label: '商品分类名称',
            prop: 'goodsCategoryName',
            search: true,
            sortable: true
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            search: true,
            sortable: true
        },
        {
            label: '规格名称',
            prop: 'modelName',
            search: true,
            sortable: true
        },
        {
            label: '单位',
            prop: 'unit',
            search: true,
            sortable: true
        },
        {
            label: '采购状态',
            prop: 'purchaseStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_status',
            search: true,
            sortable: true
        },
        {
            label: '采购订单分类',
            prop: 'purchaseOrderType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_order_type',
            search: true,
            sortable: true
        },
        {
            label: '采购截止时间',
            prop: 'buyFinalTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '发起人',
            prop: 'initiatorId',
            // search: true,
            sortable: true
        },
        {
            label: '执行人',
            prop: 'transactorId',
            // search: true,
            sortable: true
        },
        //  {
        //   label: '供应商',
        //   prop: 'supplierId'
        // },
        {
            label: '供应商名称',
            prop: 'supplierName'
            ,
            search: true,
            sortable: true
        },
        {
            label: '市场价',
            prop: 'marketPrice',
            // search: true,
            sortable: true
        },
        {
            label: '采购价',
            prop: 'buyPrice'
        },
        {
            label: '需求采购数量',
            prop: 'needBuyNum'
        },
        {
            label: '实际采购数量',
            prop: 'realBuyNum'
        },
        {
            label: '揽货时间',
            prop: 'receiveTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '付款方式',
            prop: 'payType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_pay_type',
            search: true,
            sortable: true
        },
        {
            label: '揽货方式',
            prop: 'receiveType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_receive_type',
            search: true,
            sortable: true
        },
        {
            label: '采购执行状态',
            prop: 'actionType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_action_type',
            search: true,
            sortable: true
        },
        {
            label: '采购方式类型',
            prop: 'methodType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_method_type',
            search: true,
            sortable: true
        },
        {
            label: '竞价截止时间',
            prop: 'biddingEndTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '报价状态',
            prop: 'offerType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_offer_type',
            search: true,
            sortable: true
        },
        {
            label: '请款类型',
            prop: 'lendingType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_lending_type',
            search: true,
            sortable: true
        },
        {
            label: '请款人类型',
            prop: 'lenderType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_lender_type',
            search: true,
            sortable: true
        },
        {
            label: '来源采购单明细编号',
            prop: 'originalPurchaseDetailSn',
            search: true,
            sortable: true
        },
        {
            label: '中标状态',
            prop: 'biddingStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_bidding_status',
            search: true,
            sortable: true
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
