/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家id',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        {
            label: '汇报类型',
            prop: 'reportType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_report_type',
            search: true,
            sortable: true
        },
        {
            label: '汇报分类',
            prop: 'reportCategoryType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_report_category_type',
            search: true,
            sortable: true
        },
        {
            label: '汇报状态',
            prop: 'reportStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_report_status',
            search: true,
            sortable: true
        },
        {
            label: '采购单编号',
            prop: 'purchaseSn',
            search: true,
            sortable: true
        },
        {
            label: '采购单明细编号',
            prop: 'purchaseDetailSn',
            search: true,
            sortable: true
        },
        //  {
        //   label: '汇报人',
        //   prop: 'reporterId',
        //       search: true,
        //       sortable: true
        // },
        {
            label: '汇报人名称',
            prop: 'reporterName',
            search: true,
            sortable: true
        },
        {
            label: '汇报内容',
            prop: 'reportContent'
        },
        {
            label: '汇报时间',
            prop: 'reportTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '汇报图片',
            prop: 'reportPhoto',
            type: 'upload',
            imgWidth: 100,
            imgHeight: 50,
            listType: 'picture-img'
        },
        //  {
        //   label: '反馈人',
        //   prop: 'answerId'
        // },
        {
            label: '反馈人名称',
            prop: 'answerName',
            search: true,
            sortable: true
        },
        {
            label: '反馈内容',
            prop: 'answerContent'
        },
        {
            label: '反馈时间',
            prop: 'answerTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
