/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    editBtn:false,
    delBtn:false,
    addBtn:false,
    column: [
        {
            label: '主键',
            prop: 'id',
            hide: true
        },
        {
            label: '平台商家ID',
            prop: 'pmId',
            hide: true
        },
        {
            label: '通行证ID',
            prop: 'passportId',
            hide: true
        },
        {
            label: '商家ID',
            prop: 'merchantId',
            hide: true
        },
        {
            label: '采购商名称',
            prop: 'buyerName',
            search: true
        },
        {
            label: '采购商简称',
            prop: 'shortName'
        },
        {
            label: '商家',
            prop: 'merchantName',
            search: true
        },
        {
            label: '联系人',
            prop: 'contact'
        },
        {
            label: '手机号码',
            prop: 'telephone'
        },
        {
            label: '供应商地址',
            prop: 'address',
            hide: true
        },
        {
            label: '账期',
            prop: 'payPeriod',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_pay_period',
            hide: true
        },
        {
            label: '是否需确认订单',
            prop: 'needConfirmation',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            hide: true
        },
        {
            label: '采购商类型',
            prop: 'buyerType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_buyer_type',
            search: true
        },
        {
            label: '消息推送',
            prop: 'needPush',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true
        },
        {
            label: '是否启用',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_use_status',
            search: true
        },
        {
            label: '排序值',
            prop: 'sort',
            hide: true
        },
        {
            label: '用户id',
            prop: 'userId',
            hide: true
        },
        {
            label: '区域地址',
            prop: 'area',
        },
        {
            label: '经度',
            prop: 'longitude'
        },
        {
            label: '纬度',
            prop: 'latitude'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
    ]
}
