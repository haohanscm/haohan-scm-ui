/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    editBtn: false,
    selection: true,
    delBtn:false,
    addBtn:false,
    viewBtn: false,
    column: [
        {
            label: '订单编号',
            prop: 'orderId',
            minWidth: 120,
            search: true,
        },
        {
            label: '用户姓名',
            prop: 'realName',
            search: true,
        },
        {
            label: '用户电话',
            prop: 'userPhone',
            minWidth:120,
            search: true,
        },
        {
            label: '详细地址',
            prop: 'userAddress',
            minWidth:120,
            overHidden: true,
        },
        {
            label: '订单商品总数',
            prop: 'totalNum',
            minWidth:120,
        },
        {
            label: '订单总价',
            prop: 'totalPrice'
        },
        {
            label: '邮费',
            prop: 'totalPostage'
        },
        {
            label: '实际支付金额',
            prop: 'payPrice',
            minWidth:120,
        },
        {
            label: '支付邮费',
            prop: 'payPostage'
        },
        {
            label: '抵扣金额',
            prop: 'deductionPrice'
        },
        {
            label: '优惠券金额',
            prop: 'couponPrice',
            minWidth:100,
        },
        {
            label: '支付状态',
            prop: 'paid',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true,
        },
        {
            label: '支付时间',
            prop: 'payTime',
            type: "timestamp",
            format: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '支付方式',
            prop: 'payType'
        },
        {
            label: '创建时间',
            prop: 'addTime'
        },
        {
            label: '订单状态',
            prop: 'status'
        },
        {
            label: '退款状态',
            prop: 'refundStatus'
        },
        {
            label: '发货类型',
            prop: 'deliveryType',
        },
        {
            label: '快递单号/手机号',
            prop: 'deliveryId',
            minWidth:100,
        },
        {
            label: '备注',
            prop: 'mark'
        },
        {
            label: '商户ID',
            prop: 'merId'
        },
        {
            label: '支付渠道',
            prop: 'isChannel'
        },
        {
            label: '是否已转换',
            prop: 'isScm',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true,
            searchLabelWidth: 100,
            minWidth: 100,
        },
    ]
}
