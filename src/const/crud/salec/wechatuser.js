/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '微信用户id',
      prop: 'uid'
    },
	  {
      label: '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
      prop: 'unionid'
    },
	  {
      label: '用户的标识，对当前公众号唯一',
      prop: 'openid'
    },
	  {
      label: '小程序唯一身份ID',
      prop: 'routineOpenid'
    },
	  {
      label: '用户的昵称',
      prop: 'nickname'
    },
	  {
      label: '用户头像',
      prop: 'headimgurl',
      type: 'img',
      dataType: 'string',
    },
	  {
      label: '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
      prop: 'sex'
    },
	  {
      label: '用户所在城市',
      prop: 'city'
    },
	  {
      label: '用户的语言，简体中文为zh_CN',
      prop: 'language'
    },
	  {
      label: '用户所在省份',
      prop: 'province'
    },
	  {
      label: '用户所在国家',
      prop: 'country'
    },
	  {
      label: '公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注',
      prop: 'remark'
    },
	  {
      label: '用户所在的分组ID（兼容旧的用户分组接口）',
      prop: 'groupid'
    },
	  {
      label: '用户被打上的标签ID列表',
      prop: 'tagidList'
    },
	  {
      label: '用户是否订阅该公众号标识',
      prop: 'subscribe'
    },
	  {
      label: '关注公众号时间',
      prop: 'subscribeTime'
    },
	  {
      label: '添加时间',
      prop: 'addTime'
    },
	  {
      label: '一级推荐人',
      prop: 'stair'
    },
	  {
      label: '二级推荐人',
      prop: 'second'
    },
	  {
      label: '一级推荐人订单',
      prop: 'orderStair'
    },
	  {
      label: '二级推荐人订单',
      prop: 'orderSecond'
    },
	  {
      label: '佣金',
      prop: 'nowMoney'
    },
	  {
      label: '小程序用户会话密匙',
      prop: 'sessionKey'
    },
	  {
      label: '用户类型',
      prop: 'userType'
    },
  ]
}
