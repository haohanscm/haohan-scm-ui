/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '单号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn:false,
  addBtn:false,
  editBtn: false,
  column: [
    {
      label: '采购类型',
      prop: 'purchaseType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      sortable: true,
      search: true,
      hide: true
    }, {
      label: '单据状态',
      prop: 'supplyName',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      sortable: true,
      search: true,
      hide: true
    }, {
      label: '客户名称',
      prop: 'customeName'
    }, {
      label: '单据金额',
      prop: 'telephone'
    }, {
      label: '类型',
      prop: 'amountPayable'
    }, {
      label: '单据状态',
      prop: 'amountPaid'
    }, {
      label: '创建日期',
      prop: 'date',
      type: 'date',
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      search: true
    },{
      label: '制单人',
      prop: 'deliveryMethod'
    },{
      label: '操作',
      prop: 'SettlementStatus'
    }
  ]
}


