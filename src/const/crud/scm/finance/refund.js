/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '退款编号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn:false,
  addBtn:false,
  editBtn: false,
  column: [
    {
      label: '退款单号',
      prop: 'purchaseType',
      type: 'text',
      sortable: true,
      search: true,
      hide: true
    }, {
      label: '采购商',
      prop: 'purchasingAgent'
    },{
      label: '退款状态',
      prop: 'refundStatus',
      type: 'select',
      //dicUrl: '/admin/dict/type/scm_purchase_order_type',
      //sortable: true,
      search: true,
    }, {
      label: '采购类型',
      prop: 'OriginalReturnNumber',
      type: 'select',
      // dicUrl: '/admin/dict/type/scm_purchase_order_type',
      sortable: true,
      search: true,
      hide: true
    },{
      label: '采购类型',
      prop: 'procurementType'
    }, {
      label: '退款日期',
      prop: 'refundDate'
    },{
      label: '原退货单号',
      prop: 'operate'
    },{
      label: '退款金额',
      prop: 'refundAmount'
    }
  ]
}


