/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    delBtn:false,
    addBtn:false,
    editBtn: false,
    column: [
        {
            label: '退货单号',
            prop: 'returnSn',
            search: true,
        }, {
            label: '原订单号',
            prop: 'orderSn',
            search: true,
        }, {
            label: '客户姓名',
            prop: 'returnCustomer'
        },{
            label: '货品金额',
            prop: 'sumAmount',
        },{
            label: '其他费用',
            prop: 'otherAmount',
            sortable:true,
        },
        {
            label:'合计金额',
            prop:'totalAmount'
        },
        {
            label:'申请时间',
            prop:'createDate',
            type: 'datetime',
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true,
            sortable:true,
        },{
            label:"退货原因",
            prop:"returnReason"
        },{
            label:"退货状态",
            prop:"returnStatus",
            dicUrl:'/admin/dict/type/scm_aftersales_return_type',
            type: 'select',
            search: true
        }
    ]
}


