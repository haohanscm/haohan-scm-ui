/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: true,
    addBtn:false,
    editBtn: true,
    delBtn:false,
    column: [
        {
            label: '企业名称',
            prop: '',
            search: true,
        }, {
            label: '企业类型',
            prop: '',
        }, {
            label: '合同ID',
            prop: '',
        }, {
            label: '合同名称',
            prop: ''
        }, {
            label: '合同签署时间',
            prop: '',
            type: 'datetime',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
        },
        {
            label: '操作人',
            prop: ''
        }
    ]
}


