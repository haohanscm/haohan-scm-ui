/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: false,
    viewBtn: false,
    // dateBtn: true,
    addBtn:false,
    delBtn:false,
    editBtn: false,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            search: true,
            sortable: true,
        },{
            label: '商品图片',
            prop: 'goodsImg',
            type: 'upload',
            imgWidth: 100,
            imgHeight: 50,
            listType: 'picture-img'
        },{
            label: '商品规格',
            prop: 'modelName'
        },{
            label: '商品分类',
            prop: 'goodsCategoryName',
            type: 'select',
        }, {
            label: '记录人',
            prop: 'transactorName'
        },{
            label: '供应商',
            prop: 'supplierName'
        }, {
            label: '市场价',
            prop: 'marketPrice'
        },  {
            label: '日期',
            prop: 'recordTime',
            type: 'datetime',
            more:true,
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true
        }
    ]
}


