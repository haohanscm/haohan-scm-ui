/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    viewBtn: false,
    addBtn:false,
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '主键',
            prop: 'id',
            hide: true
        },
        {
            label: '平台商家',
            prop: 'pmId',
            hide: true
        },
        {
            label: '名称',
            prop: 'name',
            search: true,
            sortable: true
        },
        {
            label: '联系电话',
            prop: 'telephone',
            search: true,
            sortable: true
        },
        {
            label: '采购员工类型',
            prop: 'purchaseEmployeeType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_employee_type',
            search: true,
            sortable: true
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_use_status',
            search: true,
            sortable: true
        },
        {
            label: '小程序启用',
            prop: 'passportId',
            solt: true,
            sortable: true
        },
        {
            label: '后台启用',
            prop: 'userId',
            solt: true,
            sortable: true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            hide: true,
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea',
            hide: true
        }
    ]
}
