/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    // selection: true,
    viewBtn: false,
    // dateBtn: true,
    addBtn:false,
    editBtn: false,
    column: [
        {
            label: '采购退货单号',
            prop: 'returnSn',
            search: true,
            sortable: true,
        },{
            label: '退货金额',
            prop: 'returnAmount'
        } ,{
            label: '原采购单',
            prop: 'orderSn'
        }, {
            label: '退货类型',
            prop: 'returnType',
            dicUrl: '/admin/dict/type/scm_aftersales_return_satus',
            search: true
        },{
            label: '采购员',
            prop: 'operatorId'
        },{
            label: '制单人',
            prop: 'initiator'
        },{
            label: '制单时间',
            prop: 'applyTime',
            type: 'datetime',
            minWidth:'160px',
            more:true,
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            // search: true
        }, {
            label: '审核状态',
            prop: 'returnStatus',
            dicUrl:'/admin/dict/type/scm_aftersales_return_type'
        }
    ]
}


