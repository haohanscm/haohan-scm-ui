export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: true,
    addBtn: false,
    editBtn: false,
    delBtn:false,
    menu:false,
    column: [
        {
            label: '商品名称',
            prop: '',
            search:true
        },
        {
            label: 'SKU编码',
            prop: '',
            search:true
        },
        {
            label: '平均单价',
            prop: ''
        },
        {
            label: '采购量',
            prop: ''
        },
        {
            label: '剩余库存',
            prop: ''
        },
        {
            label: '已采购单数量',
            prop: ''
        },
        {
            label:'采购总金额',
            prop:''
        },

    ]
}