export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: true,
    addBtn: false,
    editBtn: false,
    delBtn:false,
    column: [
        {
            label: '供应商名称',
            prop: 'supplierName',
            search:true
        },
        {
            label: '联系人姓名',
            prop: 'contact',
            search:true
        },
        // {
        //     label: '注册来源',
        //     prop: ''
        // },
        {
            label: '手机号码',
            prop: 'telephone'
        },
        // {
        //     label: '采购员',
        //     prop: ''
        // },
        // {
        //     label: '添加时间',
        //     prop: 'createDate'
        // },
        {
            label:'星级',
            prop:'supplierLevel',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_supplier_level',
        },
        {
            label: '账期',
            prop: 'payPeriod',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_pay_period',
            hide: true
        },
        // {
        //     label: '供应商类型',
        //     prop: 'supplierType',
        //     type: 'select',
        //     dicUrl: '/admin/dict/type/pds_supplier_type',
        //     search: true
        // },
        {
            label: '消息推送',
            prop: 'needPush',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            search: true
        },
        {
            label: '是否启用',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_use_status',
            search: true
        },
        {
            label: '排序值',
            prop: 'sort'
        },
        {
            label: '部门',
            prop: 'area',
        },
        {
            label: '经度',
            prop: 'longitude'
        },
        {
            label: '纬度',
            prop: 'latitude'
        },
    ]
}
