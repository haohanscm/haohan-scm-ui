export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: true,
    // dateBtn: true,
    addBtn: false,
    editBtn: false,
    column: [
        {
            label: '供应商名称',
            prop: '',
            search: true
        }, {
            label: '联系人姓名',
            prop: ''
        }, {
            label: '注册来源',
            prop: ''
        }, {
            label: '手机号码',
            prop: ''
        }, {
            label: '添加时间',
            prop: ''
        }, {
            label: '星级',
            prop: ''
        }
    ]
}


