/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [{
      label: '盘点单号',
      prop: 'afterProductSn',
      search: true
    },
     {
      label: '商品名称',
      prop: 'prodcutName',
      type: 'datetime'
    },
    //  {
    //   label: '入库类型',
    //   prop: 'supplyName',
    //   type: 'select',
    //   dicUrl: '/admin/dict/type/scm_purchase_order_type',
    //   sortable: true,
    //   search: true,
    //   hide: true
    // },
    {
      label: '仓库',
      prop: 'supply',
      type: 'select',
      search: true,
    }, {
      label: '商品数',
      prop: 'resultNumber',
      type: 'datetime'
    },
    {
      label: '类型',
      prop: 'lossType',
      dicUrl: '/admin/dict/type/scm_loss_type',
      type: 'select',
      search: true,

    },
    //  {
    //   label: '入库数量',
    //   prop: 'num'
    // },
    //  {
    //   label: '关联单号',
    //   prop: 'amount'
    // },
    {
      label: '创建日期',
      prop: 'createDate',
      type: 'datetime',
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      search: true
    }
  ]
}
