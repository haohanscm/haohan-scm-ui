/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: false,
    delBtn:false,
    addBtn:false,
    editBtn: false,
    column: [
        {
            label: '入库单号',
            prop: 'enterWarehouseSn',
            search: true
        }, {
            label: '入库类型',
            prop: 'enterType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_enter_type ',
            sortable: true,
            search: true,
            hide: true
        }, {
            label: '仓库',
            prop: 'warehouseSn'
        }, {
            label: '入库状态',
            prop: 'enterStatus',
            type: 'select',
            dicUrl:'/admin/dict/type/scm_enter_status ',
            sortable: true,
            search: true,

        }, {
            label: '入库数量',
            prop: 'num'
        }, {
            label: '关联单号',
            prop: 'amount'
        }, {
            label: '制单人',
            prop: 'amount'
        },{
            label: '创建日期',
            prop: 'applyTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true
        }
    ]
}


