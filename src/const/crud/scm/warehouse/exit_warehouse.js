/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: false,
    delBtn:false,
    addBtn:false,
    editBtn: false,
    column: [
        {
            label: '出库单号',
            prop: 'exitWarehouseSn',
            search: true
        }, {
            label: '出库类型',
            prop: 'exitType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_exit_type',
            sortable: true,
            search: true,
            hide: true
        }, {
            label: '仓库',
            prop: 'warehouseSn',
        }, {
            label: '出库状态',
            prop: 'exitStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_exit_status',

        }, {
            label: '出库数量',
            prop: 'num'
        }, {
            label: '关联单号',
            prop: 'amount'
        }, {
            label: '制单人',
            prop: 'auditorName'
        },{
            label: '创建日期',
            prop: 'applyTime',
            type: 'datetime',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true
        }
    ]
}


