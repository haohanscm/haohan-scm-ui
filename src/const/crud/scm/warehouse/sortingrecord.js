/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '采购商',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  // selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [{
      label: '商品名称',
      prop: 'check',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      search: true

    },
    // {
    //   label: '仓库',
    //   prop: 'warehouse',
    //   type: 'select',
    //   dicUrl: '/admin/dict/type/scm_purchase_order_type',
    //   // sortable: true,
    //   search: true,
    //   hide: true
    // },
    // {
    //   label: '商品名称',
    //   prop: 'warehouse',
    //   type: 'select',
    //   dicUrl: '/admin/dict/type/scm_purchase_order_type',
    //   // sortable: true,
    //   search: true,
    //   // hide: true
    // },
    {
      label: '规格',
      prop: 'supply',
    }, {
      label: '单位',
      prop: 'num'
    }, {
      label: '订购数量',
      prop: 'amount'
    }, {
      label: '实际数量',
      prop: 'amount'
    },
    {
      label: '分拣状态',
      prop: 'amount'
    },
    //  {
    //   label: '分拣状态',
    //   prop: 'amount',
    //   type: 'datetime',
    //   format: "yyyy-MM-dd",
    //   valueFormat: "yyyy-MM-dd HH:mm:ss",
    //   search: true
    // }
  ]
}
