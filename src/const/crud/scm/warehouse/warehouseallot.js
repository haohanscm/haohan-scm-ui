/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [{
      label: '单号',
      prop: 'purchaseType',
      search: true,
      hide: true,
    },
    {
      label: '调拨单号',
      prop: 'inWarehouseSn'
    },
    {
      label: '调拨时间',
      prop: 'auditTime'
    },
    //  {
    //   label: '入库类型',
    //   prop: 'supplyName',
    //   type: 'select',
    //   dicUrl: '/admin/dict/type/scm_purchase_order_type',
    //   sortable: true,
    //   search: true,
    //   hide: true
    // },
    {
      label: '调入仓库',
      prop: 'warehouseAllotSn',
      type: 'select',
      search: true,
    },
    //  {
    //   label: '商品数',
    //   prop: 'numgoods',
    //   type: 'datetime'
    // }, {
    //   label: '金额',
    //   prop: 'num'
    // },
    {
      label: '调出仓库',
      prop: 'telephone',
      type: 'select',
      search: true,

    },
    {
      label: '调拨状态',
      prop: 'telephone',
      type: 'select',
      search: true,

    },
    //  {
    //   label: '入库数量',
    //   prop: 'num'
    // },
    //  {
    //   label: '关联单号',
    //   prop: 'amount'
    // },
    {
      label: '制单人',
      prop: 'amount'
    },
    // {
    //   label: '创建日期',
    //   prop: 'actionTime',
    //   type: 'datetime',
    //   format: "yyyy-MM-dd",
    //   valueFormat: "yyyy-MM-dd HH:mm:ss",
    //   search: true
    // }
  ]
}
