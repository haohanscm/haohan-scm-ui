/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  // selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [{
      label: '商品名称',
      prop: 'goodsName',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      search: true,
    },
    {
      label: '品类',
      prop: 'categoryName',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      hide: true,
      search: true,
    },
    {
      label: '单位',
      prop: 'modelUnit',
    },
    {
      label: '仓库',
      prop: 'warehouse',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      // sortable: true,
      search: true,
      // hide: true
    },
    {
      label: '目前库存数量',
      prop: 'modelStorage'
    },
     {
      label: '库存状态',
      prop: 'remarks'
    }
  ]
}
