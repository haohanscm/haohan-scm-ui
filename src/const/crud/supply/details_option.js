export const tableOption = {
    page: false,
    "border": true,
    "index": true,
    // "selection": true,
    // indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    // labelWidth: 1,
    // 列显隐按钮
    refreshBtn:false,
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        {
            label: '商品图片',
            prop: 'goodsImg',
            minWidth: 100,
            type:'img',
            dataType: 'string',
            // span: 24,
            // listType: 'picture-img',
            // imgWidth:140,
            // imgHeight:140,
            // listType:'picture-img',
            // searchLabelWidth: 100,
        },
        {
        label: '商品名称',
        prop: 'goodsName',
        minWidth: 100,
        searchLabelWidth: 100,
    }, {
        label: '规格名称',
        prop: 'modelName',
        minWidth: 100,
        searchLabelWidth: 100,
    }, {
        label: '单位',
        prop: 'unit',
        // type: 'select',
        minWidth: 100,

    }, {
        label: '下单数量',
        prop: 'orderGoodsNum',
        minWidth: 100,

    }, {
        label: '市场价格',
        prop: 'marketPrice',
        minWidth: 100,

    }
        , {
        label: '采购价格',
        prop: 'buyPrice',
        minWidth: 100,

    },
    {
        label: '商品金额',
        prop: 'amount',
        minWidth: 100,
        slot: true
    }
    ,
    {
        label: '采购明细编号',
        prop: 'buyDetailSn',
        minWidth: 120,
        // slot: true,
    },
    {
        label: '供应订单编号',
        prop: 'supplySn',
        minWidth: 120,
        slot: true,
    }, {
        label: '备注信息',
        prop: 'remarks',
        minWidth: 100,
    }, {
        label: '状态',
        prop: 'status',
        dicUrl: '/admin/dict/type/scm_buy_order_status',
        minWidth: 100,
        // search: true4
    }, {
        label: '供应商',
        prop: 'supplyId',
        slot: true,
        minWidth: 140,
    }
    ]
}