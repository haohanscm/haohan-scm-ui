/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
// 下面展示数据行
export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        // {
        //     label: '主键',
        //     prop: 'id'
        // },
        {
            label: '订单号',
            prop: 'supplySn',
            minWidth:100,
            span: 8
            // search: true
        },
        {
            label: '商品规格编号',
            prop: 'goodsModelSn',
            // search: true,
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            // search: true,
        },
        {
            label: '商品图片',
            prop: 'goodsImg',
            type:'upload',
            imgWidth:80,
            imgHeight:80,
            listType:'picture-img',

        },
        {
            label: '商品规格名称',
            prop: 'modelName',
            // search: true,
        },{
            
            label: '采购数量',
            prop: 'goodsNum',
            // search: true,
        },{
            label: '成交价格',
            prop: 'dealPrice',
            // search: true,
        },{
            label: '单位',
            prop: 'unit',
            // search: true,
        },{
            label: '金额',
            prop: 'amount',
            // search: true,
        },

    ]
}
