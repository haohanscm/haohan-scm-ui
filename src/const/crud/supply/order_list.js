/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    expand: true,
    // 搜索显隐按钮
    // searchBtn: false,
    // emptyBtnText:false,
    expandRowKeys: [1],
    selection: true,
    rowKey: 'id',
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    // border: false,
    column: [
        {
            label: '订单号',
            prop: 'supplySn',
            minWidth: 100,
            search: true
        },
        {
            label: '购买人',
            prop: 'contact',
            search: true,
        },
        {
            label: '状态',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_buy_order_status',
            search: true,
        },
        {
            label: '供应商',
            prop: 'supplierName',
            search: true,
        }, {

            label: '下单时间',
            prop: 'orderTime',
            minWidth: 100,
            // search: true,
        }, {
            label: '供应日期',
            prop: 'supplyDate',
            minWidth: 100,
            type: "datetime",
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            search: true,
        }, {
            label: '联系电话',
            prop: 'telephone',
            minWidth: 120,
            search: true,
        }, {
            label: '供应地址',
            prop: 'address',
            overHidden: true,
            minWidth: 110,
            search: true,
        }, {
            label: '成交时间',
            prop: 'dealTime',
            // dicUrl: '/admin/dict/type/scm_delivery_type',
            minWidth: 110,
            // search: true,
        },
        {

            label: '配送方式',
            prop: 'deliveryType',
            // value: 1,
            dicUrl: '/admin/dict/type/scm_delivery_type',
            minWidth: 100,
            search: true,
            // label: '配送方式',
            // prop: 'deliveryType',
            type: 'select',
            // dicUrl: '/admin/dict/type/scm_delivery_type',
            // 
        },
        {
            label: '其他金额',
            prop: 'otherAmount',
            // search: true,
        },
        {
            label: '商品合计金额',
            prop: 'sumAmount',
            minWidth: 110,
            // search: true,
        },
        {
            label: '供应总金额',
            prop: 'totalAmount',
            minWidth: 110,
            // search: true,
        },
    ]
}
