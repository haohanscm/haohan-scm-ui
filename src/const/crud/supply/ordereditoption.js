export const ordereditoption = {
  submitBtn: false,
  column: [{
    label: '业务员',
    prop: 'name',
    span: 8,
    formslot: true
  },
  {
    label: '用户名称',
    prop: 'text',
    span: 8,
    formslot: true
  },
  {
    label: '交货日期',
    prop: 'date',
    span: 8,
    formslot: true
  },
  {
    label: '销售单类型',
    prop: 'type',
    span: 8,
    formslot: true
  },
  {
    label: '收货地址',
    prop: 'address',
    span: 8,
    formslot: true
  },
  {
    label: '备注',
    prop: 'remark',
    span: 8,
    formslot: true
  },
  {
    label: '其它金额',
    prop: 'number1',
    span: 8,
    formslot: true
  },
  {
    label: '商品合计金额',
    prop: 'number2',
    span: 8,
    formslot: true
  },
  {
    label: '总计金额',
    prop: 'number3',
    span: 8,
    formslot: true
  },
  {
    label: '优惠金额',
    prop: 'number4',
    span: 8,
    formslot: true
  }]
}
