export const tableOption = {
    "border": true,
    "index": true,
    "selection":true,
    // indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    // labelWidth: 1,
    
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [{
        label: '采购商名称',
        prop: 'buyerName',
        minWidth: 100,
        search: true,
        searchLabelWidth: 100,
    },
    {
        label: '采购时间',
        prop: 'buyTime',
        minWidth: 100,
        search: true,
        searchLabelWidth: 100,
    },{
        label: '采购单编号',
        prop: 'buyOrderSn',
        minWidth: 100,
        search: true,
        searchLabelWidth: 100,
    },{
        label: '状态',
        prop: 'status',
        type: 'select',
        dicUrl: '/admin/dict/type/scm_buy_order_status',
        minWidth: 100,
        search: true
    },{
        label: '送货批次',
        prop: 'buySeq',
        dicUrl: '/admin/dict/type/scm_delivery_seq',
        minWidth: 100,
        search: true
    },{
        label: '送货日期',
        prop: 'deliveryDate',
      type: "datetime",
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd",
        minWidth: 100,
        search: true
    }
    ,{
        label: '采购需求',
        prop: 'needNote',
        minWidth: 100,
        search: true
    },{
        label: '总价预估',
        prop: 'genPrice',
        minWidth: 100,
        search: true
    },{
        label: '采购总价',
        prop: 'totalPrice',
        minWidth: 100,
        search: true
    },{
        label: '联系人',
        prop: 'contact',
        minWidth: 100,
        search: true
    },{
        label: '联系电话',
        prop: 'telephone',
        minWidth: 100,
        search: true
    },{
        label: '配送地址',
        prop: 'address',
        minWidth: 100,
        search: true
    },{
        label: '成交时间',
        prop: 'dealTime',
        minWidth: 100,
        search: true
    },{
        label: '运费',
        prop: 'shipFee',
        minWidth: 100,
        search: true
    },{
        label: '零售单号',
        prop: 'goodsOrderId',
        minWidth: 100,
        search: true
    },{
        label: '备注信息',
        prop: 'remarks',
        minWidth: 100,
        search: true
        
    },{
        label: '配送方式',
        prop: 'deliveryType',
        dicUrl: '/admin/dict/type/scm_delivery_type',
        minWidth: 100,
        // search: true
    },
]
}