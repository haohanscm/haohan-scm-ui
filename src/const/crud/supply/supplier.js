/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '通行证ID',
            prop: 'passportId'
        },
        {
            label: '商家名称',
            prop: 'merchantName',
            search:true
        },
        {
            label: '全称',
            prop: 'supplierName'
        },
        {
            label: '供应商名称',
            prop: 'shortName'
        },
        {
            label: '联系人',
            prop: 'contact'
        },
        {
            label: '联系电话',
            prop: 'telephone'
        },
        {
            label: '微信',
            prop: 'wechatId'
        },
        {
            label: '操作员',
            prop: 'operator'
        },
        {
            label: '账期',
            prop: 'payPeriod',
        },
        {
            label: '供应商地址',
            prop: 'address'
        },
        {
            label: '标签',
            prop: 'tags'
        },
        {
            label: '启用状态',
            prop: 'status',
            type:'select',
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '供应商类型',
            prop: 'supplierType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_supply_type '
        },
        {
            label: '是否开启消息推送',
            prop: 'needPush',
            type:'select',
            dicUrl:'/admin/dict/type/scm_yes_no '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        {
            label: '评级:1星-5星',
            prop: 'supplierLevel',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_customer_level '
        },
        {
            label: '区域地址',
            prop: 'area',
        },
        {
            label: '经度',
            prop: 'longitude'
        },
        {
            label: '纬度',
            prop: 'latitude'
        },
    ]
}
