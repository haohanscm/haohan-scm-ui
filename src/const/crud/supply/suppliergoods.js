/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '供应商名称',
            prop: 'shortName',
            search:true
        },
        {
            label: '商家名称',
            prop: 'supplierMerchantId'
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            search:true
        },
        {
            label: '商品规格名称',
            prop: 'ModelName'
        },
        {
            label: '启用状态',
            prop: 'status',
            type:'select',
            dicUrl:'/admin/dict/type/scm_use_status '

        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        {
            label: '供应类型',
            prop: 'supplyType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_supply_type '
        },
    ]
}
export const bindTableOption = {
  emptyText: '暂无数据',
  selection: false,
  index: true,
  indexLabel: '序号',
  addBtn: false,
  editBtn: false,
  refreshBtn: false,
  columnBtn: false,
  align: 'center',
  menuAlign: 'center',
  border: true,
  header: false,
  menu: false,
  height: 400,
  column: [
    {
      label: '商品图片',
      prop: 'modelUrl',
      type: 'img',
      dataType: 'string'
    },
    {
      label: '商品名称',
      prop: 'goodsName',
    },
    {
      label: '规格',
      prop: 'modelName',
    },
    {
      label: '单位',
      prop: 'modelUnit',
    },
    {
      label: '售价',
      prop: 'modelPrice',
    },
    {
      label: '操作',
      prop: 'handle',
      slot: true
    }
  ]
}
export const relateSupplygoods = {
  emptyText: '暂无数据',
  selection: false,
  index: true,
  indexLabel: '序号',
  addBtn: false,
  editBtn: false,
  refreshBtn: false,
  columnBtn: false,
  align: 'center',
  menuAlign: 'center',
  border: true,
  header: false,
  menu: false,
  height: 400,
  column: [
    {
      label: '商品图片',
      prop: 'modelUrl',
      type: 'img',
      dataType: 'string'
    },
    {
      label: '商品名称',
      prop: 'goodsName',
    },
    {
      label: '规格',
      prop: 'modelName',
    },
    {
      label: '单位',
      prop: 'modelUnit',
    },
    {
      label: '售价',
      prop: 'modelPrice',
    },
    {
      label: '库存',
      prop: 'modelStorage',
    },
    {
      label: '供应商',
      prop: 'modelList',
      minWidth: "150",
      value: [],
      slot: true
    },
    {
      label: '操作',
      prop: 'handle',
      slot: true
    }
  ]
}
