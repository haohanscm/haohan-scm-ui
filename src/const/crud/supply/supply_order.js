/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

// 收货相关和订单相关信息
export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '订单信息',
            prop: 'group1',
            column: [
                {
                    label: '订单号',
                    prop: 'supplySn',
                    span: 8
                },
                {
                    label: '状态',
                    prop: 'status',
                    span: 8,
                    dicUrl: '/admin/dict/type/scm_buy_order_status',
                },
                {
                    label: '供应商名称',
                    prop: 'supplierName',
                    span: 8
                },
                {
                    label: '供应地址',
                    prop: 'address',
                    span: 8
                },
                {
                    label: '收货人地址',
                    prop: 'address',
                    span: 8
                },{
                    label: '下单时间',
                    prop: 'orderTime',
                    span: 8
                },{
                    label: '供应日期	',
                    prop: 'supplyDate',
                    span: 8
                },{
                    label: '需求描述',
                    prop: 'needNote',
                    span: 8
                },{
                    label: '供应总金额',
                    prop: 'totalAmount',
                    span: 8
                },{
                    label: '商品合计金额',
                    prop: 'sumAmount',
                    span: 8
                },{
                    label: '其他金额',
                    prop: 'otherAmount',
                    span: 8
                },{
                    label: '联系人',
                    prop: 'contact',
                    span: 8
                },{
                    label: '收货人电话',
                    prop: 'telephone',
                    span: 8
                },
                {
                    label: '成交时间',
                    prop: 'dealTime',
                    span: 8
                },
                {
                    label: '配送方式',
                    prop: 'deliveryType',
                    span: 8,
                    dicUrl: '/admin/dict/type/scm_delivery_type',
                }
            ]
        },
        {
            icon: 'el-icon-info',
            label: '用户信息',
            prop: 'group3',
            column: [
                {
                    label: '用户姓名',
                    prop: 'contact',
                    span: 8
                },
                {
                    label: '联系电话',
                    prop: 'telephone',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_order_type',
                    span: 8
                },
                {
                    "label": "下单时间",
                    "prop": "orderTime",
                    "type": "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    span: 8
                },

                {
                    label: '常用快递',
                    prop: 'totalAmount',
                    span: 8
                },
                {
                    label: '取货地址',
                    prop: 'address',
                    span: 8
                },
                {
                    label: '备注',
                    prop: 'sumAmount',
                    span: 8
                },
                {
                    label: '订单商品种类数',
                    prop: 'goodsNum',
                    span: 8
                },
                {
                    label: '备注信息',
                    prop: 'remarks',
                    maxlength: 255,
                    span: 12,
                    showWordLimit: true
                }
            ]
        }
    ]
}
