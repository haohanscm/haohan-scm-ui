/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

// 发货相关信息 做展示
export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '发货相关',
            prop: 'group2',
            column: [
                // {
                //     label: '平台商家',
                //     prop: 'pmName',
                //     span: 4
                // },
                // {
                //     label: '发货单编号',
                //     prop: 'shipRecordSn',
                //     span: 4
                // },
                {
                    label: '发货人名称',
                    prop: 'shipperName',
                    span: 4
                },
                {
                    label: '发货人电话',
                    prop: 'shipperTelephone',
                    span: 4
                },
                {
                    label: '发货地址',
                    prop: 'shipAddress',
                    span: 4
                },
                {
                    "label": "发货时间",
                    "prop": "shipTime",
                    "type": "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    span: 6
                },
                {
                    label: '发货状态',
                    prop: 'shipStatus',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_ship_record_status',
                    span: 4
                },
                {
                    label: '发货方式',
                    prop: 'shipType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_ship_type',
                    span: 4
                },
                {
                    label: '物流单号',
                    prop: 'logisticsSn',
                    span: 4
                },
                {
                    label: '物流公司',
                    prop: 'logisticsName',
                    span: 4
                },
            ]
        }
    ]
}
