export const supplygoodsdetail = {
  detail: true,
  group: [
    {
      icon: 'el-icon-info',
      label: '商品信息',
      prop: 'group1',
      column: [
        {
          label: '商品名称',
          prop: 'goodsName',
          span: 8
        },
        {
          label: '商品唯一编号',
          prop: 'goodsSn',
          disabled: true,
          span: 8
        },
        {
          label: '排序值(权重)',
          prop: 'sorting',
          span: 8
        },
        {
          label: '计量单位',
          prop: 'unit',
          span: 8
        },
        {
          label: '上下架',
          prop: 'isMarketable',
          span: 8,
          dicUrl: '/admin/dict/type/scm_yes_no'
          // formslot: true
        },
        {
          label: '商品状态',
          prop: 'goodsStatus',
          dicUrl: '/admin/dict/type/scm_goods_status',
          span: 8
        },
        {
          label: 'C端销售',
          prop: 'salecFlag',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '商品类型',
          prop: 'goodsType',
          dicUrl: '/admin/dict/type/scm_goods_type',
          span: 8
        },
        {
          label: '扫码购编码',
          prop: 'scanCode',
          span: 8
        },
        {
          label: '展示分类',
          prop: 'categoryName',
          span: 8
        },
        {
          label: '搜索关键词',
          prop: 'simpleDesc',
          type: 'text',
          span: 8
        },
        {
          label: '商品主图',
          prop: 'thumbUrl',
          span: 24,
          formslot: true
        },
        {
          label: '商品轮播图',
          prop: 'photoList',
          span: 24,
          formslot: true
        },
      ]
    },
    {
      icon: 'el-icon-info',
      label: '价格库存',
      prop: 'group2',
      column: [
        {
          label: '市场价',
          prop: 'marketPrice'
        },
        {
          label: '虚拟价格',
          prop: 'virtualPrice'
        },
        {
          label: '会员价',
          prop: 'vipPrice'
        },
        {
          label: '库存',
          prop: 'storage'
        }
      ]
    }
  ]
}
export const goodsoption = {
  title: '订单商品明细',
  page: false,
  border: true,
  index: true,
  indexWidth: 50,
  indexLabel: "序号",
  maxHeight: 400,
  stripe: true,
  header: false,
  align: "center",
  menu: false,
  column: [
    {
      label: '规格类型',
      prop: 'typeName'
    },
    {
      label: '规格名称',
      prop: 'modelName',
    },
    {
      label: '商品图片',
      prop: 'modelUrl',
      type: 'img',
      dataType: 'string'
    },
    {
      label: '售价',
      prop: 'modelPrice'
    },
    {
      label: '库存',
      prop: 'modelStorage',
      slot: true
    },
    {
      label: '单位',
      prop: 'modelUnit'
    },
    {
      label: "虚拟价",
      prop: 'virtualPrice'
    },
    {
      label: '参考成本价',
      prop: 'costPrice'
    },
    {
      label: '重量',
      prop: 'weight',
    },
    {
      label: "体积",
      prop: 'volume'
    },
    {
      label: '扫码购编码',
      prop: 'modelCode'
    },
    {
      label: "规格编码",
      prop: "goodsModelSn"
    },
    {
      label: '平台售卖',
      prop: 'relationType',
      slot: true
    },
    {
      label: '操作',
      prop: 'handle',
      slot: true
    }
  ]
}
export const goodsDetail = {
  title: '订单商品明细',
  page: false,
  border: true,
  index: true,
  indexWidth: 50,
  indexLabel: "序号",
  maxHeight: 400,
  stripe: true,
  header: false,
  align: "center",
  menu: false,
  column: [
    {
      label: '规格类型',
      prop: 'typeName'
    },
    {
      label: '规格名称',
      prop: 'modelName',
    },
    {
      label: '商品图片',
      prop: 'modelUrl',
      type: 'img',
      dataType: 'string'
    },
    {
      label: '售价',
      prop: 'modelPrice'
    },
    {
      label: '库存',
      prop: 'modelStorage',
      slot: true
    },
    {
      label: '单位',
      prop: 'modelUnit'
    },
    {
      label: "虚拟价",
      prop: 'virtualPrice'
    },
    {
      label: '参考成本价',
      prop: 'costPrice'
    },

    {
      label: '重量',
      prop: 'weight',
    },
    {
      label: "体积",
      prop: 'volume'
    },
    {
      label: '扫码购编码',
      prop: 'modelCode'
    },
    {
      label: "规格编码",
      prop: "goodsModelSn"
    }
  ]
}
