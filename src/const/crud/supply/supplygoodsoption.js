export const taboption = {
  column: [{
    label: '全部',
    prop: '0'
  }, {
    label: '在售中',
    prop: '1'
  }, {
    label: '已售罄',
    prop: '2'
  }, {
    label: '已下架',
    prop: '3'
  }]
}
export const tableoption = {
  emptyText: '暂无数据',
  selection: false,
  index: true,
  indexLabel: '序号',
  addBtn: false,
  editBtn: false,
  refreshBtn: false,
  columnBtn: false,
  align: 'center',
  menuAlign: 'center',
  border: true,
  header: false,
  column: [
    {
      label: '商品名称',
      prop: 'goodsName',
      fixed: true
    },
    {
      label: '权重',
      prop: 'sort',
      slot: true
    },
    {
      label: '商品图片',
      prop: 'thumbUrl',
      type: 'img',
      dataType: 'string',
      slot: true

    }, {
      label: '商品编码',
      prop: 'goodsSn'
    }, {
      label: '售价',
      prop: 'marketPrice'
    }, {
      label: '库存',
      prop: 'storage',
      slot: true
    }, {
      label: '单位',
      prop: 'unit'
    }, {
      label: '是否上架',
      prop: 'isMarketable',
      slot: true
    }, {
      label: '平台售卖',
      prop: 'relationType',
      slot: true
    }
  ]
}
export const supplyOption = {
  emptyText: '暂无数据',
  selection: false,
  index: true,
  indexLabel: '序号',
  addBtn: false,
  editBtn: false,
  refreshBtn: false,
  columnBtn: false,
  align: 'center',
  menuAlign: 'center',
  border: true,
  header: false,
  menu: false,
  height: 400,
  column: [
    {
      label: '商品名称',
      prop: 'goodsName',
    },
    {
      label: '商品图片',
      prop: 'modelUrl',
      type: 'img',
      dataType: 'string'
    },
    {
      label: '规格',
      prop: 'modelName'
    },
    {
      label: '单位',
      prop: 'modelUnit'
    },
    {
      label: '售价',
      prop: 'modelPrice'
    },
    {
      label: '库存',
      prop: 'modelStorage'
    },
    {
      label: '操作',
      prop: 'handle',
      slot: true
    }
  ]
}
