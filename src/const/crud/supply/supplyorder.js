
export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  menuAlign: 'center',
  align: 'center',
  addBtn: false,
  editBtn: false,
  column: [
    {
      label: '订单编号',
      prop: 'salesOrderSn',
      search: true,
      width: 100
    },
    {
      label: '客户编号',
      prop: 'customerSn',
      search: true,
      width: 100
    },
    {
      label: '客户名称',
      prop: 'customerName',
      search: true
    }, {
      label: '下单时间',
      prop: 'orderTime',
      overHidden: true,
      width: 180
    }, {
      label: '交货日期',
      prop: 'deliveryDate',
      overHidden: true,
      width: 120
    },
    {
      label: '合计金额',
      prop: 'sumAmount'
    },
    {
      label: '其它金额',
      prop: 'otherAmount'
    },
    {
      label: '总计金额',
      prop: 'totalAmount'
    },
    {
      label: '订单类型', //:1代客下单,2自主下单
      prop: 'salesType',
      dicUrl: '/admin/dict/type/scm_sales_type',
      type: 'select',
      search: true
    },
    {
      label: '业务员名称',
      prop: 'employeeName',
      width: 180
    },
    {
      label: '收货人名称',
      prop: 'linkmanName'
    },
    {
      label: '收货地址',
      prop: 'address',
      overHidden: true,
      width: 180
    },
    {
      label: '手机号',
      prop: 'telephone',
      search: true,
      width: 180
    },
    {
      label: '审核状态', //: 1.待审核2.审核不通过3.审核通过
      prop: 'reviewStatus',
      dicUrl: '/admin/dict/type/scm_review_status',
      type: 'select',
      search: true
    },
    {
      label: '货品种数',
      prop: 'goodsNum'
    },
    {
      label: '支付状态', //:0未付,1支付中,2已付
      prop: 'payStatus',
      dicUrl: '/admin/dict/type/scm_pay_status',
      type: 'select',
      search: true
    },
    {
      label: '创建时间',
      prop: 'createDate',
      hide: true
    },
    {
      label: '备注信息',
      prop: 'remarks'
    }
  ]
}
