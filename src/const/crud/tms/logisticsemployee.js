/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '通行证ID',
      prop: 'passportId'
    },
	  {
      label: '用户id',
      prop: 'userId'
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '联系电话',
      prop: 'telephone'
    },
	  {
      label: '物流员工类型:1物流经理2司机3第三方物流',
      prop: 'logisticsEmployeeType'
    },
	  {
      label: '启用状态:0.未启用1.启用',
      prop: 'useStatus'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
