/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家ID',
      prop: 'pmId'
    },
	  {
      label: '路线编号',
      prop: 'lineNo'
    },
	  {
      label: '路线名称',
      prop: 'routeName'
    },
	  {
      label: '出发地',
      prop: 'start'
    },
	  {
      label: '目的地',
      prop: 'destination'
    },
	  {
      label: '路线规划',
      prop: 'routePlanning'
    },
	  {
      label: '途径点',
      prop: 'pathPoint'
    },
	  {
      label: '路况',
      prop: 'roadCondition'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
