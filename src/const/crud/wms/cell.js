/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '货架编号',
            prop: 'shelfSn'
        },
        {
            label: '货位编号',
            prop: 'cellSn'
        },
        {
            label: '货位名称',
            prop: 'cellName',
            search:true
        },
        {
            label: '描述',
            prop: 'cellDesc'
        },
        {
            label: '货架行号',
            prop: 'shelfRow'
        },
        {
            label: '货架列号',
            prop: 'shelfColumn'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '存货状态',
            prop: 'storageStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_storage_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
