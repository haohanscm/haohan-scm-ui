/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '入库单编号',
            prop: 'enterWarehouseSn'
        },
        {
            label: '入库批次号',
            prop: 'batchNumber'
        },
        {
            label: '入库申请时间',
            prop: 'applyTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '采购单编号',
            prop: 'purchaseSn',
            search:true
        },
        {
            label: '入库状态',
            prop: 'enterStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_enter_status '
        },
        {
            label: '入库单类型',
            prop: 'enterType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_enter_type '
        },
        {
            label: '货品处理类型',
            prop: 'storageType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_storage_type '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
