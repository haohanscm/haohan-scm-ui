/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '入库单编号',
            prop: 'enterWarehouseSn'
        },
        {
            label: '入库单明细编号',
            prop: 'enterWarehouseDetailSn'
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '货架编号',
            prop: 'shelfSn'
        },
        {
            label: '货位编号',
            prop: 'cellSn'
        },
        {
            label: '托盘编号',
            prop: 'palletSn'
        },
        {
            label: '汇总单编号',
            prop: 'summaryOrderId'
        },
        {
            label: '采购单明细编号',
            prop: 'purchaseDetailSn'
        },
        {
            label: '入库商品规格id',
            prop: 'goodsModelId'
        },
        {
            label: '入库货品编号',
            prop: 'productSn'
        },
        {
            label: '入库货品数量',
            prop: 'productNumber'
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '申请人名称',
            prop: 'applicantName'
        },
        {
            label: '入库申请时间',
            prop: 'applyTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '验收人名称',
            prop: 'auditorName',
            search:true
        },
        {
            label: '验收时间',
            prop: 'auditTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '入库状态',
            prop: 'enterStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_enter_status '
        },
        {
            label: '货品处理类型',
            prop: 'storageType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_storage_type '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
