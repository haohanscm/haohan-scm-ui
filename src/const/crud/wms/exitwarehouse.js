/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '出库单编号',
            prop: 'exitWarehouseSn'
        },
        {
            label: '出库申请人名称',
            prop: 'applicantName',
            search:true
        },
        {
            label: '出库申请时间',
            prop: 'applyTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '出库状态',
            prop: 'exitStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_exit_status '
        },
        {
            label: '出库单类型',
            prop: 'exitType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_exit_type '
        },
        {
            label: '验收人名称',
            prop: 'auditorName'
        },
        {
            label: '验收时间',
            prop: 'auditTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
