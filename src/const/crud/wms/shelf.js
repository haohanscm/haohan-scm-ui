/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '货架名称',
            prop: 'shelfName',
            search:true
        },
        {
            label: '描述',
            prop: 'shelfDesc'
        },
        {
            label: '货架地址',
            prop: 'shelfAddress',
            search: true
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
