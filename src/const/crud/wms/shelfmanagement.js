/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '操作类型',
            prop: 'shelfOperate',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_shelf_operate '
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '货架编号',
            prop: 'shelfSn'
        },
        {
            label: '货位编号',
            prop: 'cellSn'
        },
        {
            label: '托盘编号',
            prop: 'palletSn'
        },
        {
            label: '货品名称',
            prop: 'productName',
            search: true
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '货品数量',
            prop: 'productNumber'
        },
        {
            label: '下架目标托盘编号',
            prop: 'targetPalletSn'
        },
        {
            label: '操作时间',
            prop: 'operateTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '操作人名称',
            prop: 'operatorName',
            search:true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
