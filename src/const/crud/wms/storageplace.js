/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '暂存点名称',
            prop: 'placeName',
            search: true
        },
        {
            label: '暂存点类型',
            prop: 'storagePlaceType',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_storage_place_type '
        },
        {
            label: '暂存点地址',
            prop: 'placeAddress'
        },
        {
            label: '描述',
            prop: 'placeDesc'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_use_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
