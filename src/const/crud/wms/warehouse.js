/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '仓库名称',
            prop: 'warehouseName',
            search:true
        },
        {
            label: '仓库类型',
            prop: 'warehouseType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_warehouse_type '
        },
        {
            label: '仓库地址',
            prop: 'warehouseAddress'
        },
        {
            label: '描述',
            prop: 'warehouseDesc'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '负责人名称',
            prop: 'directorName'
        },
        {
            label: '负责人电话',
            prop: 'directorTelephone'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
