/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '库存调拨编号',
            prop: 'warehouseAllotSn'
        },
        {
            label: '调出仓库名称',
            prop: 'outWarehouseName'
        },
        {
            label: '调入仓库名称',
            prop: 'inWarehouseName',
            search: true
        },
        {
            label: '调拨状态',
            prop: 'allotStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_allot_status '
        },
        {
            label: '调拨时间',
            prop: 'allotTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '申请人名称',
            prop: 'applicantName',
            search:true
        },
        {
            label: '申请时间',
            prop: 'applyTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '确认人名称',
            prop: 'auditorName'
        },
        {
            label: '确认时间',
            prop: 'auditTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
