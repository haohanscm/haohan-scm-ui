/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '库存调拨编号',
            prop: 'warehouseAllotSn'
        },
        {
            label: '库存调拨明细编号',
            prop: 'allotDetailSn'
        },
        {
            label: '调出仓库名称',
            prop: 'outWarehouseName'
        },
        {
            label: '调入仓库名称',
            prop: 'inWarehouseName'
        },
        {
            label: '调入仓库货架编号',
            prop: 'shelfSn'
        },
        {
            label: '调入仓库货位编号',
            prop: 'cellSn'
        },
        {
            label: '调入仓库托盘编号',
            prop: 'palletSn'
        },
        {
            label: '货品名称',
            prop: 'productName',
            search: true
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '货品数量',
            prop: 'productNumber'
        },
        {
            label: '调入仓库接收货品数量',
            prop: 'receiveNumber'
        },
        {
            label: '调拨状态:',
            prop: 'allotStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_allot_status '
        },
        {
            label: '操作人名称',
            prop: 'operatorName',
            search:true
        },
        {
            label: '操作时间',
            prop: 'operateTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
