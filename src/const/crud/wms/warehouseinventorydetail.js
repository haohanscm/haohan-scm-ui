/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '库存盘点明细编号',
            prop: 'inventoryDetailSn'
        },
        {
            label: '库存盘点编号',
            prop: 'warehouseInventorySn'
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '货品名称',
            prop: 'productName',
            search: true
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '原有数量',
            prop: 'originalNumber'
        },
        {
            label: '盘点后数量',
            prop: 'resultNumber'
        },
        {
            label: '操作人名称',
            prop: 'operatorName',
            search:true
        },
        {
            label: '盘点时间',
            prop: 'operateTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
