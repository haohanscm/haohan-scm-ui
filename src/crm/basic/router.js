import Layout from '@/page/index/'

const superviseRouter = {
    path: '/basic',
    // redirect: '/funds/purchase',
    component: Layout,
    meta: {title: '基础信息管理'},
    children: [
        {
            path: 'marketList',
            component: () => import('./market/market.vue'),
            name: '市场管理',
            meta: {title: '市场管理'}
        }, {
            path: 'addMarket',
            component: () => import('./market/addMarket.vue'),
            name: '新增市场管理',
            meta: {title: '新增市场管理'}
        }, {
            path: 'editMarket',
            component: () => import('./market/editMarket.vue'),
            name: '编辑市场管理',
            meta: {title: '编辑市场管理'}
        }, {
            path: 'detailMarket',
            component: () => import('./market/detailMarket.vue'),
            name: '市场管理详情',
            meta: {title: '市场管理详情'}
        },
        {
            path: 'moveMarket',
            component: () => import('./market/moveMarket.vue'),
            name: '迁移市场客户',
            meta: {title: '迁移市场客户'}
        },
        {
            path: 'regional',
            component: () => import('./regional/regional.vue'),
            name: '区域管理',
            meta: {title: '区域管理'}
        }, {
            path: 'contract',
            component: () => import('./contract/contract.vue'),
            name: '合同管理',
            meta: {title: '合同管理'}
        }, {
            path: 'addContract',
            component: () => import('./contract/addContract.vue'),
            name: '新增合同',
            meta: {title: '新增合同'}
        }, {
            path: 'editContract',
            component: () => import('./contract/editContract.vue'),
            name: '编辑合同',
            meta: {title: '编辑合同'}
        }, {
            path: 'detailContract',
            component: () => import('./contract/detailContract.vue'),
            name: '合同详情',
            meta: {title: '合同详情'}
        }
    ]
}

export default superviseRouter;
