import req from "../../http/httpBase.js"
export default class Request extends req {
    constructor() {
        super()
    }

      // 查询商列表
    fetchCtrList(params = {}) {
      return this.post("/crm/api/crm/customer/customerList", params)
    }
    // 客户分布地图
    customerMap(params = {}) {
      return this.post("/crm/api/crm/customer/customerMap", params)
    }
}
