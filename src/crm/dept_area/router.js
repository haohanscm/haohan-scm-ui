import Layout from '@/page/index/'

const teamRouter = {
  path: '/deptArea',
  redirect: '/deptArea/deptAreaList',
  component: Layout,
  meta: {title: '部门负责区域'},
  children: [
      {
        path: 'deptAreaList',
        component: () => import('./index.vue'),
        name: '部门负责区域列表',
        meta: {title: '部门负责区域列表'}
      }, 
      {
        path: 'detail',
        component: () => import('./detail.vue'),
        name: '部门负责区域详情',
        meta: {title: '部门负责区域详情'}
      },
      {
        path: 'update',
        component: () => import('./update.vue'),
        name: '编辑部门负责区域',
        meta: {title: '编辑部门负责区域'}
      }
  ]
}

export default teamRouter;
