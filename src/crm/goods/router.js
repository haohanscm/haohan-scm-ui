import Layout from '@/page/index/'

const waresRouter = {
    path: '/wares',
    redirect: '/goods/goodsList',
    component: Layout,
    meta: {title: '商品库'},
    children: [{
          path: 'waresList',
          component: () => import('./goods_list'),
          name: '商品列表',
          meta: {title: '商品列表'}
      }, {
          path: 'promotionSettings',
          component: () => import('./promotionSettings/index'),
          name: '促销设置',
          meta: {title: '促销设置'}
      }, {
          path: 'addSettings',
          component: () => import('./promotionSettings/addSettings'),
          name: '新增促销',
          meta: {title: '新增促销'}
      },
    // {
    //   path: 'platform_buyer_goods',
    //   component: () => import('./platform_buyer_goods/index.vue'),
    //   name: '商品报价',
    //   meta: {title: '商品报价'}
    // },
    // {
    //   path: 'addGoods',
    //   component: () => import('./../goods/addGoods/index.vue'),
    //   name: '新增商品',
    //   meta: {title: '新增商品'}
    // },
    // {
    //   path: 'editGoods',
    //   component: () => import('./../goods/addGoods/index.vue'),
    //   name: '编辑商品',
    //   meta: {title: '编辑商品'}
    // },
    {
      path: 'categories',
      component: () => import('./goods_cate/goodsCate.vue'),
      name: '商品分类',
      meta: {title: '商品分类'}
    },
    // {
    //   path: 'addGoodsCate',
    //   component: () => import('./../goods/goods_cate/modifyGoodsCate.vue'),
    //   name: '新增分类',
    //   meta: {title: '新增分类'}
    // },
    // {
    //   path: 'editGoodsCate',
    //   component: () => import('./../goods/goods_cate/modifyGoodsCate.vue'),
    //   name: '编辑分类',
    //   meta: {title: '编辑分类'}
    // },
  ]
}

export default waresRouter;
