import Layout from '@/page/index/'

const homeDeliveryRouter = {
    path: '/home_delivery',
    redirect: '/home_delivery/home_delivery_list',
    component: Layout,
    meta: {title: '送货服务记录'},
    children: [
        {
            path: 'home_delivery_list',
            component: () => import('./index.vue'),
            name: '送货服务记录列表',
            meta: {title: '送货服务记录列表'}
        },
        {
          path: 'detail',
          component: () => import('./detail.vue'),
          name: '送货服务记录详情',
          meta: {title: '送货服务记录详情'}
        },
        {
            path: 'add',
            component: () => import('./add.vue'),
            name: '新增送货服务记录',
            meta: {title: '新增送货服务记录'}
        },
        {
            path: 'update',
            component: () => import('./update.vue'),
            name: '编辑送货服务记录',
            meta: {title: '编辑送货服务记录'}
        }
    ]
}

export default homeDeliveryRouter;
