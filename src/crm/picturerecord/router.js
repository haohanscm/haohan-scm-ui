import Layout from '@/page/index/'

const picRouter = {
  path: '/pic',
  // redirect: '/customer/customerList',
  component: Layout,
  meta: {title: '现场图片'},
  children: [
      {
        path: 'picIndex',
        component: () => import('./index.vue'),
        name: '现场图片',
        meta: {title: '现场图片'}
      }, {
        path: 'detailPic',
        component: () => import('./detailPic.vue'),
        name: '图片详情',
        meta: {title: '图片详情'}
      }, {
        path: 'addPic',
        component: () => import('./addPic.vue'),
        name: '新增图片',
        meta: {title: '新增图片'}
      }, {
        path: 'editPic',
        component: () => import('./editPic.vue'),
        name: '编辑图片',
        meta: {title: '编辑图片'}
      }
  ]
}

export default picRouter;
