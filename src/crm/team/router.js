import Layout from '@/page/index/'

const teamRouter = {
  path: '/team',
  // redirect: '/customer/customerList',
  component: Layout,
  meta: {title: '客户数据库'},
  children: [
      {
        path: 'teamList',
        component: () => import('./teamList.vue'),
        name: '员工列表',
        meta: {title: '员工列表'}
      }, {
        path: 'teamDetail',
        component: () => import('./detail.vue'),
        name: '员工详情',
        meta: {title: '员工详情'}
      }, {
        path: 'addTeam',
        component: () => import('./add.vue'),
        name: '新增员工',
        meta: {title: '新增员工'}
      }, {
        path: 'editTeam',
        component: () => import('./update.vue'),
        name: '编辑员工',
        meta: {title: '编辑员工'}
      }
  ]
}

export default teamRouter;
