var qs = require("qs");
import http  from '@/router/axios'
import utils from '@/util/pds/utils'
let util = new utils();
import {getStore, setStore} from "@/util/store";

// // 添加请求拦截器
// http.interceptors.request.use(function (config) {
//   // 在发送请求之前做些什么
//   // console.log("reqParams:"+qs.stringify(config, "true"));
//   // console.log("real request url ==> ", config.url)
//   return config;
// }, function (error) {
//   // 对请求错误做些什么
//   return Promise.reject(error);
// });

// // // 添加响应拦截器/
// http.interceptors.response.use(function (response) {
//   // 对响应数据做点什么
//
//   // console.log("url:"+response.config.url);
//
//   if(response.config.url.startsWith("/pds")){
//     // let resp = JSON.stringify(response);
//     // console.log("pds-response:"+response.data);
//     return response.data;
//   }
//
//   return response;
// }, function (error) {
//   // 对响应错误做点什么
//   return Promise.reject(error);
// });

//设置请求主体
class httpBase {
  constructor() {
    this.pdsUrl = $constant.pdsUrl
    this.header = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    this.handleParams = function (params, indices, header) {
      // 用于一般系统
      params.shopId = getStore({name: "shopId"});
      params.pmId = getStore({name: "pmId"});
      // 用于君磊农产品

      if (header['Content-Type'] == 'application/x-www-form-urlencoded') {
        return qs.stringify(params, {
          indices
        })
      } else {
        return params;
      }

    }
  }

  //定义在类中的方法不需要添加function

  post(url, params, header = {
    'Content-Type': 'application/x-www-form-urlencoded',
  }, indices = true, pdsUrl = '') {
    var paramsData = this.handleParams(params, indices, header)
    // console.log("req-params:  " + paramsData)
    return http({
      url: (pdsUrl || this.pdsUrl) + url,
      data: paramsData,
      method: "post",
      headers: header
    })
  }

  json(url, params) {
    return this.post(url, params, {
      'Content-Type': 'application/json'
    })
  }

  get(url, header = {
    'Content-Type': 'text/html'
  }) {
    return http({
      url: url,
      method: "get",
      headers: header
    })
  }

  getPds(url, header = {
    'Content-Type': 'text/html'
  }) {
    return http({
      url: this.pdsUrl + url,
      method: "get",
      headers: header
    })
  }
}

export default httpBase
