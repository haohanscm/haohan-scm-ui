import Layout from '@/page/index/'

const inMailRouter = {
  path: '/inMail',
  redirect: '/inMail/list',
  component: Layout,
  meta: {title: '站内信消息'},
  children: [
      {
        path: 'list',
        component: () => import('./index.vue'),
        name: '消息列表',
        meta: {title: '消息列表'}
      },
      {
          path: 'detail',
          component: () => import('./detail.vue'),
          name: '消息详情',
          meta: {title: '消息详情'}
      },
      {
          path: 'sendPlatform',
          component: () => import('./platform.vue'),
          name: '发送平台消息',
          meta: {title: '发送平台消息'}
      },
      {
          path: 'modify',
          component: () => import('./modify.vue'),
          name: '消息修改',
          meta: {title: '消息修改'}
      },
  ]
}

export default inMailRouter;
