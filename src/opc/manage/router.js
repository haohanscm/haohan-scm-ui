import Layout from '@/page/index/'

const manageRouter = {
    path: '/manage',
    redirect: '/manage/merchant_list',
    component: Layout,
    meta: {title: '商家管理'},
    children: [
        {
            path: 'merchant_list',
            component: () => import('./merchant/index.vue'),
            name: '商家列表',
            meta: {title: '商家列表'}
        },
        {
            path: 'merchant_edit',
            component: () => import('./merchant/edit.vue'),
            name: '商家编辑',
            meta: {title: '商家编辑'}
        },
        {
            path: 'merchant_detail',
            component: () => import('./merchant/detail.vue'),
            name: '商家详情',
            meta: {title: '商家详情'}
        },
        {
            path: 'platform_merchant',
            component: () => import('./merchant/platform.vue'),
            name: '平台商家详情',
            meta: {title: '平台商家详情'}
        },
        {
            path: 'shop_list',
            component: () => import('./shop/index.vue'),
            name: '店铺列表',
            meta: {title: '店铺列表'}
        },
        {
            path: 'shop_edit',
            component: () => import('./shop/edit.vue'),
            name: '店铺编辑',
            meta: {title: '店铺编辑'}
        },
        {
            path: 'shop_detail',
            component: () => import('./shop/detail.vue'),
            name: '店铺详情',
            meta: {title: '店铺详情'}
        },

    ]
};

export default manageRouter;
