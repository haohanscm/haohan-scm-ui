import Layout from '@/page/index/'

const shipRouter = {
    path: '/shipRecord',
    redirect: '/shipRecord/shipList',
    component: Layout,
    meta: {title: '发货单'},
    children: [
        {
            path: 'shipList',
            component: () => import('./index.vue'),
            name: '发货单列表',
            meta: {title: '发货单列表'}
        },
        {
            path: 'shipDetail',
            component: () => import('./detail.vue'),
            name: '发货单详情',
            meta: {title: '发货单详情'}
        },
        {
            path: 'completeShip',
            component: () => import('./complete.vue'),
            name: '完成发货',
            meta: {title: '完成发货'}
        },
        {
            path: 'modifyShip',
            component: () => import('./edit.vue'),
            name: '编辑发货单',
            meta: {title: '编辑发货单'}
        },
    ]
}

export default shipRouter;
