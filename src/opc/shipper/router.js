import Layout from '@/page/index/'

const shipperRouter = {
    path: '/shipper',
    redirect: '/shipper/shipperList',
    component: Layout,
    meta: {title: '发货单'},
    children: [
        {
            path: 'shipperList',
            component: () => import('./index.vue'),
            name: '发货人列表',
            meta: {title: '发货人列表'}
        },
        {
            path: 'editShipper',
            component: () => import('./edit.vue'),
            name: '编辑发货人',
            meta: {title: '编辑发货人'}
        },
    ]
}

export default shipperRouter;
