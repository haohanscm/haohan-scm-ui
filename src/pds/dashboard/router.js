const sortingRouter = {
    path: '/dashboard',
    component: () => import('./index.vue'),
    name: 'dashboard',
    meta: { title: '数据汇总' }
}

export default sortingRouter;
