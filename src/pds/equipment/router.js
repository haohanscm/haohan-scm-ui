import Layout from '@/page/index/'

const equipmentRouter = {
    path: '/equipment',
    redirct: '/equipment/printer',
    component: Layout,
    meta: { title: '设备管理' },
    children:[
        {
            path: 'printer',
            component: () => import('./printer/index.vue'),
            name: '飞鹅打印机管理',
            meta: {title: '飞鹅打印机管理'}
        }, {
          path: 'detail',
          component: () => import('./printer/detail'),
          name: '飞鹅打印机详情',
          meta: {title: '飞鹅打印机详情'}
        },
        {
          path: 'edit',
          component: () => import('./printer/edit'),
          name: '编辑飞鹅打印机',
          meta: {title: '编辑飞鹅打印机'}
        },
        {
          path: 'handleAdd',
          component: () => import('./printer/handleAdd'),
          name: '新增飞鹅打印机',
          meta: {title: '新增飞鹅打印机'}
        },{
            path: 'cloudprintterminal',
            component: () => import('./cloudprintterminal/index.vue'),
            name: '易联云打印机',
            meta: {title: '易联云打印机'}
          }, {
          path: 'clouDdetail',
          component: () => import('./cloudprintterminal/detail'),
          name: '易联云打印机详情',
          meta: {title: '易联云打印机详情'}
        },
        {
          path: 'clouDedit',
          component: () => import('./cloudprintterminal/edit'),
          name: '编辑易联云打印机',
          meta: {title: '编辑易联云打印机'}
        },
        {
          path: 'clouDhandleAdd',
          component: () => import('./cloudprintterminal/handleAdd'),
          name: '新增易联云打印机',
          meta: {title: '新增易联云打印机'}
        },{
            path: 'terminal',
            component: () => import('./terminal/index.vue'),
            name: 'terminal',
            meta: {title: '终端管理'}
        }
    ]
}

export default equipmentRouter;
