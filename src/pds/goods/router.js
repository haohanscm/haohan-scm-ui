import Layout from '@/page/index/'

const goodsRouter = {
  path: '/goods',
  redirct: '/goods/goodsList',
  component: Layout,
  meta: {title: '商品库'},
  children: [
    // {
    //   path: 'goodsList',
    //   component: () => import('./goodsLib/index.vue'),
    //   name: '商品列表',
    //   meta: {title: '商品列表'}
    // },
      {
      path: 'goodsList',
      component: () => import('./goods_list'),
      name: '商品列表',
      meta: {title: '商品列表'}
    },
    // {
    //     path: 'goods_quote',
    //     component: () => import('./goods_quote/index.vue'),
    //     name: 'goods_quote',
    //     meta: {title: '商品报价'}
    // },
    {
      path: 'platform_buyer_goods',
      component: () => import('./platform_buyer_goods/index.vue'),
      name: '商品报价',
      meta: {title: '商品报价'}
    },
    {
      path: 'addGoods',
      component: () => import('./../goods/addGoods/index.vue'),
      name: '新增商品',
      meta: {title: '新增商品'}
    },
    {
      path: 'editGoods',
      component: () => import('./../goods/addGoods/index.vue'),
      name: '编辑商品',
      meta: {title: '编辑商品'}
    },
    {
      path: 'goodsCate',
      component: () => import('./goods_cate/goodsCate.vue'),
      name: '商品分类',
      meta: {title: '商品分类'}
    },
    {
      path: 'addGoodsCate',
      component: () => import('./../goods/goods_cate/modifyGoodsCate.vue'),
      name: '新增分类',
      meta: {title: '新增分类'}
    },
    {
      path: 'editGoodsCate',
      component: () => import('./../goods/goods_cate/modifyGoodsCate.vue'),
      name: '编辑分类',
      meta: {title: '编辑分类'}
    },
  ]
}

export default goodsRouter;
