import Layout from '@/page/index/'

const buyerRouter = {
        path: '/buyer',
        redirect: '/buyer/buyerList',
        component: Layout,
        meta: { title: '采购商' },
        children: [
                {
                        path: 'buyerList',
                        component: () => import('./index.vue'),
                        name: '采购商列表',
                        meta: { title: '采购商列表' }
                }, {
                        path: 'buyeradd',
                        component: () => import('./add.vue'),
                        name: '新增采购商',
                        meta: { title: '新增采购商' }
                }, {
                        path: 'buyeredit',
                        component: () => import('./edit.vue'),
                        name: '编辑采购商',
                        meta: { title: '编辑采购商' }
                }, {
                        path: 'buyerdetails',
                        component: () => import('./details.vue'),
                        name: '采购商详情',
                        meta: { title: '采购商详情' }
                }
        ]
}

export default buyerRouter;