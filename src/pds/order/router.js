import Layout from '@/page/index/'

const orderRouter = {
    path: '/orderCenter',
    redirect: '/orderCenter/supply_match',
    component: Layout,
    meta: { title: '订单管理' },
    children:[
        {
            path: 'afterSale',
            component: () => import('./afterSale/index.vue'),
            name: '售后订单',
            meta: {title: '售后订单'}
        },{
            path: 'order_purchase',
            component: () => import('./order/index.vue'),
            name: '采购订单',
            meta: {title: '采购订单'}
        },{
            path: 'supply_match',
            component: () => import('./supply_match/index.vue'),
            name: '供给匹配',
            meta: {title: '供给匹配'}
        },{
            path: 'order_delivery',
            component: () => import('./delivery/index.vue'),
            name: '配送订单',
            meta: {title: '配送订单'}
        },{
            path: 'trans',
            component: () => import('./trans/index.vue'),
            name: '交易订单',
            meta: {title: '交易订单'}
        },{
            path: 'order_supply',
            component: () => import('./supply/index.vue'),
            name: '供应订单',
            meta: {title: '供应订单'}
        },{
            path: 'customer_order',
            component: () => import('./customerOrder/index.vue'),
            name: '代客下单',
            meta: {title: '代客下单'}
        },{
            path: 'order_selfLift',
            component: () => import('./selfLift/index.vue'),
            name: '自提订单',
            meta: {title: '自提订单'}
        }, {
          path: 'return_order',
          component: () => import('./return_order/index.vue'),
          name: '退货订单',
          meta: {title: '退货订单'}
        }, {
          path: 'detail',
          component: () => import('./return_order/detail'),
          name: '退货订单详情',
          meta: {title: '退货订单详情'}
        },
        {
          path: 'edit',
          component: () => import('./return_order/edit'),
          name: '编辑退货订单',
          meta: {title: '编辑退货订单'}
        },
        {
          path: 'handleAdd',
          component: () => import('./return_order/handleAdd'),
          name: '新增退货单',
          meta: {title: '新增退货单'}
        },
        {
          path: 'cheack',
          component: () => import('./return_order/cheack'),
          name: '审核退货单',
          meta: {title: '审核退货单'}
        },
        {
          path: 'Warehousing',
          component: () => import('./return_order/Warehousing'),
          name: '退货单入库',
          meta: {title: '退货单入库'}
        },
        {
          path: 'takeOver',
          component: () => import('./return_order/takeOver'),
          name: '退货单确认收货',
          meta: {title: '退货单确认收货'}
        }
    ]
}

export default orderRouter;

