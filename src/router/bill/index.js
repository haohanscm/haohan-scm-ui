import receivableRouter from '../../bill/receivable/router' // 应收账单
import payableRouter from '../../bill/payable/router' // 应付账单
import settlementRouter from '../../bill/settlement/router' // 结算单

// 账单模块功能
export default [
    receivableRouter,
    payableRouter,
    settlementRouter
]
