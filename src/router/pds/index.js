import order from '../../pds/order/router'
import goods from '../../pds/goods/router'
import equipment from '../../pds/equipment/router'
import manage from '../../pds/manage/router'
import warehouse from "../../pds/warehouse/router"
import dashboard from "../../pds/dashboard/router"
import buyerRouter from "../../pds/manage/buyer/router"

export default [
  order,
  goods,
  equipment,
  manage,
  warehouse,
  dashboard,
  buyerRouter
]

