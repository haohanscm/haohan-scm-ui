import Vue from 'vue';
import Router from 'vue-router';
import demo from '../../page/demo/demoRouter'
import purchase from '../../pds/purchase/router'
import order from '../../pds/order/router'
import message from '../../pds/message/router'
import goods from '../../pds/goods/router'
import logistics from '../../pds/logistics/router'
import equipment from '../../pds/equipment/router'
import report from '../../pds/report/router'
import buyers from '../../pds/manage/router'
import sorting from '../../pds/sorting/router'
import warehouse from "../../pds/warehouse/router"
import dashboard from "../../pds/dashboard/router"
import storeCash from "../../pds/store_cash/router"


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: () => import('../../components/pds/navigation/Home.vue'),
      meta: {title: '首页'},
      hidden: true,
      children: [
        demo,
        supplier,
        purchase,
        order,
        message,
        goods,
        logistics,
        equipment,
        report,
        buyers,
        sorting,
        warehouse,
        dashboard,
        storeCash
      ]
    },
    {
      path: '/storeCash',
      component: () => import('../../components/pds/board/cash'),
      meta: {title: '收银台'},
      children: [
        storeCash,
      ]
    }
  ]
})
