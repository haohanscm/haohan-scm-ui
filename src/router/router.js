import Vue from 'vue'
import VueRouter from 'vue-router'
import AvueRouter from './avue-router'
import Store from '../store/'

import BillRouter from './bill/'
import CrmRouter from './crm/'
import MessageRouter from './message/'
import OpcRouter from './opc/'
import PageRouter from './page/'
import PdsRouter from './pds/'
import ScmRouter from './scm/'
import ViewsRouter from './views/'
import supply from './supply/'
import goods from './goods'

Vue.use(VueRouter)
const Router = new VueRouter({
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      if (from.meta.keepAlive) {
        from.meta.savedPosition = document.body.scrollTop
      }
      return {
        x: 0,
        y: to.meta.savedPosition || 0
      }
    }
  },
  routes: [].concat([])
})
AvueRouter.install(Router, Store)
Router.$avueRouter.formatRoutes(Store.state.user.menu, true)
Router.addRoutes([
  ...BillRouter,
  ...CrmRouter,
  ...MessageRouter,
  ...OpcRouter,
  ...PageRouter,
  ...PdsRouter,
  ...ScmRouter,
  ...ViewsRouter,
  ...supply,
  ...goods
])
export default Router
