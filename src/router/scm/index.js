import sendBack from '../../scm/purchase/send_back/router'
import purchaseOrder from '../../scm/purchase/purchase_order/router'
import marketQuotation from '../../scm/purchase/market_quotation/router'
import purchaseTask from '../../scm/purchase/purchase_task/router'
import order from '../../scm/order/order_manage/router'
import return_order from '../../scm/order/return_order/router'

import orderManage from '../../scm/order/order_manage/router'
import summaryOrder from '../../scm/order/summary_order/router'
import returnOrder from '../../scm/order/return_order/router'
import enterWarehouse from '../../scm/warehouse/enter_warehouse/router'
import exitWarehouse from '../../scm/warehouse/exit_warehouse/router'
import refund from '../../scm/finance/refund/router'
import audit from '../../scm/finance/audit/router'
import warehouseinventory from '../../scm/warehouse/warehouseinventory/router'
import breakage from '../../scm/warehouse/breakage/router'
import warehouseallot from '../../scm/warehouse/warehouseallot/router'
import outstorage from '../../scm/warehouse/outstorage/router'
import warehousewarning from '../../scm/warehouse/warehousewarning/router'
import clientmanage from '../../scm/client/clientmanage/router'
import customerstatistics from '../../scm/client/customerstatistics/router'
import purchaseEmployee from '../../scm/purchase/purchase_employee/router'

//财务管理
import receivable from '../../scm/finance/receivable/router'
import payable from '../../scm/finance/payable/router'
import settlement from '../../scm/finance/settlement/router'
// 供应
import supplierOrder from '../../scm/supplier/supplierorder/router'
import supplierdata from '../../scm/supplier/supplierdata/router'
import offerOrder from '../../scm/supplier/offerOrder/router'
// 生产
import shipOrder from '../../scm/product/shipOrder/router'



export default [
    // 采购
  sendBack,
  purchaseOrder,
    purchaseEmployee,
    purchaseTask,
  /**
   * 供应管理
   */
  supplierOrder,
  supplierdata,
    offerOrder,
  //订单管理
  order,
  orderManage,
  summaryOrder,
  return_order,
  //财务管理
  receivable, //应收账单
  payable, //应付账单
  settlement, //结算
  refund, //订单退款审核
  audit, //请款审核
  //仓库管理
  enterWarehouse,
  exitWarehouse,
  warehouseinventory,
  breakage,
  warehouseallot,
  outstorage,
  warehousewarning,
    // 生产
    shipOrder,
  //客户管理
  clientmanage,
  customerstatistics,

]
