import Layout from '@/page/index/'

// 订单管理
const orderManageRouter = {
  path: '/customerstatistics',
  redirect: '/customerstatistics/customerstatistics_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
    {
      path: 'customerstatistics_list',
      component: () => import('./../customerstatistics'),
      name: '应收账单'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详细'
    },

    /* {
       path: 'valetOrder',
       component: () => import('../../../scm/order/customerOrder/components/goodsItem'),
       name: '代客下单'
     }*/
  ]
}

export default orderManageRouter;
