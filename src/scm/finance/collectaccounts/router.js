import Layout from '@/page/index/'

// 应收账单
const orderManageRouter = {
  path: '/collectaccounts',
  redirect: '/closeaccount/collectaccounts_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
    {
      path: 'collectaccounts_list',
      component: () => import('./../collectaccounts'),
      name: '应收账单'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详细'
    }
    /* {
       path: 'valetOrder',
       component: () => import('../../../scm/order/customerOrder/components/goodsItem'),
       name: '代客下单'
     }*/
  ]
}

export default orderManageRouter;
