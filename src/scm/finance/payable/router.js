import Layout from '@/page/index/'

// 应付账单
const orderManageRouter = {
  path: '/payable',
  redirect: '/payable/payable_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
    {
      path: 'payable_list',
      component: () => import('./../payable'),
      name: '应付账单'
    },
    // {
    //   path: 'detail',
    //   component: () => import('./detail'),
    //   name: '详细'
    // },
    // {
    //   path: 'settlement',
    //   component: () => import('./settlement'),
    //   name: '结算'
    // }
  ]
}

export default orderManageRouter;
