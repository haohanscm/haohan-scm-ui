import Layout from '@/page/index/'

// 订单管理
const orderManageRouter = {
  path: '/settleaccounts',
  redirect: '/settleaccounts/settleaccounts_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
    {
      path: 'settleaccounts_list',
      component: () => import('./../settleaccounts'),
      name: '应付结算'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详细'
    },
    {
      path: 'settlement',
      component: () => import('./settlement'),
      name: '结算'
    }
   /* {
      path: 'valetOrder',
      component: () => import('../../../scm/order/customerOrder/components/goodsItem'),
      name: '代客下单'
    }*/
  ]
}

export default orderManageRouter;
