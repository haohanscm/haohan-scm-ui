import Layout from '@/page/index/'

// 配送单
const shipOrderRouter = {
    path: '/ship_order',
    redirect: '/ship_order/ship_order_list',
    component: Layout,
    meta: {title: '配送单'},
    children: [
        {
            path:'ship_order_list',
            component:() => import('./index'),
            name:'配送单',
            meta:{title:'配送单列表'}
        },
    ]
}

export default shipOrderRouter;
