import Layout from '@/page/index/'

// 采购单
const purchaseOrderRouter = {
    path: '/purchase_order',
    redirect: '/purchase_order/purchase_order_list',
    component: Layout,
    meta: {title: '采购单'},
    children: [
        {
            path: 'purchase_order_list',
            component: () => import('./../purchase_order'),
            name: '采购单列表'
        },
        {
            path: 'addPlan',
            component: () => import('./add_plan'),
            name: '新增采购计划'
        },
        {
            path: 'addNeeds',
            component: () => import('./add_needs'),
            name: '新增按需采购'
        },
        {
            path: 'detail',
            component: () => import('./detail'),
            name: '采购单详情'
        },
        {
            path: 'edit',
            component: () => import('./edit'),
            name: '编辑采购单'
        },
        {
          path: 'launch',
          component: () => import('./launch'),
          name: '发起采购'
        },
        {
            path: 'check',
            component: () => import('./check'),
            name: '审核采购单'
        }
    ]
}

export default purchaseOrderRouter;
