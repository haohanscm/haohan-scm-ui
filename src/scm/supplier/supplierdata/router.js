import Layout from '@/page/index/'

// 供应商管理
const supplierDataRouter = {
    path: '/supplierdata',
    redirect: '/supplierdata/list',
    component: Layout,
    meta: {title: '供应商管理'},
    children: [
        {
            path:'list',
            component:() => import('./../supplierdata'),
            name:'供应商列表',
            meta:{title:'供应商列表'}
        },
        {
            path: 'add',
            component: () => import('./../supplierdata/add'),
            name: '添加供应商',
            meta: {title: '添加供应商'}
        },
        {
            path: 'edit',
            component: () => import('./../supplierdata/add'),
            name: '编辑供应商信息',
            meta: {title:"编辑供应商信息"}
        },

    ]
}

export default supplierDataRouter;
