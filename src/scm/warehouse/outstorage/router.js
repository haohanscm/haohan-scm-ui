import Layout from '@/page/index/'

// 仓库管理
const outstorageRouter = {
  path: '/outstorage',
  redirect: '/outstorage/outstorage_list ',
  component: Layout,
  meta: {
    title: '仓库'
  },
  children: [{
      path: 'outstorage_list',
      component: () => import('./../outstorage'),
      name: '盘点'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详情'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详情'
    }
  ]
}

export default outstorageRouter;
