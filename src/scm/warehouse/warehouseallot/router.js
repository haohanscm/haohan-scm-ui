import Layout from '@/page/index/'

// 仓库管理
const warehouseallotRouter = {
  path: '/warehouseallot',
  redirect: '/warehouseallot/warehouseallot_list ',
  component: Layout,
  meta: {
    title: '仓库'
  },
  children: [{
      path: 'warehouseallot_list',
      component: () => import('./../warehouseallot'),
      name: '盘点'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详情'
    },
    {
      path: 'redact',
      component: () => import('./redact'),
      name: '编辑'
    },
    {
      path: 'increased',
      component: () => import('./increased'),
      name: '新增'
    }
  ]
}

export default warehouseallotRouter;
