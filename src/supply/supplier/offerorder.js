/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 100,
  align: 'center',
  delBtn: false,
  addBtn: false,
  editBtn: false,
  maxHeight: 600,
  column: [
    {
      label: '平台商家',
      prop: 'pmId',
      hide: true
    },
    {
      label: '报价用户',
      prop: 'offerUid',
      hide: true
    },
    {
      label: '报价单号',
      prop: 'offerOrderId',
      search: true
    },
    {
      label: '询价单号',
      prop: 'askOrderId',
      hide: true
    },
    {
      label: '采购明细编号',
      prop: 'purchaseDetailSn',
      search: true
    },
    {
      label: '供应商',
      prop: 'supplierId',
      hide: true
    },
    {
      label: '供应商名称',
      prop: 'supplierName',
      search: true
    },
    {
      label: '商品名称',
      prop: 'goodsName',
      search: true
    },
    {
      label: '规格名称',
      prop: 'modelName'
    },
    {
      label: '单位',
      prop: 'unit'
    },
    {
      label: '采购数量',
      prop: 'buyNum'
    },
    {
      label: '最大供应数量',
      prop: 'supplyNum'
    },
    {
      label: '供应商报价',
      prop: 'supplyPrice'
    },
    {
      label: '实物图片',
      prop: 'supplyImg',
      type: 'upload',
      imgWidth: 100,
      imgHeight: 50,
      listType: 'picture-img',
      hide: true
    },
    {
      label: '商品图片',
      prop: 'goodsImg',
      type: 'upload',
      imgWidth: 100,
      imgHeight: 50,
      listType: 'picture-img',
      disabled: true
    },
    {
      label: '供应说明',
      prop: 'supplyDesc'
    },
    {
      label: '起售数量',
      prop: 'minSaleNum',
      hide: true
    },
    {
      label: '成交单价',
      prop: 'dealPrice'
    },
    {
      label: '备注信息',
      prop: 'remarks'
    },
    {
      label: '报价类型',
      prop: 'offerType',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_pds_offer_type'
    },
    {
      label: '报价单状态',
      prop: 'status',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_pds_offer_status '
    },
    {
      label: '发货状态',
      prop: 'shipStatus',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_ship_status '
    },

    {
      label: '揽货方式',
      prop: 'receiveType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_receive_type'
    },
    {
      label: '备货时间',
      prop: 'prepareDate',
      type: 'datetime',
      format: 'yyyy-MM-dd',
      valueFormat: 'yyyy-MM-dd',
      search: true
    },
    {
      label: '询价时间',
      prop: 'askPriceTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      hide: true
    },
    {
      label: '报价时间',
      prop: 'offerPriceTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
    {
      label: '成交时间',
      prop: 'dealTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      hide: true
    },
    {
      label: '发货时间',
      prop: 'deliveryTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      hide: true
    },
    {
      label: '收货时间',
      prop: 'receiveTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      hide: true
    },
    {
      label: '货源有效期',
      prop: 'validityTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
    {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true,
      hide: true
    }
  ]
}
